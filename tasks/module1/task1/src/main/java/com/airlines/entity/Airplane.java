package com.airlines.entity;

import java.util.Objects;

public class Airplane {
    private AirplaneType type;
    private Long id;
    private String name;
    private String destination;
    private String pointOfDeparture;

    public Long getCapacity() {
        return capacity;
    }

    public Long getCarryingCapacity() {
        return carryingCapacity;
    }

    private Long capacity;
    private Long carryingCapacity;
    public AirplaneType getType() {
        return type;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getPointOfDeparture() {
        return pointOfDeparture;
    }

    public void setPointOfDeparture(String pointOfDeparture) {
        this.pointOfDeparture = pointOfDeparture;
    }

    @Override
    public String toString() {
        return "Airplane" + "<br>" +
                " type=" + type.getType() + "<br>" +
                " id=" + id + "<br>" +
                " name='" + name + '\'' + "<br>" +
                " pointOfDeparture='" + pointOfDeparture + '\'' + "<br>" +
                " destination='" + destination + '\'' + "<br>"+
                " capacity=" + capacity +  "<br>"+
                " carryingCapacity=" + carryingCapacity + "<br>"
                ;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Airplane airplane = (Airplane) o;
        return type == airplane.type &&
                Objects.equals(id, airplane.id) &&
                Objects.equals(name, airplane.name) &&
                Objects.equals(destination, airplane.destination) &&
                Objects.equals(pointOfDeparture, airplane.pointOfDeparture) &&
                Objects.equals(capacity, airplane.capacity) &&
                Objects.equals(carryingCapacity, airplane.carryingCapacity);
    }

    @Override
    public int hashCode() {
        return Objects.hash(type, id, name, destination, pointOfDeparture, capacity, carryingCapacity);
    }

    public void setType(AirplaneType type) {
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Airplane(AirplaneType type, Long id, String name, String pointOfDeparture, String destination,Long capacity, Long carryingCapacity ) {
        this.type = type;
        this.id = id;
        this.name = name;
        this.pointOfDeparture = pointOfDeparture;
        this.destination = destination;
        this.capacity = capacity;
        this.carryingCapacity = carryingCapacity;

    }

}
