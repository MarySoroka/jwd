package com.airlines.command;

import java.util.Map;

public class DefaultCommand implements Command {

    @Override
    public String execute(Map<String, String> data) {
       return Command.getDefaultResources();
    }
}
