package com.airlines.command;

import com.airlines.service.AirlineService;

import java.util.Map;

public class GetTotalCapacityCommand implements Command {
    private AirlineService airlineService;

    public GetTotalCapacityCommand(AirlineService airlineService) {
        this.airlineService = airlineService;
    }

    @Override
    public String execute(Map<String, String> data) {
        return Command.getResources() +"Total capacity "+ airlineService.getTotalCapacity()+"<br>" + Command.fromListToString(airlineService.getAllAirplanes());
    }

}
