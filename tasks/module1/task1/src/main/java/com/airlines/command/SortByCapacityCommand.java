package com.airlines.command;

import com.airlines.comparator.CapacityComparator;
import com.airlines.service.AirlineService;

import java.util.Map;

public class SortByCapacityCommand implements Command {
    private AirlineService airlineService;
    private CapacityComparator capacityComparator;

    public SortByCapacityCommand(AirlineService airlineService) {
        this.airlineService = airlineService;
        this.capacityComparator = new CapacityComparator();
    }

    @Override
    public String execute(Map<String, String> data) {
        return Command.getResources() + Command.fromListToString(airlineService.sort(capacityComparator)).toString();
    }
}
