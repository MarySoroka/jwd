package com.airlines.entity;

public class CargoAirplane extends Airplane {
    public CargoType getCargoType() {
        return cargoType;
    }

    @Override
    public String toString() {
        return super.toString() +
                "cargoType=" + cargoType.getType() + "<br>" + "<br>";
    }
    private CargoType cargoType;

    public CargoAirplane(AirplaneType type, Long id, String name, String pointOfDeparture, String destination, String cargoType, Long capacity, Long carryingCapacity) {
        super(type, id, name, pointOfDeparture, destination, capacity, carryingCapacity);
        this.cargoType = CargoType.getByName(cargoType);
    }
}
