package com.airlines.service;

import com.airlines.entity.Airplane;

import java.util.Comparator;
import java.util.List;

public interface AirlineService {
    List<Airplane> getAllAirplanes();

    List<Airplane> readFromFileAirplanes(String fileData);

    List<Airplane> sort(Comparator<? super Airplane> comparator);

    Long getTotalCapacity();

    Long getTotalCarryingCapacity();
}
