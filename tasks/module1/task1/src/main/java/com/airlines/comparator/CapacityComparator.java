package com.airlines.comparator;

import com.airlines.entity.Airplane;

import java.util.Comparator;

public class CapacityComparator implements Comparator<Airplane> {
    @Override
    public int compare(Airplane a1, Airplane a2) {
        return Long.compare(a1.getCapacity(),a2.getCapacity());
    }

}
