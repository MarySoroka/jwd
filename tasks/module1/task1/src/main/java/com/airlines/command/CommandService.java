package com.airlines.command;

import java.util.Map;

public interface CommandService {
    String execute(Map<String, String> data);

}
