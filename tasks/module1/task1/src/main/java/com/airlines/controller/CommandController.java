package com.airlines.controller;

import com.airlines.command.Command;
import com.airlines.command.CommandService;
import com.airlines.command.CommandType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.util.Map;

import static com.airlines.ApplicationConstants.DEFAULT_COMMAND;

public class CommandController implements CommandService {
    private Map<CommandType, Command> commandMap;
    private static final Log LOGGER = LogFactory.getLog(CommandController.class);
    public CommandController(Map<CommandType, Command> commandMap) {
        this.commandMap = commandMap;
    }

    @Override
    public String execute(Map<String, String> data) {
        final Command command = commandMap.get(CommandType.getByName(data.getOrDefault("command",DEFAULT_COMMAND)));
        String airplanes = command.execute(data);
        LOGGER.info("start command using factory");
        return airplanes;
    }
}
