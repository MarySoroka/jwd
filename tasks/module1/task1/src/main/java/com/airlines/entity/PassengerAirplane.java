package com.airlines.entity;

import java.util.Objects;

public class PassengerAirplane extends Airplane {
    public Integer getMaxAmountOfPassengers() {
        return maxAmountOfPassengers;
    }

    private Integer maxAmountOfPassengers;

    @Override
    public String toString() {
        return super.toString() +
                "maxAmountOfPassengers = " + maxAmountOfPassengers + "<br>" + "<br>"
                ;
    }



    public PassengerAirplane(AirplaneType type, Long id, String name, String pointOfDeparture, String destination, String maxAmount, Long capacity, Long carryingCapacity) {
        super(type, id, name, pointOfDeparture, destination, capacity, carryingCapacity);
        this.maxAmountOfPassengers = Integer.parseInt(maxAmount);
    }
}
