package com.airlines.dal;

import com.airlines.entity.Airplane;
import com.airlines.entity.AirplaneType;
import com.airlines.entity.factory.FactoryService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class InMemoryAirlineImpl implements InMemoryAirline {
    public List<Airplane> getAirplanes() {
        return airplanes;
    }

    private final List<Airplane> airplanes = new ArrayList<>();
    private FactoryService factory;

    public InMemoryAirlineImpl(FactoryService factory) {
        this.factory = factory;

    }

    private void initialization(String data) {
        String[] airplaneList = data.split(",");
        for (int i = 0; i < airplaneList.length; i += 8) {
            this.airplanes.add(factory.getAirplane(AirplaneType.getByName(airplaneList[i + 1]),
                    Long.valueOf(airplaneList[i]), airplaneList[i + 2], airplaneList[i + 3],
                    airplaneList[i + 4], airplaneList[i + 5], Long.parseLong(airplaneList[i + 6]), Long.parseLong(airplaneList[i + 7])));
        }


    }

    @Override
    public List<Airplane> getAllAirplanes() {
        return new ArrayList<>(this.airplanes);
    }

    @Override
    public List<Airplane> readFromFileAirplanes(String data) {
        initialization(data);
        return this.airplanes;
    }


    @Override
    public  List<Airplane> sort(Comparator<? super Airplane> comparator) {
        List<Airplane> airplane = getAirplanes();
        airplane.sort(comparator);
        return airplane;
    }


}
