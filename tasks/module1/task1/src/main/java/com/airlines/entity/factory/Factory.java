package com.airlines.entity.factory;

import com.airlines.entity.Airplane;
import com.airlines.entity.AirplaneType;

import java.util.Map;

public class Factory implements FactoryService{
    private Map<AirplaneType, AirplaneFactory> airplaneFactoryMap;
    public Factory(Map<AirplaneType,AirplaneFactory> airplaneFactoryMap) {
        this.airplaneFactoryMap = airplaneFactoryMap;

    }
    @Override
    public Airplane getAirplane(AirplaneType type, Long id, String name, String pointOfDeparture, String destination, String specificData, Long capacity, Long carryingCapacity){
         final AirplaneFactory airplaneFactory = airplaneFactoryMap.get(type);
         return airplaneFactory.createAirplane(type, id, name, pointOfDeparture, destination, specificData, capacity, carryingCapacity);
    }
}
