package com.airlines.command;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public enum CommandType {
    DEFAULT("DEFAULT"),
    READ_FROM_FILE("READ_FROM_FILE"),
    GET_TOTAL_CAPACITY("TOTAL_CAPACITY"),
    GET_TOTAL_CARRYING_CAPACITY("TOTAL_CARRYING_CAPACITY"),
    SORT_BY_CAPACITY("SORT_BY_CAPACITY"),
    SORT_BY_TYPE_AND_NAME("SORT_BY_TYPE_AND_NAME");
    public String getType() {
        return type;
    }
    private static final Log LOGGER = LogFactory.getLog(CommandType.class);

    private String type;

    CommandType(String type) {
        this.type = type;
    }

    public static CommandType getByName(String name) {
        for (CommandType c :
                CommandType.values()) {
            if (c.type.equals(name)) {
                return c;
            }
        }
        LOGGER.error(" can't find command with this name");
        return CommandType.DEFAULT;
    }
}
