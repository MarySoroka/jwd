package com.airlines.service;

import com.airlines.dal.InMemoryAirline;
import com.airlines.entity.Airplane;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class AirlineServiceImpl implements AirlineService {
    private InMemoryAirline inMemoryAirline;

    public AirlineServiceImpl(InMemoryAirline inMemoryAirline) {
        this.inMemoryAirline = inMemoryAirline;
    }

    @Override
    public List<Airplane> getAllAirplanes() {
         return new ArrayList<>(inMemoryAirline.getAllAirplanes());
    }

    @Override
    public List<Airplane> readFromFileAirplanes(String fileData) {
        return new ArrayList<>(inMemoryAirline.readFromFileAirplanes(fileData));

    }

    @Override
    public List<Airplane> sort(Comparator<? super Airplane> comparator) {
        return inMemoryAirline.sort(comparator);
    }

    @Override
    public Long getTotalCapacity() {
        long capacity = 0;
        for (Airplane a :
                inMemoryAirline.getAllAirplanes()) {
            capacity += a.getCapacity();
        }
        return capacity;
    }

    @Override
    public Long getTotalCarryingCapacity() {
        long capacity = 0;
        for (Airplane a :
                inMemoryAirline.getAllAirplanes()) {
            capacity += a.getCarryingCapacity();
        }
        return capacity;
    }
}
