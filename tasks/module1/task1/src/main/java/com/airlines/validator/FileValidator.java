package com.airlines.validator;

import com.airlines.entity.AerialWorkType;
import com.airlines.entity.AirplaneType;
import com.airlines.entity.CargoType;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.airlines.ApplicationConstants.VALID;

public class FileValidator {
    private static final Log LOGGER = LogFactory.getLog(FileValidator.class);

    public Map<Boolean, String> isFileDataValid(String filepath) {
        Map<Boolean, String> map = isPathValid(filepath);
        if (map.containsKey(false)) {
            LOGGER.error(map.get(false));
            return map;
        }
        File file = new File(filepath);
        StringBuilder airplaneList = new StringBuilder();
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String airplane = br.readLine();
            while ((airplane = br.readLine()) != null) {
                String[] data = airplane.split(",");
                map.putAll(isNumberOfDataValid(data.length));

                map.putAll(isNumberValid(data[0]));
                map.putAll(isTypeValid(data[1]));
                map.putAll(isSpecificDataValid(data[1], data[5]));
                map.putAll(isNumberValid(data[6]));
                map.putAll(isNumberValid(data[7]));
                if (map.containsKey(false)) {
                    LOGGER.error(map.get(false));
                    return map;

                }
                airplaneList.append(data[0]).append(",");
                airplaneList.append(data[1]).append(",");
                airplaneList.append(data[2]).append(",");
                airplaneList.append(data[3]).append(",");
                airplaneList.append(data[4]).append(",");
                airplaneList.append(data[5]).append(",");
                airplaneList.append(data[6]).append(",");
                airplaneList.append(data[7]).append(",");
            }
        } catch (IOException e) {
            LOGGER.error("Can't read from file "+filepath);
            map.put(false, e.getMessage());
        }
        if (airplaneList.length() == 0){
            LOGGER.error("Can't read from file "+filepath);
            map.put(false, "Can't read this file");
        }
        map.put(true, airplaneList.toString());
        return map;
    }

    public Map<Boolean, String> isNumberValid(String data) {
        Map<Boolean, String> map = new HashMap<>();
        try {
            Integer.parseInt(data);
        } catch (NumberFormatException ex) {
            map.put(false, ex.getMessage());
            return map;
        }
        map.put(true, VALID);
        return map;
    }

    public Map<Boolean, String> isTypeValid(String data) {
        Map<Boolean, String> map = new HashMap<>();
        if (AirplaneType.getByName(data) == null) {
            map.put(false, "invalid airplane type");
            LOGGER.error(map.get(false));
            return map;
        }
        map.put(true, VALID);
        return map;

    }

    public Map<Boolean, String> isCargoTypeValid(String data) {
        Map<Boolean, String> map = new HashMap<>();
        if (CargoType.getByName(data) == null) {
            map.put(false, "Invalid cargo type");
            LOGGER.error(map.get(false));
            return map;
        }
        map.put(true, VALID);
        return map;

    }

    public Map<Boolean, String> isAerialWorkTypeValid(String data) {
        Map<Boolean, String> map = new HashMap<>();
        if (AerialWorkType.getByName(data) == null) {
            map.put(false, "Invalid aerial work type");
            LOGGER.error(map.get(false));
            return map;
        }
        map.put(true, VALID);
        return map;

    }

    public Map<Boolean, String> isSpecificDataValid(String airplaneType, String specificData) {
        Map<Boolean, String> map = new HashMap<>();
        switch (Objects.requireNonNull(AirplaneType.getByName(airplaneType))) {
            case PASSENGER:
                map = isNumberValid(specificData);
                break;
            case CARGO:
                map = isCargoTypeValid(specificData);
                break;
            case WORK:
                map = isAerialWorkTypeValid(specificData);
                break;
        }
        return map;

    }

    public Map<Boolean, String> isNumberOfDataValid(int len) {
        Map<Boolean, String> map = new HashMap<>();
        if (len != 8) {
            map.put(false, "invalid number of arguments");
            LOGGER.error(map.get(false));
            return map;
        }
        map.put(true, VALID);
        return map;
    }

    public Map<Boolean, String> isPathValid(String filePath) {
        Map<Boolean, String> map = new HashMap<>();
        File file;
        file = new File(filePath);
        if (!file.exists()) {
            map.put(false, "This file doesn't exist");
            LOGGER.error(map.get(false));
            return map;
        }
        if (file.isDirectory()) {
            map.put(false, "This is directory");
            LOGGER.error(map.get(false));
            return map;
        }
        map.put(true, filePath);
        return map;
    }
}
