package com.airlines.command;

import com.airlines.service.AirlineService;

import java.util.Map;

public class GetTotalCarryingCapacityCommand implements Command {
    private AirlineService airlineService;

    public GetTotalCarryingCapacityCommand(AirlineService airlineService) {
        this.airlineService = airlineService;
    }

    @Override
    public String execute(Map<String, String> data) {
        return Command.getResources() +"Total carrying capacity " + airlineService.getTotalCarryingCapacity() +"<br>"+ Command.fromListToString(airlineService.getAllAirplanes());
    }

}
