package com.airlines.command;

import com.airlines.validator.FileValidator;
import com.airlines.service.AirlineService;

import java.util.Map;

public class ReadCommand implements Command {
    private AirlineService airlineService;

    public ReadCommand(AirlineService airlineService) {
        this.airlineService = airlineService;
        this.fileValidator = new FileValidator();
    }

    private FileValidator fileValidator;


    @Override
    public String execute(Map<String, String> data) {
        String filepath = data.getOrDefault("value", "");
        Map<Boolean, String> dataValidation = fileValidator.isFileDataValid(filepath);
        if (dataValidation.containsKey(false)) {
            return Command.getDefaultResources()+dataValidation.get(false);
        }
        return Command.getResources() + Command.fromListToString(airlineService.readFromFileAirplanes(dataValidation.get(true)));
    }
}


