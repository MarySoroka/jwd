package com.airlines.command;

import com.airlines.comparator.AirplaneTypeComparator;
import com.airlines.comparator.NameComparator;
import com.airlines.service.AirlineService;

import java.util.Map;

public class SortByTypeAndNameCommand implements Command {
    private AirlineService airlineService;
    private NameComparator nameComparator;
    private AirplaneTypeComparator airplaneTypeComparator;

    public SortByTypeAndNameCommand(AirlineService airlineService) {
        this.airlineService = airlineService;
        this.nameComparator = new NameComparator();
        this.airplaneTypeComparator = new AirplaneTypeComparator();
    }

    @Override
    public String execute(Map<String, String> data) {
        return Command.getResources() + Command.fromListToString(airlineService.sort(airplaneTypeComparator.thenComparing(nameComparator))).toString();
    }
}
