package com.airlines.entity;

import com.airlines.entity.factory.AerialWorkAirplaneFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public enum CargoType {
    INANIMATE("inanimate"),
    ANIMATE("animate");
    private String type;

    public String getType() {
        return type;
    }

    private static final Log LOGGER = LogFactory.getLog(AerialWorkAirplaneFactory.class);

    CargoType(String type) {
        this.type = type;
    }

    public static CargoType getByName(String cargoType) {
        for (CargoType t :
                CargoType.values()) {
            if (t.type.equals(cargoType)) {
                return t;
            }
        }
        LOGGER.info("can't find type by name");
        return null;
    }
}
