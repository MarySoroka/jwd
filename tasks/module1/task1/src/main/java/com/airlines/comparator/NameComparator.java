package com.airlines.comparator;

import com.airlines.entity.Airplane;

import java.util.Comparator;

public class NameComparator implements Comparator<Airplane> {
    @Override
    public int compare(Airplane a1, Airplane a2) {
        return a1.getName().compareTo(a2.getName());
    }
}

