package com.airlines.entity;

public class AerialWorkAirplane extends Airplane {
    public AerialWorkType getAerialWorkType() {
        return aerialWorkType;
    }

    private AerialWorkType aerialWorkType;

    public AerialWorkAirplane(AirplaneType type, Long id, String name, String pointOfDeparture, String destination, String aerialWorkType, Long capacity, Long carryingCapacity) {
        super(type, id, name, pointOfDeparture, destination, capacity, carryingCapacity);
        this.aerialWorkType = AerialWorkType.getByName(aerialWorkType);
    }

    @Override
    public String toString() {
        return super.toString() +
                "aerialWorkType=" + aerialWorkType.getType() + "<br>" + "<br>";
    }
}
