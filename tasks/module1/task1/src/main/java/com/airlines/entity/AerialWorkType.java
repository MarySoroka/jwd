package com.airlines.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public enum AerialWorkType {
    MEDICAL("medical"),
    SHOOTING("shooting"),
    CHEMICAL("chemical");
    private String type;

    public String getType() {
        return type;
    }

    AerialWorkType(String type) {
        this.type = type;
    }

    private static final Log LOGGER = LogFactory.getLog(AerialWorkType.class);

    public static AerialWorkType getByName(String aerialWorkType) {
        for (AerialWorkType a :
                AerialWorkType.values()) {
            if (a.type.equals(aerialWorkType)) {
                return a;
            }
        }
        LOGGER.error("can't find type by name");
        return null;
    }
}
