package com.airlines;

import com.airlines.command.*;
import com.airlines.controller.CommandController;
import com.airlines.controller.ServerHandler;
import com.airlines.dal.InMemoryAirline;
import com.airlines.dal.InMemoryAirlineImpl;
import com.airlines.entity.AirplaneType;
import com.airlines.entity.factory.*;
import com.airlines.service.AirlineService;
import com.airlines.service.AirlineServiceImpl;
import com.sun.net.httpserver.HttpServer;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.util.EnumMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Application {
    static final Log logger = LogFactory.getLog(Application.class);

    public static void main(String[] args) throws IOException {
        InetSocketAddress localhost = new InetSocketAddress("localhost", 36666);
        HttpServer server = HttpServer.create(localhost, 0);

        Map<AirplaneType, AirplaneFactory> airplaneFactoryMap = new EnumMap<>(AirplaneType.class);
        airplaneFactoryMap.put(AirplaneType.PASSENGER, new PassengerAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.CARGO, new CargoAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.WORK, new AerialWorkAirplaneFactory());
        FactoryService factoryService = new Factory(airplaneFactoryMap);
        InMemoryAirline dao = new InMemoryAirlineImpl(factoryService);

        AirlineService airlineService = new AirlineServiceImpl(dao);
        Map<CommandType, Command> commandMap = new EnumMap<>(CommandType.class);
        commandMap.put(CommandType.DEFAULT, new DefaultCommand());
        commandMap.put(CommandType.READ_FROM_FILE, new ReadCommand(airlineService));
        commandMap.put(CommandType.GET_TOTAL_CAPACITY, new GetTotalCapacityCommand(airlineService));
        commandMap.put(CommandType.GET_TOTAL_CARRYING_CAPACITY, new GetTotalCarryingCapacityCommand(airlineService));
        commandMap.put(CommandType.SORT_BY_CAPACITY, new SortByCapacityCommand(airlineService));
        commandMap.put(CommandType.SORT_BY_TYPE_AND_NAME, new SortByTypeAndNameCommand(airlineService));
        CommandService commandService = new CommandController(commandMap);


        server.createContext("/airline", new ServerHandler(commandService));

        ExecutorService executor = Executors.newFixedThreadPool(4);
        server.setExecutor(executor);
        server.start();

        logger.info("Server started on " + server.getAddress().toString());

    }
}
