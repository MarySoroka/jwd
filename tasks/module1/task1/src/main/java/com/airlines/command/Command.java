package com.airlines.command;

import com.airlines.entity.Airplane;

import java.util.List;
import java.util.Map;

public interface Command{
    String execute(Map<String, String> data);
    static StringBuilder fromListToString(List<Airplane> airplanes) {
        StringBuilder airlineResponse = new StringBuilder();
        airlineResponse.append("<ul>");
        for (Airplane airplane : airplanes) {

            airlineResponse.append("<li>");
            airlineResponse.append(airplane.toString());
            airlineResponse.append("</li>");

        }
        airlineResponse.append("</ul>");
        return airlineResponse;
    }
    static String getResources(){
        return "<form action=\"/airline\" method=\"POST\">\n" +
                "    <input type=\"hidden\" name=\"command\" value=\"TOTAL_CAPACITY\">\n" +
                "    <input type=\"submit\" value=\"Total capacity\">\n" +
                "</form>\n" +"<br>"+
                "<form action=\"/airline\" method=\"POST\">\n" +
                "    <input type=\"hidden\" name=\"command\" value=\"TOTAL_CARRYING_CAPACITY\">\n" +
                "    <input type=\"submit\" value=\"Total carrying capacity\">\n" +
                "</form>\n" +"<br>"+
                "<form action=\"/airline\" method=\"POST\">\n" +
                "    <input type=\"hidden\" name=\"command\" value=\"SORT_BY_TYPE_AND_NAME\">\n" +
                "    <input type=\"submit\" value=\"Sort by name and type\">\n" +
                "</form>\n" +"<br>"+
                "<form action=\"/airline\" method=\"POST\">\n" +
                "    <input type=\"hidden\" name=\"command\" value=\"SORT_BY_CAPACITY\">\n" +
                "    <input type=\"submit\" value=\"Sort by capacity\">\n" +
                "</form>";
    }
    static String getDefaultResources(){
        return "<form action=\"/airline\" method=\"POST\">\n" +
                "    <label for=\"value\">Input your filepath:</label>\n" +
                "    <input type=\"text\" id=\"value\" name=\"value\" required><br>\n" +
                "    <input type=\"hidden\" name=\"command\" value=\"READ_FROM_FILE\">\n" +
                "    <input type=\"submit\" value=\"Read\">\n" +
                "</form>";
    }

}
