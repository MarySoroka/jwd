package com.airlines.controller;

import com.airlines.command.CommandService;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import java.io.*;
import java.net.URI;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.airlines.ApplicationConstants.DEFAULT_PAGE;

public class ServerHandler implements HttpHandler {
    private CommandService commandService;

    public ServerHandler(CommandService commandService) {
        this.commandService = commandService;

    }

    private static final Log LOGGER = LogFactory.getLog(ServerHandler.class);

    @Override
    public void handle(com.sun.net.httpserver.HttpExchange exchange) throws IOException {
        getRequest(exchange);
        Map<String, String> responseData = getResponseData(exchange.getRequestBody());
        StringBuilder commandExecution = new StringBuilder();
        commandExecution.append(commandService.execute(responseData));
        String resources = getResources();
        String view = MessageFormat.format(resources, commandExecution);
        sendResponse(exchange, view);
    }

    private void sendResponse(HttpExchange exchange, String view) throws IOException {
        OutputStream responseBody = exchange.getResponseBody();
        exchange.sendResponseHeaders(200, view.length());
        responseBody.write(view.getBytes());
        responseBody.flush();
        responseBody.close();
}

    private String getResources() {
        InputStream resourceAsStream = this.getClass().getResourceAsStream(DEFAULT_PAGE);
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resourceAsStream));
        return bufferedReader.lines().collect(Collectors.joining());
    }

    private void getRequest(HttpExchange exchange) {
        String requestMethod = exchange.getRequestMethod();
        URI requestURI = exchange.getRequestURI();
        LOGGER.info("Received incoming request: " + requestMethod + " URI " + requestURI.toString());
        LOGGER.info(requestURI.getQuery());
    }

    public Map<String, String> getResponseData(InputStream responseData) throws IOException {
        Map<String, String> dataMap = new HashMap<>();
        if (responseData.available() == 0) {
            dataMap.put("", "");
            return dataMap;
        }
        final InputStreamReader reader = new InputStreamReader(responseData, StandardCharsets.UTF_8);
        final Stream<String> data = new BufferedReader(reader).lines();
        final String dataForDecode = data.collect(Collectors.joining());
        String inputData = URLDecoder.decode(dataForDecode, StandardCharsets.UTF_8.name());
        LOGGER.info("Received data " + inputData);
        List<String> parsedData = Arrays.asList(inputData.split("(&)|(=)"));
        for (int i = 0; i < parsedData.size(); i += 2) {
            dataMap.put(parsedData.get(i), parsedData.get(i + 1));
        }
        return dataMap;
    }
}
