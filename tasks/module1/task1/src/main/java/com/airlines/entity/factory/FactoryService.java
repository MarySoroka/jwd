package com.airlines.entity.factory;

import com.airlines.entity.Airplane;
import com.airlines.entity.AirplaneType;

public interface FactoryService {
    Airplane getAirplane(AirplaneType type, Long id, String name, String pointOfDeparture, String destination, String specificData, Long capacity, Long carryingCapacity);
}
