package com.airlines.comparator;

import com.airlines.entity.Airplane;

import java.util.Comparator;

public class AirplaneTypeComparator implements Comparator<Airplane> {

    @Override
    public int compare(Airplane a1, Airplane a2) {
        return a1.getType().getType().compareTo(a2.getType().getType());
    }

}
