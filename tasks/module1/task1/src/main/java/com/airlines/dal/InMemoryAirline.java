package com.airlines.dal;

import com.airlines.entity.Airplane;

import java.util.Comparator;
import java.util.List;

public interface InMemoryAirline {
    List<Airplane> getAllAirplanes();
    List<Airplane> readFromFileAirplanes(String data);
    List<Airplane> sort(Comparator<? super Airplane> comparator);}
