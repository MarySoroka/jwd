package com.airlines.entity.factory;

import com.airlines.entity.AerialWorkAirplane;
import com.airlines.entity.Airplane;
import com.airlines.entity.AirplaneType;


public class AerialWorkAirplaneFactory implements AirplaneFactory {
    @Override
    public Airplane createAirplane(AirplaneType type, Long id, String name, String pointOfDeparture, String destination, String specificData, Long capacity, Long carryingCapacity) {
        return new AerialWorkAirplane(type, id, name, pointOfDeparture, destination, specificData, capacity,  carryingCapacity);

    }
}
