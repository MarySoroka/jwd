package com.airlines.entity;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public enum AirplaneType {
    CARGO("cargo"),
    WORK("aerial work"),
    PASSENGER("passenger");


    public String getType() {
        return type;
    }

    private static final Log LOGGER = LogFactory.getLog(AirplaneType.class);

    private String type;

    AirplaneType(String type) {
        this.type = type;
    }

    public static AirplaneType getByName(String name) {
        for (AirplaneType t :
                AirplaneType.values()) {
            if (t.type.equals(name)) {
                return t;
            }
        }
        LOGGER.error("can't find type by name");
        return null;
    }
}
