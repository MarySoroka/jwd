package com.airlines.validator;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Map;
import java.util.Objects;

@RunWith(JUnit4.class)
public class FileValidatorTest extends Assert {
    private FileValidator fileValidator = new FileValidator();

    @Test
    public void isFileDataValidWorkRight() {
        String path = this.getClass().getResource("/validData.csv").getPath();
        Map<Boolean, String> validation = fileValidator.isFileDataValid(path);
        assertTrue(validation.containsKey(true));
        System.out.println(validation.get(true));
    }

    @Test
    public void isPathValidWorkRight() {
        String path = this.getClass().getResource("/validData.csv").getPath();
        Map<Boolean, String> validation = fileValidator.isFileDataValid(path);
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isFileDataValid("/hom/");
        assertTrue(validation.containsKey(false));
        validation = fileValidator.isFileDataValid("/home/Deskt");
        assertTrue(validation.containsKey(false));


    }

    @Test
    public void isIdValidWorkRight() {
        Map<Boolean, String> validation = fileValidator.isNumberValid("1");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isNumberValid("fff");
        assertTrue(validation.containsKey(false));
    }

    @Test
    public void isTypeValidWorkRight() {
        Map<Boolean, String> validation = fileValidator.isTypeValid("cargo");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isTypeValid("passenger");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isTypeValid("aerial work");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isTypeValid("ggg");
        assertTrue(validation.containsKey(false));
    }

    @Test
    public void isCargoTypeValidWorkRight() {
        Map<Boolean, String> validation = fileValidator.isCargoTypeValid("animate");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isCargoTypeValid("inanimate");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isCargoTypeValid("jjj");
        assertTrue(validation.containsKey(false));
    }

    @Test
    public void isAerialWorkTypeValidWorkRight() {
        Map<Boolean, String> validation = fileValidator.isAerialWorkTypeValid("shooting");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isAerialWorkTypeValid("medical");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isAerialWorkTypeValid("chemical");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isAerialWorkTypeValid("sjjj");
        assertTrue(validation.containsKey(false));
    }

    @Test
    public void isSpecificDataValidWorkRight() {
        Map<Boolean, String> validation = fileValidator.isSpecificDataValid("cargo", "animate");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isSpecificDataValid("cargo", "inanimate");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isSpecificDataValid("cargo", "alkle");
        assertTrue(validation.containsKey(false));
        validation = fileValidator.isSpecificDataValid("passenger", "100");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isSpecificDataValid("passenger", "animate");
        assertTrue(validation.containsKey(false));
        validation = fileValidator.isSpecificDataValid("aerial work", "medical");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isSpecificDataValid("aerial work", "shooting");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isSpecificDataValid("aerial work", "chemical");
        assertTrue(validation.containsKey(true));
        validation = fileValidator.isSpecificDataValid("aerial work", "hkj");
        assertTrue(validation.containsKey(false));
    }


}
