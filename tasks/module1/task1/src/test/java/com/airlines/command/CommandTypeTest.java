package com.airlines.command;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CommandTypeTest extends Assert {
    @Test
    public void getByNameCommandTypeReadFromFileIsRight() {
        String type = "READ_FROM_FILE";
        CommandType commandType = null;
        for (CommandType a : CommandType.values()) {
            if (a.getType().equals(type)) {
                commandType = a;
            }
        }
        CommandType commandTypeTest = CommandType.getByName(type);
        assertEquals(commandType, commandTypeTest);
    }
    @Test
    public void getByNameCommandTypeDefaultIsRight() {
        String type = "DEFAULT";
        CommandType commandType = null;
        for (CommandType a : CommandType.values()) {
            if (a.getType().equals(type)) {
                commandType = a;
            }
        }
        CommandType commandTypeTest = CommandType.getByName(type);
        assertEquals(commandType, commandTypeTest);
    }

    @Test
    public void getByNameCommandTypeSortByCapacityIsRight() {
        String type = "SORT_BY_CAPACITY";
        CommandType commandType = null;
        for (CommandType a : CommandType.values()) {
            if (a.getType().equals(type)) {
                commandType = a;
            }
        }
        CommandType commandTypeTest = CommandType.getByName(type);
        assertEquals(commandType, commandTypeTest);
    }

    @Test
    public void getByNameCommandTypeSortByTypeAndNameIsRight() {
        String type = "SORT_BY_TYPE_AND_NAME";
        CommandType commandType = null;
        for (CommandType a : CommandType.values()) {
            if (a.getType().equals(type)) {
                commandType = a;
            }
        }
        CommandType commandTypeTest = CommandType.getByName(type);
        assertEquals(commandType, commandTypeTest);
    }
    @Test
    public void getByNameCommandTypeGetTotalCapacityIsRight() {
        String type = "TOTAL_CAPACITY";
        CommandType commandType = null;
        for (CommandType a : CommandType.values()) {
            if (a.getType().equals(type)) {
                commandType = a;
            }
        }
        CommandType commandTypeTest = CommandType.getByName(type);
        assertEquals(commandType, commandTypeTest);
    }
    @Test
    public void getByNameCommandTypeGetTotalCarryingCapacityIsRight() {
        String type = "TOTAL_CARRYING_CAPACITY";
        CommandType commandType = null;
        for (CommandType a : CommandType.values()) {
            if (a.getType().equals(type)) {
                commandType = a;
            }
        }
        CommandType commandTypeTest = CommandType.getByName(type);
        assertEquals(commandType, commandTypeTest);
    }
    @Test
    public void getByNameCommandTypeInvalidIsRight() {
        String type = ",";
        CommandType commandType = CommandType.DEFAULT;
        for (CommandType a : CommandType.values()) {
            if (a.getType().equals(type)) {
                commandType = a;
            }
        }
        CommandType commandTypeTest = CommandType.getByName(type);
        assertEquals(commandType, commandTypeTest);
    }
}
