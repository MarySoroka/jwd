package com.airlines.controller;

import com.airlines.command.*;
import com.airlines.validator.FileValidator;
import com.airlines.dal.InMemoryAirlineImpl;
import com.airlines.entity.Airplane;
import com.airlines.entity.AirplaneType;
import com.airlines.entity.factory.*;
import com.airlines.service.AirlineServiceImpl;
import com.airlines.service.AirlineService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;

@RunWith(JUnit4.class)
public class CommandControllerTest extends Assert {
    private CommandService commandService;
    private List<Airplane> airplanes = new ArrayList<>();
    private FactoryService factoryService;
    @Before
    public void prepareCommandsMap() {
        Map<AirplaneType, AirplaneFactory> airplaneFactoryMap = new EnumMap<AirplaneType, AirplaneFactory>(AirplaneType.class);
        airplaneFactoryMap.put(AirplaneType.PASSENGER, new PassengerAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.CARGO, new CargoAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.WORK, new AerialWorkAirplaneFactory());
        factoryService = new Factory(airplaneFactoryMap);
        InMemoryAirlineImpl dao = new InMemoryAirlineImpl(factoryService);

        AirlineService airlineService = new AirlineServiceImpl(dao);


        Map<CommandType, Command> commandMap = new EnumMap<>(CommandType.class);
        commandMap.put(CommandType.READ_FROM_FILE, new ReadCommand(airlineService));
        commandMap.put(CommandType.DEFAULT, new DefaultCommand());
        commandService = new CommandController(commandMap);
    }

    private void initialization(String data) {
        String[] airplaneList = data.split(",");
        for (int i = 0; i < airplaneList.length; i += 8) {
            airplanes.add(factoryService.getAirplane(AirplaneType.getByName(airplaneList[i + 1]),
                    Long.valueOf(airplaneList[i]), airplaneList[i + 2], airplaneList[i + 3],
                    airplaneList[i + 4], airplaneList[i + 5], Long.parseLong(airplaneList[i + 6]), Long.parseLong(airplaneList[i + 7])));
        }


    }
    @Test
    public void executionOfTheReadCommandByUsingControllerRight() {
        Map<String, String> map = new HashMap<>();
        map.put("command", "READ_FROM_FILE");
        String path = this.getClass().getResource("/validData.csv").getPath();
        map.put("value", path);
        String test = commandService.execute(map);
        initialization((new FileValidator().isFileDataValid(map.get("value"))).get(true));
        String commandResult = Command.getResources() + Command.fromListToString(airplanes).toString();
        assertEquals(commandResult, test);
    }
    @Test
    public void executionOfTheDefaultCommandByUsingControllerRight() {
        Map<String, String> map = new HashMap<>();
        map.put("command", "DEFAULT");
        map.put("value", "");
        String test = commandService.execute(map);
        String commandResult = Command.getDefaultResources()  ;
        assertEquals(commandResult, test);
    }

}
