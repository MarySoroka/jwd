package com.airlines.dal;

import com.airlines.entity.Airplane;
import com.airlines.entity.AirplaneType;
import com.airlines.entity.PassengerAirplane;
import com.airlines.entity.factory.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;

@RunWith(JUnit4.class)
public class InMemoryAirlineServiceImplImplTest extends Assert {
    private FactoryService factory;
    private InMemoryAirline dao;
    @Before
    public void prepareFactoryForTest(){
        Map<AirplaneType, AirplaneFactory> airplaneFactoryMap = new EnumMap<AirplaneType, AirplaneFactory>(AirplaneType.class);
        airplaneFactoryMap.put(AirplaneType.PASSENGER, new PassengerAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.CARGO, new CargoAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.WORK, new AerialWorkAirplaneFactory());
        factory = new Factory(airplaneFactoryMap);
        dao = new InMemoryAirlineImpl(factory);
    }
    @Test
    public void initializationOfAirplanesIsRight(){
      String test = String.valueOf(init());
      String daoResult = String.valueOf(dao.readFromFileAirplanes("1,passenger,Typhim Vi,Wanmao,Nicoya,300,332,232"));
      assertEquals(test,daoResult);
    }
    private List<Airplane> init(){
        List<Airplane> airplanes = new ArrayList<>();
        String[] airplaneList = "1,passenger,Typhim Vi,Wanmao,Nicoya,300,332,232".split(",");
        for (int i = 0; i < airplaneList.length; i += 8) {
            airplanes.add(factory.getAirplane(AirplaneType.getByName(airplaneList[i + 1]),
                    Long.valueOf(airplaneList[i]), airplaneList[i + 2], airplaneList[i + 3],
                    airplaneList[i + 4], airplaneList[i + 5], Long.parseLong(airplaneList[i + 6]), Long.parseLong(airplaneList[i + 7])));
        }
        return airplanes;
    }
    @Test
    public void getAllAirplanes(){
        String test ="["+ String.valueOf(new PassengerAirplane(AirplaneType.PASSENGER, 1L, "Typhim Vi","Wanmao","Nicoya","300",332L,232L))+"]";
        dao.readFromFileAirplanes("1,passenger,Typhim Vi,Wanmao,Nicoya,300,332,232");
        String daoResult = String.valueOf(dao.getAllAirplanes());
        assertEquals(test,daoResult);
    }
}
