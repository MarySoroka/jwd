package com.airlines.entity.airline;

import com.airlines.entity.AirplaneType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class AirplaneTypeTest extends Assert {
    @Test
    public void getByNameAirplaneTypePassengerIsRight() {
        String type = "passenger";
        AirplaneType airplaneType = null;
        for (AirplaneType a : AirplaneType.values()) {
            if (a.getType().equals(type)) {
                airplaneType = a;
            }
        }
        AirplaneType airplaneTypeTest = AirplaneType.getByName(type);
        assertEquals(airplaneType, airplaneTypeTest);
    }

    @Test
    public void getByNameAirplaneTypeCargoIsRight() {
        String type = "cargo";
        AirplaneType airplaneType = null;
        for (AirplaneType a : AirplaneType.values()) {
            if (a.getType().equals(type)) {
                airplaneType = a;
            }
        }
        AirplaneType airplaneTypeTest = AirplaneType.getByName(type);
        assertEquals(airplaneType, airplaneTypeTest);
    }

    @Test
    public void getByNameAirplaneTypeAerialWorkIsRight() {
        String type = "aerial work";
        AirplaneType airplaneType = null;
        for (AirplaneType a : AirplaneType.values()) {
            if (a.getType().equals(type)) {
                airplaneType = a;
            }
        }
        AirplaneType airplaneTypeTest = AirplaneType.getByName(type);
        assertEquals(airplaneType, airplaneTypeTest);
    }
}
