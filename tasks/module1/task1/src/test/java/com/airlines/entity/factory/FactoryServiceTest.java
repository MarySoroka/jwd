package com.airlines.entity.factory;

import com.airlines.entity.AirplaneType;
import com.airlines.entity.AerialWorkAirplane;
import com.airlines.entity.CargoAirplane;
import com.airlines.entity.PassengerAirplane;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.EnumMap;
import java.util.Map;

@RunWith(JUnit4.class)
public class FactoryServiceTest extends Assert {
    private FactoryService factoryService;
    private String pointOfDeparture;
    private String destination;
    private Long id;
    private Long capacity;
    private Long carryingCapacity;
    @Before
    public void setFactory(){
        Map<AirplaneType, AirplaneFactory> airplaneFactoryMap = new EnumMap<AirplaneType, AirplaneFactory>(AirplaneType.class);
        airplaneFactoryMap.put(AirplaneType.PASSENGER, new PassengerAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.CARGO, new CargoAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.WORK, new AerialWorkAirplaneFactory());
        factoryService = new Factory(airplaneFactoryMap);
        id = 2L;
        pointOfDeparture = "point";
        destination = "destination";
        capacity = 100L;
        carryingCapacity = 77L;
    }
    @Test
    public void creationByFactoringPassengerAirplaneShouldBeRight(){
        AirplaneType value = AirplaneType.values()[2];
        String specificData = "10";

        PassengerAirplane airplaneTest = (PassengerAirplane) factoryService.getAirplane(value, id, value.toString(), pointOfDeparture, destination, specificData, capacity, carryingCapacity);
        PassengerAirplane airplane = new PassengerAirplane(value, id, value.toString(), pointOfDeparture, destination, specificData, capacity, carryingCapacity);
        assertEquals(airplane.getId(), airplaneTest.getId());
        assertEquals(airplane.getName(), airplaneTest.getName());
        assertEquals(airplane.getDestination(), airplaneTest.getDestination());
        assertEquals(airplane.getPointOfDeparture(), airplaneTest.getPointOfDeparture());
        assertEquals(airplane.getType(), airplaneTest.getType());
        assertEquals(airplane.getMaxAmountOfPassengers(), airplaneTest.getMaxAmountOfPassengers());
    }

    @Test
    public void creationByFactoringCargoAirplaneShouldBeRight(){
        AirplaneType value = AirplaneType.values()[0];
        String specificData = "animate";
        CargoAirplane airplaneTest = (CargoAirplane) factoryService.getAirplane(value, id, value.toString(), pointOfDeparture, destination, specificData,capacity,carryingCapacity);
        CargoAirplane airplane = new CargoAirplane(value, id, value.toString(), pointOfDeparture, destination, specificData,capacity,carryingCapacity);
        assertEquals(airplane.getId(), airplaneTest.getId());
        System.out.println(id);
        assertEquals(airplane.getName(), airplaneTest.getName());
        assertEquals(airplane.getDestination(), airplaneTest.getDestination());
        assertEquals(airplane.getPointOfDeparture(), airplaneTest.getPointOfDeparture());
        assertEquals(airplane.getType(), airplaneTest.getType());
        assertEquals(airplane.getCargoType(), airplaneTest.getCargoType());
    }

    @Test
    public void creationByFactoringAerialWorkAirplaneShouldBeRight(){
        AirplaneType value = AirplaneType.values()[1];
        String specificData = "medical";
        AerialWorkAirplane airplaneTest = (AerialWorkAirplane) factoryService.getAirplane(value, id, value.toString(), pointOfDeparture, destination, specificData,capacity,carryingCapacity);
        AerialWorkAirplane airplane = new AerialWorkAirplane(value, id, value.toString(), pointOfDeparture, destination, specificData,capacity,carryingCapacity);
        assertEquals(airplane.getId(), airplaneTest.getId());
        assertEquals(airplane.getName(), airplaneTest.getName());
        assertEquals(airplane.getDestination(), airplaneTest.getDestination());
        assertEquals(airplane.getPointOfDeparture(), airplaneTest.getPointOfDeparture());
        assertEquals(airplane.getType(), airplaneTest.getType());
        assertEquals(airplane.getAerialWorkType(), airplaneTest.getAerialWorkType());
    }
}
