package com.airlines.command;

import com.airlines.validator.FileValidator;
import com.airlines.dal.InMemoryAirlineImpl;
import com.airlines.entity.Airplane;
import com.airlines.entity.AirplaneType;
import com.airlines.entity.factory.*;
import com.airlines.service.AirlineServiceImpl;
import com.airlines.service.AirlineService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;
@RunWith(JUnit4.class)
public class GetTotalCapacityCommandTest extends Assert {
    private AirlineService airlineService;
    private Map<String, String> map = new HashMap<>();
    private InMemoryAirlineImpl dao;

    @Before
    public void prepareAirlineService() {
        Map<AirplaneType, AirplaneFactory> airplaneFactoryMap = new EnumMap<AirplaneType, AirplaneFactory>(AirplaneType.class);
        airplaneFactoryMap.put(AirplaneType.PASSENGER, new PassengerAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.CARGO, new CargoAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.WORK, new AerialWorkAirplaneFactory());
        FactoryService factoryService = new Factory(airplaneFactoryMap);
        dao = new InMemoryAirlineImpl(factoryService);
        airlineService = new AirlineServiceImpl(dao);
        Command command = new ReadCommand(airlineService);
        String path = this.getClass().getResource("/validData.csv").getPath();
        map.put("value", path);
        command.execute(map);
    }

    @Test
    public void executionOfTheGetTotalCapacityCommandIsRight() {
        Command command = new GetTotalCapacityCommand(airlineService);
        String commandResult = command.execute(map);

        Long capacity  = getCapacity();
        List<Airplane> airplane = airlineService.getAllAirplanes();

        String test = Command.getResources()+"Total capacity "+capacity +"<br>"+String.valueOf(Command.fromListToString(airplane));
        assertEquals(commandResult, test);
    }

    public Long getCapacity(){
        long capacity = 0;
        for (Airplane a :
                dao.getAllAirplanes()) {
            capacity += a.getCapacity();
        }
        return capacity;
    }
}
