package com.airlines.command;

import com.airlines.comparator.AirplaneTypeComparator;
import com.airlines.comparator.NameComparator;
import com.airlines.validator.FileValidator;
import com.airlines.dal.InMemoryAirlineImpl;
import com.airlines.entity.Airplane;
import com.airlines.entity.AirplaneType;
import com.airlines.entity.factory.*;
import com.airlines.service.AirlineServiceImpl;
import com.airlines.service.AirlineService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;
@RunWith(JUnit4.class)
public class SortByTypeAndNameCommandTest extends Assert {
    private AirlineService airlineService;
    private Map<String, String> map = new HashMap<>();

    @Before
    public void prepareAirlineService() {
        Map<AirplaneType, AirplaneFactory> airplaneFactoryMap = new EnumMap<AirplaneType, AirplaneFactory>(AirplaneType.class);
        airplaneFactoryMap.put(AirplaneType.PASSENGER, new PassengerAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.CARGO, new CargoAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.WORK, new AerialWorkAirplaneFactory());
        FactoryService factoryService = new Factory(airplaneFactoryMap);
        InMemoryAirlineImpl dao = new InMemoryAirlineImpl(factoryService);
        airlineService = new AirlineServiceImpl(dao);
        Command command = new ReadCommand(airlineService);
        String path = this.getClass().getResource("/validData.csv").getPath();
        map.put("value", path);
        command.execute(map);
    }
    @Test
    public void executionOfTheSortByTypeAndNameCommandIsRight() {
        Command command = new SortByTypeAndNameCommand(airlineService);
        String commandResult = command.execute(map);
        AirplaneTypeComparator airplaneTypeComparator = new AirplaneTypeComparator();
        Comparator<Airplane> comparator = airplaneTypeComparator.thenComparing(new NameComparator());
        List<Airplane> airplane = airlineService.getAllAirplanes();
        airplane.sort(comparator);
        String test = Command.getResources() + Command.fromListToString(airplane);
        assertEquals(commandResult, test);
    }



}
