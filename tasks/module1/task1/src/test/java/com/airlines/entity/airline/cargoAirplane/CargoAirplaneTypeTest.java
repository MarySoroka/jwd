package com.airlines.entity.airline.cargoAirplane;

import com.airlines.entity.CargoType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CargoAirplaneTypeTest extends Assert {
    @Test
    public void getByNameCargoAirplaneTypeAnimateIsRight() {
        String type = "animate";
        CargoType cargoType = null;
        for (CargoType a : CargoType.values()) {
            if (a.getType().equals(type)) {
                cargoType = a;
            }
        }
        CargoType cargoTypeTest = CargoType.getByName(type);
        assertEquals(cargoType,cargoTypeTest);
    }
    @Test
    public void getByNameCargoAirplaneTypeInanimateIsRight() {
        String type = "inanimate";
        CargoType cargoType = null;
        for (CargoType a : CargoType.values()) {
            if (a.getType().equals(type)) {
                cargoType = a;
            }
        }
        CargoType cargoTypeTest = CargoType.getByName(type);
        assertEquals(cargoType,cargoTypeTest);
    }
}
