package com.airlines.command;

import com.airlines.validator.FileValidator;
import com.airlines.dal.InMemoryAirlineImpl;
import com.airlines.entity.Airplane;
import com.airlines.entity.AirplaneType;
import com.airlines.entity.factory.*;
import com.airlines.service.AirlineServiceImpl;
import com.airlines.service.AirlineService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;

@RunWith(JUnit4.class)
public class ReadCommandTest extends Assert {
    private AirlineService airlineService;
    private List<Airplane> airplanes = new ArrayList<>();
    private Map<String, String> map = new HashMap<>();
    private FactoryService factoryService;

    @Before
    public void prepareAirlineService() {
        Map<AirplaneType, AirplaneFactory> airplaneFactoryMap = new EnumMap<AirplaneType, AirplaneFactory>(AirplaneType.class);
        airplaneFactoryMap.put(AirplaneType.PASSENGER, new PassengerAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.CARGO, new CargoAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.WORK, new AerialWorkAirplaneFactory());
        factoryService = new Factory(airplaneFactoryMap);
        InMemoryAirlineImpl dao = new InMemoryAirlineImpl(factoryService);
        airlineService = new AirlineServiceImpl(dao);
    }

    private void initialization(String data) {
        String[] airplaneList = data.split(",");
        for (int i = 0; i < airplaneList.length; i += 8) {
            this.airplanes.add(factoryService.getAirplane(AirplaneType.getByName(airplaneList[i + 1]),
                    Long.valueOf(airplaneList[i]), airplaneList[i + 2],
                    airplaneList[i + 3], airplaneList[i + 4], airplaneList[i + 5],Long.parseLong(airplaneList[i+6]),Long.parseLong(airplaneList[i+7])));
        }


    }

    @Test
    public void executionOfTheReadCommandIsRight() {
        Command command = new ReadCommand(airlineService);
        String path = this.getClass().getResource("/validData.csv").getPath();
        map.put("value", path);
        String commandResult = command.execute(map);
        initialization((new FileValidator().isFileDataValid(map.get("value"))).get(true));
        String test = Command.getResources()+Command.fromListToString(airplanes).toString();
        assertEquals(commandResult, test);
    }
    @Test
    public void executionOfTheReadCommandWithInvalidDataIsRight() {
        Command command = new ReadCommand(airlineService);
        String path = this.getClass().getResource("/invalidData.csv").getPath();
        map.put("value", path);
        String commandResult = command.execute(map);
        String test = Command.getDefaultResources()+  new FileValidator().isFileDataValid(map.get("value")).get(false);
        assertEquals(commandResult, test);
    }
    @Test
    public void executionOfTheReadCommandWithInvalidFormatIsRight() {
        Command command = new ReadCommand(airlineService);
        String path = this.getClass().getResource("/invalidFormat.txt").getPath();
        map.put("value", path);
        String commandResult = command.execute(map);
        String test = Command.getDefaultResources() + new FileValidator().isFileDataValid(map.get("value")).get(false);
        assertEquals(commandResult, test);
    }

}
