package com.airlines.command;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;
@RunWith(JUnit4.class)
public class DefaultCommandTest extends Assert {
    @Test
    public void executionOfTheDefaultCommandIsRight() {
        Map<String, String> map = new HashMap<>();
        map.put("command","DEFAULT");
        String test = " ";
        assertEquals(new DefaultCommand().execute(map), test);
    }

}
