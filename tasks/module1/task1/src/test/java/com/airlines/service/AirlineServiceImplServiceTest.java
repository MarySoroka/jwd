package com.airlines.service;

import com.airlines.command.Command;
import com.airlines.validator.FileValidator;
import com.airlines.command.ReadCommand;
import com.airlines.dal.InMemoryAirlineImpl;
import com.airlines.entity.Airplane;
import com.airlines.entity.AirplaneType;
import com.airlines.entity.factory.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.*;

@RunWith(JUnit4.class)
public class AirlineServiceImplServiceTest extends Assert {
    private AirlineService airlineService;
    private Map<String, String> map = new HashMap<>();
    private InMemoryAirlineImpl dao;

    @Before
    public void prepareAirlineService() {
        Map<AirplaneType, AirplaneFactory> airplaneFactoryMap = new EnumMap<AirplaneType, AirplaneFactory>(AirplaneType.class);
        airplaneFactoryMap.put(AirplaneType.PASSENGER, new PassengerAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.CARGO, new CargoAirplaneFactory());
        airplaneFactoryMap.put(AirplaneType.WORK, new AerialWorkAirplaneFactory());
        FactoryService factoryService = new Factory(airplaneFactoryMap);
        dao = new InMemoryAirlineImpl(factoryService);
        airlineService = new AirlineServiceImpl(dao);
        Command command = new ReadCommand(airlineService);
        String path = this.getClass().getResource("/validData.csv").getPath();
        map.put("value", path);
        command.execute(map);
    }
    @Test
    public void getAllAirplanesIsRight() {
        assertEquals(dao.getAllAirplanes(),airlineService.getAllAirplanes());
    }
    @Test
    public void ReadFromFileIsRight() {
        assertEquals(dao.readFromFileAirplanes("1,passenger,Typhim Vi,Wanmao,Nicoya,300,332,232"),
                airlineService.readFromFileAirplanes("1,passenger,Typhim Vi,Wanmao,Nicoya,300,332,232"));
    }
    @Test
    public void sortIsRight() {
        Comparator<Airplane> comparator = Comparator.comparing(Airplane::getCapacity);
        assertEquals(dao.sort(comparator),airlineService.sort(comparator));
    }
    @Test
    public void getCarryingCapacityIsRight() {
        airlineService.readFromFileAirplanes("1,passenger,Typhim Vi,Wanmao,Nicoya,300,332,232");
        assertEquals(airlineService.getTotalCarryingCapacity(), getCarryingCapacity());
        assertEquals(dao.getAllAirplanes(),airlineService.getAllAirplanes());
    }
    public Long getCarryingCapacity(){
        long capacity = 0;
        for (Airplane a :
                dao.getAllAirplanes()) {
            capacity += a.getCarryingCapacity();
        }
        return capacity;
    }
    @Test
    public void getCapacityIsRight() {
        airlineService.readFromFileAirplanes("1,passenger,Typhim Vi,Wanmao,Nicoya,300,332,232");
        assertEquals(airlineService.getTotalCapacity(), getCapacity());
        assertEquals(dao.getAllAirplanes(),airlineService.getAllAirplanes());
    }
    public Long getCapacity(){
        long capacity = 0;
        for (Airplane a :
                dao.getAllAirplanes()) {
            capacity += a.getCapacity();
        }
        return capacity;
    }
}

