package com.airlines.entity.airline.aerialWorkAirplane;

import com.airlines.entity.AerialWorkType;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class AerialWorkTypeTest extends Assert {
    @Test
    public void getByNameAerialWorkAirplaneTypeMedicalIsRight() {
        String type = "medical";
        AerialWorkType aerialWorkType = null;
        for (AerialWorkType a : AerialWorkType.values()) {
            if (a.getType().equals(type)) {
                aerialWorkType = a;
            }
        }
        AerialWorkType aerialWorkTypeTest = AerialWorkType.getByName(type);
        assertEquals(aerialWorkType,aerialWorkTypeTest);
    }
    @Test
    public void getByNameAerialWorkAirplaneTypeChemicalIsRight() {
        String type = "chemical";
        AerialWorkType aerialWorkType = null;
        for (AerialWorkType a : AerialWorkType.values()) {
            if (a.getType().equals(type)) {
                aerialWorkType = a;
            }
        }
        AerialWorkType aerialWorkTypeTest = AerialWorkType.getByName(type);
        assertEquals(aerialWorkType,aerialWorkTypeTest);
    }
    @Test
    public void getByNameAerialWorkAirplaneTypeShootingIsRight() {
        String type = "shooting";
        AerialWorkType aerialWorkType = null;
        for (AerialWorkType a : AerialWorkType.values()) {
            if (a.getType().equals(type)) {
                aerialWorkType = a;
            }
        }
        AerialWorkType aerialWorkTypeTest = AerialWorkType.getByName(type);
        assertEquals(aerialWorkType,aerialWorkTypeTest);
    }
}
