<%--
  Created by IntelliJ IDEA.
  User: maria
  Date: 5/12/20
  Time: 7:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page import="com.vouchers.command.CommandType" %>
<!DOCTYPE html>
<html>

<head>
    <title>Vouchers</title>
</head>

<body>
<c:if test="${empty voucherList}">
    <c:if test="${not empty errors}">
        <c:forEach items="${errors}" var="error">
            <p style="color: red">${error.errors}</p>
            <p style="color: red">${error.field}</p>
            <p style="color: red">${error.value}</p>

        </c:forEach>
    </c:if>
</c:if>
<div>
    <c:if test="${not empty voucherList}">
        <form action="${pageContext.request.contextPath}/" method="post" enctype="multipart/form-data">
            <input type="hidden" name="command" value="${CommandType.DOWNLOAD}">
            <button type="submit" name="${CommandType.DOWNLOAD}">Download</button>
        </form>
    <table border="1" align="center" width="100%" cellpadding="5">
        <tr bgcolor="#a9a9a9">
            <th id="id">Id</th>
            <th id="date">Date</th>
            <th id="type">Voucher type</th>
            <th id="country">Country</th>
            <th id="transport">Transport type</th>
            <th id="days">Days</th>
            <th id="nights">Nights</th>
            <th id="hotelCharacteristics" colspan="7">Hotel Characteristics</th>
            <th id="voucherCost" colspan="5">Voucher Cost</th>
        </tr>
        <tr bgcolor="#a9a9a9">
            <th colspan="7"></th>
            <th id="stars">Hotel stars</th>
            <th id="nutrition">Nutrition</th>
            <th id="room">RoomType</th>
            <th>TV</th>
            <th>Balcony</th>
            <th>Air Condition</th>
            <th>Hair Dryer</th>
            <th id="Cost">Cost</th>
            <th colspan="4">Included characteristics</th>
        </tr>
        <c:forEach items="${voucherList}" var="voucher">
            <tr bgcolor="#d3d3d3">
                <td>${voucher.id}</td>
                <td>${voucher.date}</td>
                <td>${voucher.voucherType}</td>
                <td>${voucher.country}</td>
                <td>${voucher.transportType}</td>
                <td>${voucher.days}</td>
                <td>${voucher.nights}</td>
                <td>${voucher.hotel.hotelCharacteristic.get(0).amountOfStars}</td>
                <td>${voucher.hotel.hotelCharacteristic.get(1).nutritionType}</td>
                <td>${voucher.hotel.hotelCharacteristic.get(2).roomType}</td>
                <c:forEach items="${voucher.hotel.hotelCharacteristic.get(2).roomCharacteristicList}"
                           var="characteristics">
                    <td>${characteristics.availability}</td>
                </c:forEach>
                <td>${voucher.voucherCost.cost}</td>
                <c:forEach items="${voucher.voucherCost.includedCharacteristics}" var="included">
                    <td>${included}</td>
                </c:forEach>
            </tr>
        </c:forEach>

        </c:if>
    </table>
</div>
</body>

</html>