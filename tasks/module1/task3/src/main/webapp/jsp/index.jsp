<%--
  Created by IntelliJ IDEA.
  User: maria
  Date: 5/22/20
  Time: 4:31 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
<head>
    <title>Vouchers</title>
</head>
<body>
<form action="${pageContext.request.contextPath}/" method="post" enctype="multipart/form-data">
    <label for="xmlFile">Enter your filepath</label>
    <input type="file" id="xmlFile" name="xmlFile">
    <label for="parser">Choose parser:</label>
    <select id="parser" name="parser">
        <option value="sax">SAX</option>
        <option value="dom">DOM</option>
        <option value="stax">STAX</option>
    </select>
    <input type="hidden" name="command" value="upload">
    <button type="submit" name="parse">Parse</button>
</form>

<c:if test="${not empty errors}">
    <c:forEach items="${errors}" var="error">
        <c:forEach items="${error.errors}" var="e">
            <p style="color: red">${e}</p>
        </c:forEach>
    </c:forEach>
</c:if>
</body>
</html>
