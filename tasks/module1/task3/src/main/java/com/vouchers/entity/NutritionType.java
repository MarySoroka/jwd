package com.vouchers.entity;

import com.vouchers.parsers.ParserType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum NutritionType {
    HB("HB"),
    BB("BB"),
    AL("Al"),
    NONE("None");

    public String getType() {
        return type;
    }
    private static final Logger LOGGER = LogManager.getLogger(NutritionType.class);
    private String type;

    NutritionType(String type) {
        this.type = type;
    }

    public static NutritionType getByName(String nutritionType) {
        for (NutritionType nutritionType1 :
                NutritionType.values()) {
            if (nutritionType1.type.equalsIgnoreCase(nutritionType)) {
                return nutritionType1;
            }
        }
        LOGGER.error("Can't find nutrition type by name {}",nutritionType);
        return NutritionType.NONE;
    }
}
