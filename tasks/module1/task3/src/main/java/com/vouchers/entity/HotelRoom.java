package com.vouchers.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class HotelRoom extends HotelCharacteristic {
    private RoomType roomType;
    private List<RoomCharacteristic> roomCharacteristicList;

    public HotelRoom() {
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HotelRoom hotelRoom = (HotelRoom) o;
        return roomType == hotelRoom.roomType &&
                Objects.equals(roomCharacteristicList, hotelRoom.roomCharacteristicList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roomType, roomCharacteristicList);
    }

    public RoomType getRoomType() {
        return roomType;
    }

    public List<RoomCharacteristic> getRoomCharacteristicList() {
        return roomCharacteristicList;
    }

    @Override
    public String toString() {
        return "HotelRoom{" +
                "roomType=" + roomType +
                ", roomCharacteristicList=" + roomCharacteristicList +
                "} " + super.toString();
    }

    public void setRoomType(RoomType roomType) {
        this.roomType = roomType;
    }

    public void setRoomCharacteristicList(List<RoomCharacteristic> roomCharacteristicList) {
        this.roomCharacteristicList = roomCharacteristicList;
    }

    public HotelRoom(RoomType roomType, List<RoomCharacteristic> roomCharacteristicList) {
        this.roomType = roomType;
        this.roomCharacteristicList = roomCharacteristicList;
    }
    public HotelRoom(HotelRoom another) {
        this.roomType = another.roomType;
        this.setName(another.getName());
        List<RoomCharacteristic> roomCharacteristics = new ArrayList<>();
        for (RoomCharacteristic r:
             another.getRoomCharacteristicList()) {
            roomCharacteristics.add(new RoomCharacteristic(r.getName(),r.isAvailability()));
        }
        this.roomCharacteristicList = roomCharacteristics;
    }
}
