package com.vouchers.service;

import com.vouchers.builder.EntityBuilder;
import com.vouchers.entity.Voucher;
import com.vouchers.validation.Validator;

import java.util.List;

public interface VoucherService {
    List<Voucher> getAll();
    void save(Voucher voucher);
    String getXmlFile(EntityBuilder<Voucher> builder);
}
