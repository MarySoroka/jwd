package com.vouchers.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;

import static com.vouchers.application.ApplicationContext.SCHEMA_FILE;

public class XSDValidator extends com.vouchers.validation.Validator {
    private static final Logger LOGGER = LogManager.getLogger(XSDValidator.class);

    public XSDValidator(ValidationResult validationResult) {
        super.validationResult = validationResult;
    }

    public void validate(InputStream filepath) throws ValidatorException {
        try {
            Source xmlFile = new StreamSource(filepath);
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            schemaFactory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
            Schema schema = schemaFactory.newSchema(this.getClass().getResource(SCHEMA_FILE).toURI().toURL());
            javax.xml.validation.Validator validator = schema.newValidator();
            validator.validate(xmlFile);
        } catch (SAXException | IOException | URISyntaxException e) {
            LOGGER.error("Invalid data.Can't parse file ");
            this.validationResult.addMessage("", "", "Invalid data.Can't parse file " + e.getMessage());
            throw new ValidatorException("Invalid data.Can't parse file", e.fillInStackTrace());
        }

    }
}
