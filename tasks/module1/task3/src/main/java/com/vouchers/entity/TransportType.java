package com.vouchers.entity;

import com.vouchers.parsers.ParserType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum TransportType {
    AIRPLANE("airplane"),
    CAR("car"),
    BUS("bus"),
    LINER("liner"),
    TRAIN("train");

    public String getType() {
        return type;
    }
    private static final Logger LOGGER = LogManager.getLogger(TransportType.class);
    private String type;

    TransportType(String type) {
        this.type = type;
    }

    public static TransportType getByName(String t) {
        for (TransportType type :
                TransportType.values()) {
            if (type.type.equalsIgnoreCase(t)) {
                return type;
            }
        }
        LOGGER.error("Can't find transport type by name {}",t);
        return TransportType.CAR;
    }
}
