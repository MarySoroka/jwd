package com.vouchers.command;

public interface CommandProvider {
    void setCommand(CommandType commandType, AbstractAppCommand command);
    AbstractAppCommand getCommand(CommandType commandType);
    void deleteCommand(CommandType commandType);
}
