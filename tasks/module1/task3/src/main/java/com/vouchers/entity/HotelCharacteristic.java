package com.vouchers.entity;

public abstract class HotelCharacteristic {
    private String name;

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "HotelCharacteristic{" +
                "name='" + name + '\'' +
                '}';
    }

    public void setName(String name) {
        this.name = name;
    }
}
