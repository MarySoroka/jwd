package com.vouchers.entity;

import com.vouchers.parsers.ParserType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum VoucherType {
    CRUISE("cruise"),
    EXCURSION("excursion"),
    PILGRIMAGES("pilgrimages"),
    REST("rest");

    public String getType() {
        return type;
    }
    private static final Logger LOGGER = LogManager.getLogger(VoucherType.class);
    private String type;

    VoucherType(String type) {
        this.type = type;
    }

    public static VoucherType getByName(String t) {
        for (VoucherType type :
                VoucherType.values()) {
            if (type.type.equalsIgnoreCase(t)) {
                return type;
            }
        }
        LOGGER.error("Can't find this voucher type {}", t);
        return VoucherType.REST;

    }

}
