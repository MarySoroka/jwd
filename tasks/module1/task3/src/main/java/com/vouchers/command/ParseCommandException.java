package com.vouchers.command;

public class ParseCommandException extends Exception {
    public ParseCommandException() {
    }

    public ParseCommandException(String s) {
        super(s);
    }

    public ParseCommandException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ParseCommandException(Throwable throwable) {
        super(throwable);
    }

    public ParseCommandException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
