package com.vouchers.builder;

import com.vouchers.entity.Nutrition;
import com.vouchers.entity.NutritionType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class NutritionBuilder implements EntityBuilder<Nutrition> {
    private static final Logger LOGGER = LogManager.getLogger(NutritionBuilder.class);

    @Override
    public Nutrition build(Map<String, String> data) throws BuildException {
        try {
            LOGGER.info("Start build nutrition");
            Nutrition nutrition = new Nutrition();
            nutrition.setName(data.get(VoucherField.HOTEL_CHARACTERISTIC.getFieldName()+1));
            nutrition.setNutritionType(NutritionType.getByName(data.get(VoucherField.NUTRITION_TYPE.getFieldName())));
            return nutrition;
        } catch (Exception e) {
            LOGGER.error("Couldn't build nutrition. Fatal error. {}", e.getMessage());
            throw new BuildException("Couldn't build nutrition. Fatal error. ", e);
        }
    }

    @Override
    public String buildXml(Nutrition object) {
        return " <nutrition>\n" +
                "<nutrition-type>"+object.getNutritionType().getType()+"</nutrition-type>\n" +
                "</nutrition>";
    }
}
