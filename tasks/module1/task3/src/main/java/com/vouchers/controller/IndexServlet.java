package com.vouchers.controller;

import com.vouchers.application.ApplicationConstants;
import com.vouchers.application.ApplicationContext;
import com.vouchers.command.Command;
import com.vouchers.command.CommandProvider;
import com.vouchers.command.CommandType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@MultipartConfig
@WebServlet(name = "parse", urlPatterns = "/")
public class IndexServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(IndexServlet.class);
    private final ApplicationContext applicationContext;

    public IndexServlet() {
        applicationContext = ApplicationContext.getInstance();
    }

    @Override
    public void init() {
        LOGGER.info("Initialize servlet");
        applicationContext.initialization();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.info("Started doGet in servlet");
        String commandName = req.getParameter(ApplicationConstants.COMMAND_NAME_PARAM);
        CommandProvider commandProvider = (CommandProvider) applicationContext.getBean(CommandProvider.class);
        Command command = commandProvider.getCommand(CommandType.getByName(commandName));
        try {
            command.execute(req, resp);
        } catch (Exception e) {
            LOGGER.error("Failed to execute command ", e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        LOGGER.info("Started doPost in servlet");
        doGet(req, resp);
    }

    @Override
    public void destroy() {
        applicationContext.destroy();
    }

}
