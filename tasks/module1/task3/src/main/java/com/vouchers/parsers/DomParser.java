package com.vouchers.parsers;

import com.vouchers.builder.BuildException;
import com.vouchers.builder.EntityBuilder;
import com.vouchers.builder.VoucherField;
import com.vouchers.entity.VoucherType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DomParser<T> extends XMLParser<T> {
    private static final Logger LOGGER = LogManager.getLogger(DomParser.class);

    private final DocumentBuilderFactory factory;

    public DomParser(EntityBuilder<T> entityBuilder) {
        factory = DocumentBuilderFactory.newInstance();
        super.entityBuilder = entityBuilder;
        super.parsedData = new HashMap<>();
    }

    private static String getElementContent(Element element, String name) {
        NodeList nodeList = element.getElementsByTagName(name);
        Node node = nodeList.item(0);
        LOGGER.info("Element name {} and value {}", name, node.getTextContent());
        return node.getTextContent();
    }

    private T buildVoucher(Element voucherElement) throws BuildException {
        parsedData.put(VoucherField.ID.getFieldName(), voucherElement.getAttributes().getNamedItem(VoucherField.ID.getFieldName()).getNodeValue().replace("ID-", ""));
        parsedData.put(VoucherField.DAYS.getFieldName(), getElementContent(voucherElement, VoucherField.DAYS.getFieldName()));
        parsedData.put(VoucherField.COUNTRY.getFieldName(), getElementContent(voucherElement, VoucherField.COUNTRY.getFieldName()));
        parsedData.put(VoucherField.VOUCHER_TYPE.getFieldName(), getElementContent(voucherElement, VoucherField.VOUCHER_TYPE.getFieldName()));
        parsedData.put(VoucherField.NIGHTS.getFieldName(), getElementContent(voucherElement, VoucherField.NIGHTS.getFieldName()));
        parsedData.put(VoucherField.TRANSPORT_TYPE.getFieldName(), getElementContent(voucherElement, VoucherField.TRANSPORT_TYPE.getFieldName()));
        parsedData.put(VoucherField.DATE.getFieldName(), getElementContent(voucherElement, VoucherField.DATE.getFieldName()));
        getCost((Element) voucherElement.getElementsByTagName(VoucherField.VOUCHER_COST.getFieldName()).item(0));
        getHotelCharacteristicList((Element) voucherElement.getElementsByTagName(VoucherField.HOTEL.getFieldName()).item(0));
        return entityBuilder.build(parsedData);
    }

    private void getCost(Element costElement) {
        parsedData.put(VoucherField.COST.getFieldName(), getElementContent(costElement, VoucherField.COST.getFieldName()));
        int length = costElement.getElementsByTagName(VoucherField.ITEM.getFieldName()).getLength();
        parsedData.put("itemAmount", String.valueOf(length));
        for (int i = 0; i < length; i++) {
            parsedData.put(VoucherField.ITEM.getFieldName() + i, costElement.getElementsByTagName(VoucherField.ITEM.getFieldName()).item(i).getTextContent());
        }
    }

    private void getHotelCharacteristicList(Element hotelElement) {
        getHotelRoom(hotelElement);
        Element nutritionElement = (Element) hotelElement.getElementsByTagName(VoucherField.NUTRITION.getFieldName()).item(0);
        parsedData.put(VoucherField.NUTRITION_TYPE.getFieldName(), getElementContent(nutritionElement, VoucherField.NUTRITION_TYPE.getFieldName()));
        parsedData.put(VoucherField.HOTEL_STARS.getFieldName(), getElementContent(hotelElement, VoucherField.HOTEL_STARS.getFieldName()));
        int l = hotelElement.getElementsByTagName(VoucherField.HOTEL_CHARACTERISTIC.getFieldName()).getLength();
        parsedData.put("hotelChartAmount", String.valueOf(l));
        for (int i = 0; i < l; i++) {
            Element hotelCharacteristicElement = (Element) hotelElement.getElementsByTagName(VoucherField.HOTEL_CHARACTERISTIC.getFieldName()).item(i);
            parsedData.put(VoucherField.HOTEL_CHARACTERISTIC.getFieldName() + i, hotelCharacteristicElement.getAttribute(VoucherField.ROOM_CHARACTERISTIC_NAME.getFieldName()));
        }
    }

    private void getHotelRoom(Element hotelElement) {
        parsedData.put(VoucherField.HOTEL_ROOM_TYPE.getFieldName(), getElementContent((Element) hotelElement.getElementsByTagName(VoucherField.HOTEL_ROOM.getFieldName()).item(0), VoucherField.HOTEL_ROOM_TYPE.getFieldName()));
        int l = hotelElement.getElementsByTagName(VoucherField.ROOM_CHARACTERISTIC.getFieldName()).getLength();
        parsedData.put("roomChartAmount", String.valueOf(l));
        for (int i = 0; i < l; i++) {
            Element roomChar = (Element) hotelElement.getElementsByTagName(VoucherField.ROOM_CHARACTERISTIC.getFieldName()).item(i);
            parsedData.put(VoucherField.ROOM_CHARACTERISTIC_NAME.getFieldName() + i, getElementContent(roomChar, VoucherField.ROOM_CHARACTERISTIC_NAME.getFieldName()));
            parsedData.put(VoucherField.ROOM_CHARACTERISTIC_AVAILABILITY.getFieldName() + i, getElementContent(roomChar, VoucherField.ROOM_CHARACTERISTIC_AVAILABILITY.getFieldName()));
        }
    }

    @Override
    public List<T> parse(InputStream inputStream) throws ParseExecuteException, BuildException {
        try {
            parsedData = new HashMap<>();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(inputStream);
            List<T> entities = new ArrayList<>();
            Element root = document.getDocumentElement();
            NodeList nodeList = root.getElementsByTagName(VoucherField.VOUCHER.getFieldName());
            for (int i = 0; i < nodeList.getLength(); i++) {
                Element voucherElement = (Element) nodeList.item(i);
                entities.add(buildVoucher(voucherElement));
            }
            return entities;
        } catch (BuildException e) {
            LOGGER.error("Couldn't build voucher {}", e.getMessage());
            throw new BuildException("Couldn't build voucher", e);
        } catch (SAXException | ParserConfigurationException e) {
            LOGGER.error("Could't parse by dom parser {}", e.getMessage());
            throw new ParseExecuteException("Could't parse by dom parser", e.fillInStackTrace());
        } catch (IOException e) {
            LOGGER.error("Could't parse input stream. Fatal error {}", e.getMessage());
            throw new ParseExecuteException("Could't parse input stream. Fatal error", e.fillInStackTrace());
        }

    }
}

