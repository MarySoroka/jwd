package com.vouchers.command;

import com.vouchers.service.VoucherService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DisplayVouchersCommand extends AbstractAppCommand {
    private VoucherService voucherService;

    public DisplayVouchersCommand(VoucherService voucherService) {
        this.voucherService = voucherService;
    }

    @Override
    public void executeCommand(HttpServletRequest req, HttpServletResponse resp) {
        req.setAttribute("voucherList", voucherService.getAll());
        forward(req,resp,"/jsp/vouchers/table.jsp");
    }
}
