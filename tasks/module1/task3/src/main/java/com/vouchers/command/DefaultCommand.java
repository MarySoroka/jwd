package com.vouchers.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class DefaultCommand extends AbstractAppCommand {
    @Override
    public void executeCommand(HttpServletRequest req, HttpServletResponse resp) {
        forward(req,resp,"/jsp/index.jsp");
    }
}
