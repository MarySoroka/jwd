package com.vouchers.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum CommandType {
    DISPLAY_VOUCHERS("DISPLAY_VOUCHERS"),
    UPLOAD("UPLOAD"),
    DOWNLOAD("DOWNLOAD"),
    DEFAULT("DEFAULT");
    private static final Logger LOGGER = LogManager.getLogger(CommandType.class);
    public String getCommand() {
        return command;
    }

    private String command;

    CommandType(String command) {
        this.command = command;
    }

    public static CommandType getByName(String name) {
        for (CommandType c :
                CommandType.values()) {
            if (c.command.equalsIgnoreCase(name)) {
                return c;
            }
        }
        LOGGER.error("Can't find command by name {}",name);
        return CommandType.DEFAULT;
    }
}
