package com.vouchers.entity;

import java.util.Objects;

public class HotelStars extends HotelCharacteristic {
    private Short amountOfStars;

    public void setAmountOfStars(Short amountOfStars) {
        this.amountOfStars = amountOfStars;
    }

    public Short getAmountOfStars() {
        return amountOfStars;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HotelStars stars = (HotelStars) o;
        return Objects.equals(amountOfStars, stars.amountOfStars);
    }

    @Override
    public int hashCode() {
        return Objects.hash(amountOfStars);
    }

    @Override
    public String toString() {
        return "HotelStars{" +
                "amountOfStars=" + amountOfStars +
                "} " + super.toString();
    }

    public HotelStars(Short amountOfStars) {
        this.amountOfStars = amountOfStars;
    }
    public HotelStars(HotelStars another) {
        this.setName(another.getName());
        this.amountOfStars = another.amountOfStars;
    }

    public HotelStars() {
    }
}
