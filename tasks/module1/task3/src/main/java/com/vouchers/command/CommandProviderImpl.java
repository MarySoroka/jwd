package com.vouchers.command;

import java.util.EnumMap;
import java.util.Map;


public class CommandProviderImpl implements CommandProvider {
    private Map<CommandType, AbstractAppCommand> commands = new EnumMap<>(CommandType.class);
    private static final AbstractAppCommand DEFAULT_COMMAND = new DefaultCommand();

    @Override
    public void setCommand(CommandType commandType, AbstractAppCommand command) {
        this.commands.put(commandType, command);
    }

    @Override
    public AbstractAppCommand getCommand(CommandType commandType) {
        return commands.getOrDefault(commandType, DEFAULT_COMMAND);
    }

    @Override
    public void deleteCommand(CommandType commandType) {
        commands.remove(commandType);
    }
}