package com.vouchers.parsers;

public class ParseExecuteException extends Exception {
    public ParseExecuteException() {
    }

    public ParseExecuteException(String s) {
        super(s);
    }

    public ParseExecuteException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ParseExecuteException(Throwable throwable) {
        super(throwable);
    }

    public ParseExecuteException(String s, Throwable throwable, boolean b, boolean b1) {
        super(s, throwable, b, b1);
    }
}
