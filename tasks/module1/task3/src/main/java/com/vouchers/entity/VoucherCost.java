package com.vouchers.entity;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

public class VoucherCost {
    private Long cost;

    public VoucherCost(Long cost, Set<String> includedCharacteristics) {
        this.cost = cost;
        this.includedCharacteristics = includedCharacteristics;
    }

    public VoucherCost(VoucherCost another) {
        this.cost = another.cost;
        this.includedCharacteristics = new HashSet<>(another.includedCharacteristics);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VoucherCost that = (VoucherCost) o;
        return Objects.equals(cost, that.cost) &&
                Objects.equals(includedCharacteristics, that.includedCharacteristics);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cost, includedCharacteristics);
    }

    public Long getCost() {
        return cost;
    }

    public Set<String> getIncludedCharacteristics() {
        return includedCharacteristics;
    }

    @Override
    public String toString() {
        return "Cost{" +
                "cost=" + cost +
                ", includedCharacteristics=" + includedCharacteristics +
                '}';
    }

    public VoucherCost() {
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public void setIncludedCharacteristics(Set<String> includedCharacteristics) {
        this.includedCharacteristics = includedCharacteristics;
    }

    private Set<String> includedCharacteristics;
}
