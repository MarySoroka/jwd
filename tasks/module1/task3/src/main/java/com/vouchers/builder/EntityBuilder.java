package com.vouchers.builder;

import java.util.Map;
public interface EntityBuilder <T> {
    T build(Map<String, String> data) throws BuildException;
    String buildXml(T object);
}
