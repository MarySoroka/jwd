package com.vouchers.validation;

import java.io.InputStream;

public abstract class Validator {
    protected ValidationResult validationResult;

    public ValidationResult getValidationResult() {
        return validationResult;
    }

    abstract void validate(InputStream filepath) throws ValidatorException;
}
