package com.vouchers.parsers;

import com.vouchers.builder.BuildException;
import com.vouchers.builder.EntityBuilder;
import com.vouchers.builder.VoucherField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class SaxHandler<T> extends DefaultHandler {
    private static final Logger LOGGER = LogManager.getLogger(SaxHandler.class);
    private int hotelChartAmount = 0;
    private int itemCounter = 0;
    private int roomChartCounter = 0;
    private EntityBuilder<T> entityBuilder;
    private Map<String, String> parsedData;
    private List<T> entities = new ArrayList<>();
    private String content = null;
    public SaxHandler(EntityBuilder<T> entityBuilder, Map<String, String> parsedData) {
        this.entityBuilder = entityBuilder;
        this.parsedData = parsedData;
    }

    public List<T> getEntities() {
        return new ArrayList<T>(entities);
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) {
        LOGGER.info("Start tag: {}", qName);
        if (VoucherField.VOUCHER.equals(VoucherField.of(qName))) {

            parsedData.put(VoucherField.ID.getFieldName(), attributes.getValue(VoucherField.ID.getFieldName()).replace("ID-", ""));

        } else if (VoucherField.HOTEL_CHARACTERISTIC.equals(VoucherField.of(qName))) {

            parsedData.put(VoucherField.HOTEL_CHARACTERISTIC.getFieldName() + hotelChartAmount, attributes.getValue(VoucherField.ROOM_CHARACTERISTIC_NAME.getFieldName()));
            hotelChartAmount++;

        } else {
            LOGGER.error("Don't parse start {} tag", qName);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) {
        LOGGER.info("End tag: {}", qName);
        LOGGER.info("Content of the tag: {}", content);
        switch (VoucherField.of(qName)) {
            case VOUCHER:
                try {
                    entities.add(entityBuilder.build(parsedData));
                } catch (BuildException e) {
                    LOGGER.error("Couldn't build entity {}", e.getMessage());
                }
                break;
            case ROOM_CHARACTERISTIC_NAME:
                parsedData.put(VoucherField.ROOM_CHARACTERISTIC_NAME.getFieldName() + roomChartCounter, content);
                break;
            case ITEM:
                parsedData.put(VoucherField.ITEM.getFieldName() + itemCounter, content);
                itemCounter++;
                break;
            case ROOM_CHARACTERISTIC_AVAILABILITY:
                parsedData.put(VoucherField.ROOM_CHARACTERISTIC_AVAILABILITY.getFieldName() + roomChartCounter, content);
                roomChartCounter++;
                break;
            case VOUCHER_TYPE:
                parsedData.put(VoucherField.VOUCHER_TYPE.getFieldName(), content);
                break;
            case DATE:
                parsedData.put(VoucherField.DATE.getFieldName(), content);
                break;
            case COUNTRY:
                parsedData.put(VoucherField.COUNTRY.getFieldName(), content);
                break;
            case DAYS:
                parsedData.put(VoucherField.DAYS.getFieldName(), content);
                break;
            case NIGHTS:
                parsedData.put(VoucherField.NIGHTS.getFieldName(), content);
                break;
            case TRANSPORT_TYPE:
                parsedData.put(VoucherField.TRANSPORT_TYPE.getFieldName(), content);
                break;
            case HOTEL_STARS:
                parsedData.put(VoucherField.HOTEL_STARS.getFieldName(), content);
                break;
            case NUTRITION_TYPE:
                parsedData.put(VoucherField.NUTRITION_TYPE.getFieldName(), content);
                break;
            case HOTEL_ROOM_TYPE:
                parsedData.put(VoucherField.HOTEL_ROOM_TYPE.getFieldName(), content);
                break;
            case ROOM_CHARACTERISTIC:
                parsedData.put("roomChartAmount", String.valueOf(roomChartCounter));
                break;
            case COST:
                parsedData.put(VoucherField.COST.getFieldName(), content);
                break;
            case INCLUDED_CHARACTERISTICS:
                parsedData.put("itemAmount", String.valueOf(itemCounter));
                break;
            case HOTEL_CHARACTERISTIC:
                parsedData.put("hotelChartAmount", String.valueOf(hotelChartAmount));
                break;
            default:
                LOGGER.error("Don't parse end  {} tag", qName);

        }
    }

    @Override
    public void characters(char[] ch, int start, int length) {
        content = String.copyValueOf(ch, start, length).trim();
    }
}


