package com.vouchers.command;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public abstract class AbstractAppCommand implements Command {
    private static final Logger LOGGER = LogManager.getLogger(AbstractAppCommand.class);
    protected void forward(HttpServletRequest request, HttpServletResponse response, String url) {
        try {
            request.getRequestDispatcher(url).forward(request, response);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to forward view", e);
        }

    }

    protected void redirect(HttpServletResponse response, String url) {
        try {
            response.sendRedirect(url);
        } catch (Exception e) {
            throw new IllegalStateException("Failed to forward view", e);
        }

    }

    @Override
    public void execute(HttpServletRequest request, HttpServletResponse response) {
        try {
            executeCommand(request, response);
        } catch (Exception e) {
            LOGGER.error("Failed to execute command ", e);
        }
    }

    public abstract void executeCommand(HttpServletRequest request, HttpServletResponse response) throws Exception;
}
