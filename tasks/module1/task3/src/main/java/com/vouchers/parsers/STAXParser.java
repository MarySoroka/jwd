package com.vouchers.parsers;

import com.vouchers.builder.BuildException;
import com.vouchers.builder.EntityBuilder;
import com.vouchers.builder.VoucherField;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

public class STAXParser<T> extends XMLParser<T> {
    private static final Logger LOGGER = LogManager.getLogger(STAXParser.class);
    private XMLInputFactory factory;

    public STAXParser(EntityBuilder<T> entityBuilder) {
        factory = XMLInputFactory.newInstance();
        super.entityBuilder = entityBuilder;
        super.parsedData = new HashMap<>();
    }


    private void setStartElement(XMLStreamReader reader) throws XMLStreamException {
        Optional<String> context;
        String localName = reader.getLocalName();
        LOGGER.info("Start process start {} tag", localName);
        switch (VoucherField.of(localName)) {
            case VOUCHER:
                parsedData.put(VoucherField.ID.getFieldName(), reader.getAttributeValue(0).replace("ID-", ""));
                break;
            case VOUCHER_COST:
                getCost(reader);
                break;
            case HOTEL:
                getHotel(reader);
                break;
            case VOUCHER_TYPE:
                context = getXMLText(reader);
                context.ifPresent(s -> parsedData.put(VoucherField.VOUCHER_TYPE.getFieldName(), s));
                break;
            case DATE:
                context = getXMLText(reader);
                context.ifPresent(s -> parsedData.put(VoucherField.DATE.getFieldName(), s));
                break;
            case COUNTRY:
                context = getXMLText(reader);
                context.ifPresent(s -> parsedData.put(VoucherField.COUNTRY.getFieldName(), s));
                break;
            case DAYS:
                context = getXMLText(reader);
                context.ifPresent(s -> parsedData.put(VoucherField.DAYS.getFieldName(), s));
                break;
            case NIGHTS:
                context = getXMLText(reader);
                context.ifPresent(s -> parsedData.put(VoucherField.NIGHTS.getFieldName(), s));
                break;
            case TRANSPORT_TYPE:
                context = getXMLText(reader);
                context.ifPresent(s -> parsedData.put(VoucherField.TRANSPORT_TYPE.getFieldName(), s));
                break;
            default:
                LOGGER.error("Couldn't find {} tag", localName);

        }
    }

    private void getHotel(XMLStreamReader reader) throws XMLStreamException {
        int hotelChartAmount = 0;
        int roomChartAmount = 0;

        Optional<String> context;
        while (reader.hasNext()) {
            int event = reader.next();
            if (event == XMLStreamConstants.START_ELEMENT) {
                String localName = reader.getLocalName();
                LOGGER.info("Start process start {} tag", localName);
                switch (VoucherField.of(localName)) {
                    case HOTEL_CHARACTERISTIC:
                        parsedData.put(VoucherField.HOTEL_CHARACTERISTIC.getFieldName() + hotelChartAmount, reader.getAttributeValue(0));
                        hotelChartAmount++;
                        break;
                    case HOTEL_STARS:
                        context = getXMLText(reader);
                        context.ifPresent(s -> parsedData.put(VoucherField.HOTEL_STARS.getFieldName(), s));
                        break;
                    case NUTRITION_TYPE:
                        context = getXMLText(reader);
                        context.ifPresent(s -> parsedData.put(VoucherField.NUTRITION_TYPE.getFieldName(), s));
                        break;
                    case HOTEL_ROOM_TYPE:
                        context = getXMLText(reader);
                        context.ifPresent(s -> parsedData.put(VoucherField.HOTEL_ROOM_TYPE.getFieldName(), s));
                        break;
                    case ROOM_CHARACTERISTIC_NAME:
                        context = getXMLText(reader);
                        String finalName = VoucherField.ROOM_CHARACTERISTIC_NAME.getFieldName() + roomChartAmount;
                        context.ifPresent(s -> parsedData.put(finalName, s));
                        break;
                    case ROOM_CHARACTERISTIC_AVAILABILITY:
                        context = getXMLText(reader);
                        int finalRoomChartAmount = roomChartAmount;
                        context.ifPresent(s -> parsedData.put(VoucherField.ROOM_CHARACTERISTIC_AVAILABILITY.getFieldName() + finalRoomChartAmount, s));
                        roomChartAmount++;
                        break;
                    default:
                        LOGGER.info("Don't need to process {} tag", localName);
                }
            } else if (event == XMLStreamConstants.END_ELEMENT) {
                String localName = reader.getLocalName();
                LOGGER.info("Start process end {} tag", localName);
                switch (VoucherField.of(localName)) {
                    case ROOM_CHARACTERISTIC:
                        parsedData.put("roomChartAmount", String.valueOf(roomChartAmount));
                        break;
                    case HOTEL:
                        parsedData.put("hotelChartAmount", String.valueOf(hotelChartAmount));
                        return;
                    default:
                        LOGGER.info("Don't need to process end {} tag", localName);

                }
            }
        }

    }


    private void getCost(XMLStreamReader reader) throws XMLStreamException {
        Optional<String> context;
        int itemCounter = 0;
        while (reader.hasNext()) {
            int event = reader.next();
            if (event == XMLStreamConstants.START_ELEMENT) {
                String localName = reader.getLocalName();
                if (VoucherField.ITEM.getFieldName().equals(localName)) {
                    context = getXMLText(reader);
                    int finalItemCounter = itemCounter;
                    context.ifPresent(s -> parsedData.put(VoucherField.ITEM.getFieldName() + finalItemCounter, s));
                    itemCounter++;
                }
                if (VoucherField.COST.getFieldName().equals(localName)) {
                    context = getXMLText(reader);
                    context.ifPresent(s -> parsedData.put(VoucherField.COST.getFieldName(), s));
                }
            } else if (event == XMLStreamConstants.END_ELEMENT && VoucherField.VOUCHER_COST.getFieldName().equals(reader.getLocalName())) {
                parsedData.put("itemAmount", String.valueOf(itemCounter));
                return;
            }
        }

    }

    private Optional<String> getXMLText(XMLStreamReader reader) throws XMLStreamException {
        if (reader.hasNext()) {
            reader.next();
            return Optional.of(reader.getText().trim());
        }
        LOGGER.error("Couldn't get text context for tag ");
        return Optional.empty();
    }

    @Override
    public List<T> parse(InputStream inputStream) throws ParseExecuteException, BuildException {
        try {
            List<T> entities = new ArrayList<>();
            XMLStreamReader reader = factory.createXMLStreamReader(inputStream);
            while (reader.hasNext()) {
                int event = reader.next();
                if (event == XMLStreamConstants.START_ELEMENT) {
                    setStartElement(reader);
                }
                if (event == XMLStreamConstants.END_ELEMENT && VoucherField.VOUCHER.getFieldName().equals(reader.getLocalName())) {
                    T entity = entityBuilder.build(parsedData);
                    entities.add(entity);
                }
            }

            return entities;
        } catch (XMLStreamException e) {
            throw new ParseExecuteException("Could't parse by stax parser", e);
        } catch (BuildException e) {
            throw new BuildException("Couldn't build voucher", e);
        }
    }
}
