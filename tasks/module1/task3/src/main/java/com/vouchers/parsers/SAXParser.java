package com.vouchers.parsers;

import com.vouchers.builder.EntityBuilder;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParserFactory;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;

public class SAXParser<T> extends XMLParser<T> {
    public SAXParser(EntityBuilder<T> entityBuilder) {
        super.entityBuilder = entityBuilder;
        super.parsedData = new HashMap<>();
    }

    @Override
    public List<T> parse(InputStream inputStream) throws ParseExecuteException {
        try {
            javax.xml.parsers.SAXParser parser= SAXParserFactory.newInstance().newSAXParser();
            SaxHandler<T> saxHandler = new SaxHandler<>(super.entityBuilder,super.parsedData);
            parser.parse(inputStream, saxHandler);
            return saxHandler.getEntities();
        } catch (ParserConfigurationException | SAXException | IOException e) {
            throw new ParseExecuteException("Couldn't parse file by sax parser ",e.fillInStackTrace());
        }

    }
}
