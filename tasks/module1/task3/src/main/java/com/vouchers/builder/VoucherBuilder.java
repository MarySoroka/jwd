package com.vouchers.builder;

import com.vouchers.entity.TransportType;
import com.vouchers.entity.Voucher;
import com.vouchers.entity.VoucherType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;

public class VoucherBuilder implements EntityBuilder<Voucher> {
    private static final Logger LOGGER = LogManager.getLogger(VoucherBuilder.class);
    private final VoucherCostBuilder voucherCostBuilder;
    private final HotelBuilder hotelBuilder;

    public VoucherBuilder(VoucherCostBuilder voucherCostBuilder, HotelBuilder hotelBuilder) {
        this.voucherCostBuilder = voucherCostBuilder;
        this.hotelBuilder = hotelBuilder;
    }

    @Override
    public Voucher build(Map<String, String> data) throws BuildException {
        try {
            LOGGER.info("Start build voucher");
            Voucher voucher = new Voucher();
            voucher.setId(Long.valueOf(data.get(VoucherField.ID.getFieldName())));
            voucher.setVoucherCost(voucherCostBuilder.build(data));
            voucher.setHotel(hotelBuilder.build(data));
            voucher.setNights(Integer.valueOf(data.get(VoucherField.NIGHTS.getFieldName())));
            voucher.setTransportType(TransportType.getByName(data.get(VoucherField.TRANSPORT_TYPE.getFieldName())));
            voucher.setCountry(data.get(VoucherField.COUNTRY.getFieldName()));
            voucher.setVoucherType(VoucherType.getByName(data.get(VoucherField.VOUCHER_TYPE.getFieldName())));
            voucher.setDays(Integer.valueOf(data.get(VoucherField.DAYS.getFieldName())));
            voucher.setDate(new SimpleDateFormat("yyyy-mm-dd").parse(data.get(VoucherField.DATE.getFieldName())));
            return voucher;
        } catch (ParseException e) {
            LOGGER.error("Could't parse by dom parser {}",e.getMessage());
            throw new BuildException("Could't parse by dom parser", e.fillInStackTrace());
        }
    }

    @Override
    public String buildXml(Voucher object) {
        DateFormat df = new SimpleDateFormat("yyyy-mm-dd");

        return "<voucher id=\"ID-"+object.getId()+"\">\n" +
                "<type>"+object.getVoucherType().getType()+"</type>\n" +
                "<country>"+object.getCountry()+"</country>\n" +
                "<date>"+df.format(object.getDate())+"</date>\n" +
                "<days>"+object.getDays()+"</days>\n" +
                "<nights>"+object.getNights()+"</nights>\n" +
                "<transport>"+object.getTransportType().getType()+"</transport>\n"+
                hotelBuilder.buildXml(object.getHotel())+
                voucherCostBuilder.buildXml(object.getVoucherCost())+
                "</voucher>"
                ;
    }
}
