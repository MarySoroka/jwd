package com.vouchers.builder;

import com.vouchers.entity.HotelCharacteristic;
import com.vouchers.entity.HotelRoom;
import com.vouchers.entity.RoomCharacteristic;
import com.vouchers.entity.RoomType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HotelRoomBuilder implements EntityBuilder<HotelRoom> {
    private static final Logger LOGGER = LogManager.getLogger(HotelRoomBuilder.class);

    @Override
    public HotelRoom build(Map<String, String> data) throws BuildException {
        try {
            LOGGER.info("Start build hotel room");
            HotelRoom hotelRoom = new HotelRoom();
            hotelRoom.setRoomType(RoomType.getByName(Integer.valueOf(data.get(VoucherField.HOTEL_ROOM_TYPE.getFieldName()))));
            hotelRoom.setName(data.get(VoucherField.HOTEL_CHARACTERISTIC.getFieldName() + 2));
            List<RoomCharacteristic> roomCharacteristicList = new ArrayList<>();
            for (int i = 0; i < 4; i++) {
                RoomCharacteristic roomCharacteristic = new RoomCharacteristic();
                roomCharacteristic.setName(data.get(VoucherField.ROOM_CHARACTERISTIC_NAME.getFieldName() + i));
                roomCharacteristic.setAvailability(Boolean.parseBoolean(data.get(VoucherField.ROOM_CHARACTERISTIC_AVAILABILITY.getFieldName() + i)));
                roomCharacteristicList.add(roomCharacteristic);
            }
            hotelRoom.setRoomCharacteristicList(roomCharacteristicList);
            return hotelRoom;
        } catch (Exception e) {
            LOGGER.error("Couldn't build hotel room. Fatal error {}", e.getMessage());
            throw new BuildException("Couldn't build hotel room. Fatal error ", e);
        }

    }

    @Override
    public String buildXml(HotelRoom object) {
        StringBuilder roomChart = new StringBuilder();
        for (RoomCharacteristic r:
             object.getRoomCharacteristicList()) {
            roomChart.append("<characteristic>\n" + "<name>").append(r.getName()).append("</name>\n").append("<availability>").append(r.isAvailability()).append("</availability>\n").append("</characteristic>\n");
        }
        return "<room>\n" +
                "<room-type>"+object.getRoomType().getType()+"</room-type>\n" +
                "<room-characteristics>\n" +
                roomChart+
                "</room-characteristics>\n" +
                "</room>";
    }
}
