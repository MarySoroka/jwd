package com.vouchers.builder;

import com.vouchers.entity.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class HotelBuilder implements EntityBuilder<Hotel> {
    private static final Logger LOGGER = LogManager.getLogger(HotelBuilder.class);
    private final HotelStarsBuilder hotelStarsBuilder;
    private final HotelRoomBuilder hotelRoomBuilder;
    private final NutritionBuilder nutritionBuilder;

    public HotelBuilder(HotelStarsBuilder hotelStarsBuilder, HotelRoomBuilder hotelRoomBuilder, NutritionBuilder nutritionBuilder) {
        this.hotelStarsBuilder = hotelStarsBuilder;
        this.hotelRoomBuilder = hotelRoomBuilder;
        this.nutritionBuilder = nutritionBuilder;
    }

    @Override
    public Hotel build(Map<String, String> data) throws BuildException {
        try {
            LOGGER.info("Start build hotel");
            List<HotelCharacteristic> hotelCharacteristicList = new ArrayList<>();
            Hotel hotel = new Hotel();
            hotelCharacteristicList.add(hotelStarsBuilder.build(data));
            hotelCharacteristicList.add(nutritionBuilder.build(data));
            hotelCharacteristicList.add(hotelRoomBuilder.build(data));
            hotel.setHotelCharacteristic(hotelCharacteristicList);
            return hotel;
        } catch (BuildException e) {
            LOGGER.error("Couldn't build hotel {}", e.getMessage());
            throw new BuildException("Couldn't build hotel ", e);
        } catch (Exception e) {
            LOGGER.error("Couldn't build hotel. Fatal error. {}", e.getMessage());
            throw new BuildException("Couldn't build hotel. Fatal error ", e);
        }
    }

    @Override
    public String buildXml(Hotel object) {
        return "<hotel>\n" + "<hotel-characteristics name=\""+
                object.getHotelCharacteristic().get(0).getName()+"\">"+"\n"+
                hotelStarsBuilder.buildXml((HotelStars) object.getHotelCharacteristic().get(0))
                +"\n"+"</hotel-characteristics>"+ "<hotel-characteristics name=\""+
                object.getHotelCharacteristic().get(1).getName()+"\">"+"\n"
                + nutritionBuilder.buildXml((Nutrition) object.getHotelCharacteristic().get(1))
                +"\n"+"</hotel-characteristics>"
                + "<hotel-characteristics name=\""+
                object.getHotelCharacteristic().get(2).getName()+"\">"+"\n"+
                hotelRoomBuilder.buildXml((HotelRoom) object.getHotelCharacteristic().get(2))
                +"\n"+"</hotel-characteristics>"+ "\n</hotel>";
    }
}
