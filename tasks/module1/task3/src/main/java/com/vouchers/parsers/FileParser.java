package com.vouchers.parsers;

import com.vouchers.builder.BuildException;

import java.io.InputStream;
import java.util.List;

public interface FileParser<T> {
    List<T> parse(InputStream inputStream) throws ParseExecuteException, BuildException;
}
