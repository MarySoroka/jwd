package com.vouchers.dao;

import com.vouchers.entity.Voucher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

public class TouristVouchersDaoImpl implements TouristVouchersDao {
    private final Map<Long, Voucher> vouchers = new ConcurrentHashMap<>();
    private final AtomicLong sequence = new AtomicLong(0);
    private static final Logger LOGGER = LogManager.getLogger(TouristVouchersDaoImpl.class);

    @Override
    public List<Voucher> getAll() {
        return new ArrayList<>(vouchers.values());
    }

    @Override
    public Voucher get(Long key) {
        if (this.vouchers.containsKey(key)) {
          return cloneVoucher(this.vouchers.get(key));
        }else{
            LOGGER.error("Couldn't find entity with this id {}",key);
            throw new EntityNotFoundException("Couldn't find entity with this id "+key);
        }
    }

    @Override
    public void save(Voucher voucher) {
        this.vouchers.put(sequence.incrementAndGet(), cloneVoucher(voucher));
    }
    @Override
    public Voucher cloneVoucher(Voucher voucher) {
        return new Voucher(voucher);
    }

    @Override
    public void update(Long key, Voucher voucher) {
        if (this.vouchers.containsKey(key)) {
            this.vouchers.put(key, cloneVoucher(voucher));
        }else{
            LOGGER.error("Couldn't find entity with this id {}",key);
            throw new EntityNotFoundException("Couldn't find entity with this id "+key);
        }
    }

    @Override
    public void delete(Long key) {
       this.vouchers.remove(key);
    }
}
