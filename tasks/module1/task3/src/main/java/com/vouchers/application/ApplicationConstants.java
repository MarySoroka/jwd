package com.vouchers.application;

public class ApplicationConstants {
    public static final String COMMAND_NAME_PARAM = "command";
    public static final String XML_FILE_DOWNLOAD = "vouchers.xml";
}
