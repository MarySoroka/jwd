package com.vouchers.command;

public class CommandExecuteException extends RuntimeException {
    public CommandExecuteException(String message, Exception root) {
        super(message, root);
    }
}