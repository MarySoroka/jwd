package com.vouchers.builder;

public enum VoucherField {
    VOUCHER("voucher"),
    ID("id"),
    DATE("date"),
    HOTEL("hotel"),
    HOTEL_STARS("stars"),
    HOTEL_ROOM_TYPE("room-type"),
    HOTEL_ROOM("room"),
    ROOM_CHARACTERISTIC("characteristic"),
    HOTEL_CHARACTERISTIC("hotel-characteristics"),
    ROOM_CHARACTERISTIC_NAME("name"),
    HOTEL_CHARACTERISTIC_NAME("hotel-name"),
    ROOM_CHARACTERISTIC_AVAILABILITY("availability"),
    NUTRITION_TYPE("nutrition-type"),
    NUTRITION("nutrition"),
    COUNTRY("country"),
    VOUCHER_TYPE("type"),
    TRANSPORT_TYPE("transport"),
    VOUCHER_COST("voucher-cost"),
    COST("cost"),
    ITEM("item"),
    DAYS("days"),
    INCLUDED_CHARACTERISTICS("included-characteristics"),
    NIGHTS("nights"),
    NONE("none");

    private final String fieldName;

    VoucherField(String fieldName) {
        this.fieldName = fieldName;
    }

    public static VoucherField of(String fieldName) {
        for (VoucherField v :
                VoucherField.values()) {
            if (v.fieldName.equals(fieldName)) {
                return v;
            }
        }
        return VoucherField.NONE;
    }

    public String getFieldName() {
        return fieldName;
    }

}

