package com.vouchers.entity;

import java.util.Objects;

public class Nutrition extends HotelCharacteristic {
    private NutritionType nutritionType;

    public Nutrition() {
    }

    public NutritionType getNutritionType() {
        return nutritionType;
    }

    public Nutrition(Nutrition another) {
        this.setName(another.getName());
        this.nutritionType = another.nutritionType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Nutrition nutrition = (Nutrition) o;
        return nutritionType == nutrition.nutritionType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nutritionType);
    }

    public Nutrition(NutritionType nutritionType) {
        this.nutritionType = nutritionType;
    }

    @Override
    public String toString() {
        return "Nutrition{" +
                "nutritionType=" + nutritionType +
                "} " + super.toString();
    }

    public void setNutritionType(NutritionType nutritionType) {
        this.nutritionType = nutritionType;
    }
}
