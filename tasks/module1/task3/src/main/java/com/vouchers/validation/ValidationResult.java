package com.vouchers.validation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
public class ValidationResult {

    private List<ValidationMessage> messageList = new ArrayList<>();

    public void addMessage(String field, String value, String... error){
        messageList.add(new ValidationMessage(field,value, Arrays.asList(error)));
    }

    public List<ValidationMessage> getMessageList() {
        return messageList;
    }

    public boolean isValid(){
        return this.messageList.isEmpty();
    }

}
