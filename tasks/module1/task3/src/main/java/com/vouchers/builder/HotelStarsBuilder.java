package com.vouchers.builder;

import com.vouchers.entity.HotelStars;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class HotelStarsBuilder implements EntityBuilder<HotelStars> {
    private static final Logger LOGGER = LogManager.getLogger(HotelStarsBuilder.class);

    @Override
    public HotelStars build(Map<String, String> data) throws BuildException {
        try {
            LOGGER.info("Start build hotel stars");
            HotelStars hotelStars = new HotelStars();
            hotelStars.setName(data.get(VoucherField.HOTEL_CHARACTERISTIC.getFieldName()+0));
            hotelStars.setAmountOfStars(Short.valueOf(data.get(VoucherField.HOTEL_STARS.getFieldName())));
            return hotelStars;
        } catch (Exception e) {
            LOGGER.error("Couldn't build hotel stars. Fatal error. {}", e.getMessage());
            throw new BuildException("Couldn't build hotel stars. Fatal error. ", e);
        }
    }

    @Override
    public String buildXml(HotelStars object) {
        return " <stars>"+object.getAmountOfStars()+"</stars>";
    }
}
