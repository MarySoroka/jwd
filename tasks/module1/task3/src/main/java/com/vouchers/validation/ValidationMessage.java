package com.vouchers.validation;

import java.util.List;

public class ValidationMessage {
    private String field;

    public ValidationMessage(String field, String value, List<String> errors) {
        this.field = field;
        this.value = value;
        this.errors = errors;
    }

    public String getField() {
        return field;
    }

    public String getValue() {
        return value;
    }

    public List<String> getErrors() {
        return errors;
    }

    private String value;
    private List<String> errors;
}
