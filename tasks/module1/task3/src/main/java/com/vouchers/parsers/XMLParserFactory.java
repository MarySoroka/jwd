package com.vouchers.parsers;

import com.vouchers.builder.EntityBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;

public class XMLParserFactory<T> {
    private static final Logger LOGGER = LogManager.getLogger(XMLParserFactory.class);

    public Optional<XMLParser<T>> getParser(String parser, EntityBuilder<T> entityBuilder) {
        switch (ParserType.getByName(parser.toUpperCase())) {
            case SAX:
                return Optional.of(new SAXParser<T>(entityBuilder));
            case DOM:
                return Optional.of(new DomParser<T>(entityBuilder));
            case STAX:
                return Optional.of(new STAXParser<T>(entityBuilder));
            default:
                LOGGER.error("Can't find parser by name {}", parser);
                return Optional.empty();
        }

    }
}
