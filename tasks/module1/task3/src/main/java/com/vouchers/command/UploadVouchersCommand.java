package com.vouchers.command;

import com.vouchers.builder.*;
import com.vouchers.entity.Voucher;
import com.vouchers.parsers.FileParser;
import com.vouchers.parsers.XMLParser;
import com.vouchers.parsers.XMLParserFactory;
import com.vouchers.service.VoucherService;
import com.vouchers.validation.ValidationResult;
import com.vouchers.validation.Validator;
import com.vouchers.validation.ValidatorException;
import com.vouchers.validation.XSDValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class UploadVouchersCommand extends AbstractAppCommand {
    private static final Logger LOGGER = LogManager.getLogger(UploadVouchersCommand.class);
    private final XMLParserFactory<Voucher> xmlParserFactory;
    private VoucherService voucherService;

    public UploadVouchersCommand(VoucherService voucherService, XMLParserFactory<Voucher> xmlParserFactory) {
        this.voucherService = voucherService;
        this.xmlParserFactory = xmlParserFactory;
    }

    @Override
    public void executeCommand(HttpServletRequest req, HttpServletResponse resp) throws Exception {
        LOGGER.info("Start upload command");
        Part xmlFile = req.getPart("xmlFile");
        if (xmlFile == null || xmlFile.getSize() <= 0) {
            LOGGER.error("File doesn't exist or empty");
            req.setAttribute("errors", Collections.singleton("File doesn't exist or empty"));
            forward(req, resp, "/jsp/index.jsp");
            return;
        }
        Validator validator = new XSDValidator(new ValidationResult());
        try (InputStream inputStream = xmlFile.getInputStream()) {
            ((XSDValidator) validator).validate(xmlFile.getInputStream());
            Optional<XMLParser<Voucher>> xmlParser = xmlParserFactory.getParser(req.getParameter("parser"),
                    new VoucherBuilder(new VoucherCostBuilder(),
                            new HotelBuilder(new HotelStarsBuilder(), new HotelRoomBuilder(), new NutritionBuilder())));
            if (xmlParser.isPresent()) {
                FileParser<Voucher> parser = xmlParser.get();
                List<Voucher> voucherList = parser.parse(inputStream);
                voucherList.forEach(voucherService::save);
                req.setAttribute("voucherList", voucherService.getAll());
                forward(req, resp, "/jsp/vouchers/table.jsp");
            }
        } catch (ValidatorException e) {
            LOGGER.error(e.getMessage());
            req.setAttribute("errors", validator.getValidationResult().getMessageList());
            forward(req, resp, "/jsp/index.jsp");
        } catch (IOException e) {
            LOGGER.error(e.getMessage());
            throw new CommandExecuteException("Could't execute upload command", e);
        }


    }
}
