package com.vouchers.command;

import com.vouchers.application.ApplicationConstants;
import com.vouchers.builder.*;
import com.vouchers.service.VoucherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Collections;

public class DownloadVouchersCommand extends AbstractAppCommand {
    private static final Logger LOGGER = LogManager.getLogger(DownloadVouchersCommand.class);
    private VoucherService voucherService;

    public DownloadVouchersCommand(VoucherService voucherService) {
        this.voucherService = voucherService;
    }

    @Override
    public void executeCommand(HttpServletRequest req, HttpServletResponse resp) {
        String xmlFile = voucherService.getXmlFile(new VoucherBuilder(new VoucherCostBuilder(), new HotelBuilder(new HotelStarsBuilder(), new HotelRoomBuilder(), new NutritionBuilder())));
        resp.setContentType("text/xml");
        resp.setHeader("Content-Disposition", "attachment;filename=" + ApplicationConstants.XML_FILE_DOWNLOAD);
        try (BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(resp.getOutputStream()))) {
            bufferedWriter.write(xmlFile);
            bufferedWriter.flush();
            forward(req, resp, "/jsp/vouchers/table.jsp");
        } catch (IOException e) {
            LOGGER.error("Couldn't download file: {}", e.getMessage());
            req.setAttribute("errors", Collections.singleton("Couldn't download file"));
        }
    }
}
