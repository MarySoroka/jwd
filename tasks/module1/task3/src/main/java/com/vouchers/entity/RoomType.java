package com.vouchers.entity;

import com.vouchers.parsers.ParserType;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum RoomType {
    SINGLE(1),
    DOUBLE(2),
    TRIPLE(3),
    FOUR_SEATED(4);

    public Integer getType() {
        return type;
    }

    private Integer type;
    private static final Logger LOGGER = LogManager.getLogger(RoomType.class);
    RoomType(Integer type) {
        this.type = type;
    }

    public static RoomType getByName(Integer type) {
        for (RoomType roomType :
                RoomType.values()) {
            if (roomType.type.equals(type)) {
                return roomType;
            }
        }
        LOGGER.error("Can't find room type by name {}",type);
        return RoomType.SINGLE;
    }
}
