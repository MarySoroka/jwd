package com.vouchers.dao;

import java.util.List;

public interface CRUDEntityDao<ENTITY, KEY> {
    List<ENTITY> getAll();
    ENTITY get(KEY key);

    void save(ENTITY entity);
    void update(KEY key,ENTITY entity);
    void delete(KEY key);
}
