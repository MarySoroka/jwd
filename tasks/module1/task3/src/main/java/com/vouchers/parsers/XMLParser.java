package com.vouchers.parsers;

import com.vouchers.builder.EntityBuilder;

import java.util.Map;

public abstract class XMLParser<T> implements FileParser<T> {
    protected EntityBuilder<T> entityBuilder;
    protected Map<String, String> parsedData;
}
