package com.vouchers.service;

import com.vouchers.builder.EntityBuilder;
import com.vouchers.dao.TouristVouchersDao;
import com.vouchers.entity.Voucher;

import java.util.List;

public class VoucherServiceImp implements VoucherService {
    private TouristVouchersDao touristVouchersDao;

    public VoucherServiceImp(TouristVouchersDao touristVouchersDao) {
        this.touristVouchersDao = touristVouchersDao;
    }


    @Override
    public List<Voucher> getAll() {
        return this.touristVouchersDao.getAll();
    }

    @Override
    public void save(Voucher voucher) {
        this.touristVouchersDao.save(voucher);
    }

    @Override
    public String getXmlFile(EntityBuilder<Voucher> builder) {
        StringBuilder stringBuilder = new StringBuilder("<?xml version=\"1.0\"?>\n" +
                "<tourist-vouchers\n" +
                "        xmlns=\"http://train.net\"\n" +
                "        xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n" +
                "        xsi:schemaLocation=\"http://train.net vouchers.xsd\"\n" +
                "        elementFormDefault=\"qualified\">\n");
        List<Voucher> vouchers = getAll();
        for (Voucher v : vouchers) {
            stringBuilder.append(builder.buildXml(v));
        }
        stringBuilder.append("</tourist-vouchers>\n");
        return stringBuilder.toString();
    }


}
