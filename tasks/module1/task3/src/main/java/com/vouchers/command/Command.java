package com.vouchers.command;

import org.xml.sax.SAXException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.text.ParseException;

@FunctionalInterface
public interface Command {
    void execute(HttpServletRequest req, HttpServletResponse resp) throws Exception;


}
