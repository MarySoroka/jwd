package com.vouchers.parsers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public enum ParserType {
    SAX("SAX"),
    STAX("STAX"),
    DOM("DOM"),
    NONE("NONE");
    private static final Logger LOGGER = LogManager.getLogger(ParserType.class);
    private String type;

    ParserType(String type) {
        this.type = type;
    }

    public static ParserType getByName(String name) {
        for (ParserType p :
                ParserType.values()) {
            if (p.type.equals(name)) {
                return p;
            }
        }
        LOGGER.error("Can't find parser by name {}", name);
        return ParserType.NONE;
    }
}
