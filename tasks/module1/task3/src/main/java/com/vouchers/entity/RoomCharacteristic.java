package com.vouchers.entity;

import java.util.Objects;

public class RoomCharacteristic {
    private String name;
    private boolean availability;

    public String getName() {
        return name;
    }

    public boolean isAvailability() {
        return availability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoomCharacteristic that = (RoomCharacteristic) o;
        return availability == that.availability &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, availability);
    }

    public void setName(String name) {
        this.name = name;
    }

    public RoomCharacteristic(String name, boolean availability) {
        this.name = name;
        this.availability = availability;
    }

    @Override
    public String toString() {
        return "RoomCharacteristic{" +
                "name='" + name + '\'' +
                ", availability=" + availability +
                '}';
    }

    public void setAvailability(boolean availability) {
        this.availability = availability;
    }

    public RoomCharacteristic() {
    }
}
