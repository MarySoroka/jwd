package com.vouchers.dao;

import com.vouchers.entity.Voucher;

public interface TouristVouchersDao extends CRUDEntityDao<Voucher, Long> {
    Voucher cloneVoucher(Voucher voucher);
}
