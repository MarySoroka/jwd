package com.vouchers.entity;

import java.util.Date;
import java.util.Objects;

public class Voucher{

    private Long id;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Voucher voucher = (Voucher) o;
        return Objects.equals(id, voucher.id) &&
                Objects.equals(date, voucher.date) &&
                Objects.equals(hotel, voucher.hotel) &&
                Objects.equals(country, voucher.country) &&
                voucherType == voucher.voucherType &&
                transportType == voucher.transportType &&
                Objects.equals(voucherCost, voucher.voucherCost) &&
                Objects.equals(days, voucher.days) &&
                Objects.equals(nights, voucher.nights);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, hotel, country, voucherType, transportType, voucherCost, days, nights);
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    private Date date;
    private Hotel hotel;
    private String country;
    private VoucherType voucherType;
    private TransportType transportType;
    private VoucherCost voucherCost;
    private Integer days;

    public Long getId() {
        return id;
    }

    public Hotel getHotel() {
        return hotel;
    }

    public String getCountry() {
        return country;
    }

    public VoucherType getVoucherType() {
        return voucherType;
    }

    public TransportType getTransportType() {
        return transportType;
    }

    public VoucherCost getVoucherCost() {
        return voucherCost;
    }

    public Integer getDays() {
        return days;
    }

    public Integer getNights() {
        return nights;
    }

    @Override
    public String toString() {
        return "Voucher{" +
                "id=" + id +
                ", date=" + date +
                ", hotel=" + hotel +
                ", country='" + country + '\'' +
                ", voucherType=" + voucherType +
                ", transportType=" + transportType +
                ", cost=" + voucherCost +
                ", days=" + days +
                ", nights=" + nights +
                '}';
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Voucher(Long id, Date date, Hotel hotel, String country, VoucherType voucherType, TransportType transportType, VoucherCost voucherCost, Integer days, Integer nights) {
        this.id = id;
        this.date = date;
        this.hotel = hotel;
        this.country = country;
        this.voucherType = voucherType;
        this.transportType = transportType;
        this.voucherCost = voucherCost;
        this.days = days;
        this.nights = nights;
    }

    public void setNights(Integer nights) {
        this.nights = nights;
    }

    private Integer nights;

    public Voucher(Voucher another) {
        this.id = another.id;
        this.date = another.date;
        this.hotel = new Hotel(another.hotel);
        this.country = another.country;
        this.voucherType = another.voucherType;
        this.transportType =another.transportType;
        this.voucherCost = new VoucherCost(another.voucherCost);
        this.days = another.days;
        this.nights = another.nights;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setHotel(Hotel hotel) {
        this.hotel = hotel;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setVoucherType(VoucherType voucherType) {
        this.voucherType = voucherType;
    }

    public void setTransportType(TransportType transportType) {
        this.transportType = transportType;
    }

    public void setVoucherCost(VoucherCost voucherCost) {
        this.voucherCost = voucherCost;
    }

    public Voucher() {
    }

}
