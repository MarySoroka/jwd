package com.vouchers.application;

import com.vouchers.command.*;
import com.vouchers.dao.TouristVouchersDao;
import com.vouchers.dao.TouristVouchersDaoImpl;
import com.vouchers.parsers.XMLParserFactory;
import com.vouchers.service.VoucherService;
import com.vouchers.service.VoucherServiceImp;

import java.util.HashMap;
import java.util.Map;

public class ApplicationContext {
    public static final String SCHEMA_FILE = "/vouchers.xsd";
    public static  ApplicationContext instance;

    public static ApplicationContext getInstance() {
        if (instance == null){
            instance = new ApplicationContext();
            instance.initialization();
        }
        return instance;
    }

    public Object getBean(Class<?> clazz) {
        return beans.get(clazz);
    }

    private Map<Class<?>, Object> beans = new HashMap<>();

    public void initialization() {
        TouristVouchersDao touristVouchersDao = new TouristVouchersDaoImpl();
        VoucherService voucherService = new VoucherServiceImp(touristVouchersDao);
        CommandProvider commandProvider = new CommandProviderImpl();
        commandProvider.setCommand(CommandType.DEFAULT, new DefaultCommand());
        commandProvider.setCommand(CommandType.DISPLAY_VOUCHERS, new DisplayVouchersCommand(voucherService));
        commandProvider.setCommand(CommandType.UPLOAD, new UploadVouchersCommand(voucherService, new XMLParserFactory<>()));
        commandProvider.setCommand(CommandType.DOWNLOAD, new DownloadVouchersCommand(voucherService));
        beans.put(CommandProvider.class, commandProvider);
        beans.put(VoucherService.class, voucherService);
        beans.put(TouristVouchersDao.class, touristVouchersDao);
    }

    public void destroy() {
        beans.clear();
    }

}
