package com.vouchers.builder;

import com.vouchers.entity.VoucherCost;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class VoucherCostBuilder implements EntityBuilder<VoucherCost> {
    private static final Logger LOGGER = LogManager.getLogger(VoucherCostBuilder.class);

    @Override
    public VoucherCost build(Map<String, String> data) throws BuildException {
        try {
            LOGGER.info("Start build voucher cost");
            VoucherCost voucherCost = new VoucherCost();
            voucherCost.setCost(Long.valueOf(data.get(VoucherField.COST.getFieldName())));
            Set<String> items = new HashSet<>();
            for (int i = 0; i < Integer.parseInt(data.get("itemAmount")); i++) {
                items.add(data.get(VoucherField.ITEM.getFieldName() + i));
            }
            voucherCost.setIncludedCharacteristics(items);
            return voucherCost;
        } catch (Exception e) {
            LOGGER.error("Couldn't build hotel room. Fatal error {}", e.getMessage());
            throw new BuildException("Couldn't build hotel room. Fatal error ", e);
        }
    }

    @Override
    public String buildXml(VoucherCost object) {
        StringBuilder stringBuilder = new StringBuilder();
        for (String s:
             object.getIncludedCharacteristics()) {
            stringBuilder.append("<item>").append(s).append("</item>\n");
        }
        return "<voucher-cost>\n" +
                "<cost>"+object.getCost()+"</cost>\n" +
                "<included-characteristics>\n" +
                stringBuilder +
                "</included-characteristics>\n" +
                "</voucher-cost>\n";
    }
}
