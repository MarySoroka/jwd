package com.vouchers.entity;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Hotel {
    public void setHotelCharacteristic(List<HotelCharacteristic> hotelCharacteristic) {
        this.hotelCharacteristic = hotelCharacteristic;
    }

    public Hotel(List<HotelCharacteristic> hotelCharacteristic) {
        this.hotelCharacteristic = hotelCharacteristic;
    }
    public Hotel(Hotel another) {
        List<HotelCharacteristic> hotelCharacteristics = new ArrayList<>();
        hotelCharacteristics.add(new HotelStars((HotelStars) another.getHotelCharacteristic().get(0)));
        hotelCharacteristics.add(new Nutrition((Nutrition) another.getHotelCharacteristic().get(1)));
        hotelCharacteristics.add(new HotelRoom((HotelRoom) another.getHotelCharacteristic().get(2)));
        this.hotelCharacteristic = hotelCharacteristics;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Hotel hotel = (Hotel) o;
        return Objects.equals(hotelCharacteristic, hotel.hotelCharacteristic);
    }

    @Override
    public int hashCode() {
        return Objects.hash(hotelCharacteristic);
    }

    public List<HotelCharacteristic> getHotelCharacteristic() {
        return hotelCharacteristic;
    }

    @Override
    public String toString() {
        return "Hotel{" +
                "hotelCharacteristic=" + hotelCharacteristic +
                '}';
    }

    private List<HotelCharacteristic> hotelCharacteristic;
    public Hotel() {
    }
}
