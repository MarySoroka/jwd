package com.vouchers.entity;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class RoomTypeTest extends Assert {
    @Test
    public void getByNameRoomTypeIsRight() {
        int type = 1;
        RoomType roomType = RoomType.getByName(type);
        assertEquals(RoomType.SINGLE, roomType);

        type = 2;
        roomType = RoomType.getByName(type);
        assertEquals(RoomType.DOUBLE, roomType);

        type = 3;
        roomType = RoomType.getByName(type);
        assertEquals(RoomType.TRIPLE, roomType);

        type = 4;
        roomType = RoomType.getByName(type);
        assertEquals(RoomType.FOUR_SEATED, roomType);

        type = 10;
        roomType = RoomType.getByName(type);
        assertEquals(RoomType.SINGLE, roomType);
    }


}
