package com.vouchers.command;

import com.vouchers.application.ApplicationContext;
import com.vouchers.service.VoucherService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class CommandProviderTest extends Assert {
    private CommandProvider commandProvider;
    private ApplicationContext applicationContext;

    @Before
    public void setProvider() {
        applicationContext = ApplicationContext.getInstance();
        applicationContext.initialization();
        commandProvider = (CommandProvider) applicationContext.getBean(CommandProvider.class);
    }

    @Test
    public void getCommandIsRight() {
        assertEquals(UploadVouchersCommand.class, commandProvider.getCommand(CommandType.UPLOAD).getClass());
    }

    @Test
    public void deleteCommandIsRight() {
        commandProvider.deleteCommand(CommandType.DISPLAY_VOUCHERS);
        assertNotEquals(DisplayVouchersCommand.class, commandProvider.getCommand(CommandType.DISPLAY_VOUCHERS).getClass());

    }

    @Test
    public void setCommandIsRight() {
        commandProvider.setCommand(CommandType.DISPLAY_VOUCHERS, new DisplayVouchersCommand((VoucherService) applicationContext.getBean(VoucherService.class)));
        assertEquals(DisplayVouchersCommand.class, commandProvider.getCommand(CommandType.DISPLAY_VOUCHERS).getClass());

    }

}
