package com.vouchers.validation;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ValidatorTest extends Assert {

    @Test
    public void validateForInvalidPath(){
        XSDValidator xsdValidator = new XSDValidator(new ValidationResult());
        try {
            xsdValidator.validate(this.getClass().getResourceAsStream("/vouchersTest1.xml"));
            assertTrue(xsdValidator.validationResult.isValid());
            xsdValidator.validate(this.getClass().getResourceAsStream("/vouchersTest3.xml"));
        } catch (ValidatorException e) {
            assertFalse(xsdValidator.validationResult.isValid());
        }

    }
}
