package com.vouchers.command;

import org.junit.Assert;
import org.junit.Test;

public class CommandTypeTest extends Assert {

    @Test
    public void getByNameCommandTypeDefaultIsRight() {
        String type = "DEFAULT";
        CommandType commandTypeTest = CommandType.getByName(type);
        assertEquals(CommandType.DEFAULT, commandTypeTest);
    }

    @Test
    public void getByNameCommandTypeGetAllIsRight() {
        String type = "DISPLAY_VOUCHERS";
        CommandType commandTypeTest = CommandType.getByName(type);
        assertEquals(CommandType.DISPLAY_VOUCHERS, commandTypeTest);
    }

    @Test
    public void getByNameCommandTypeNoneIsRight() {
        String type = "NONE";
        CommandType commandTypeTest = CommandType.getByName(type);
        assertEquals(CommandType.DEFAULT, commandTypeTest);
    }

}
