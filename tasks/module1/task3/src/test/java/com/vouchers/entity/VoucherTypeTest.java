package com.vouchers.entity;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class VoucherTypeTest extends Assert {
    @Test
    public void getByNameVoucherTypeIsRight() {
        String type = "cruise";
        VoucherType voucherType = VoucherType.getByName(type);
        assertEquals(VoucherType.CRUISE, voucherType);

        type = "pilgrimages";
        voucherType = VoucherType.getByName(type);
        assertEquals(VoucherType.PILGRIMAGES, voucherType);

        type = "excursion";
        voucherType = VoucherType.getByName(type);
        assertEquals(VoucherType.EXCURSION, voucherType);

        type = "rest";
        voucherType = VoucherType.getByName(type);
        assertEquals(VoucherType.REST, voucherType);

        type = "none";
        voucherType = VoucherType.getByName(type);
        assertEquals(VoucherType.REST, voucherType);
    }
}
