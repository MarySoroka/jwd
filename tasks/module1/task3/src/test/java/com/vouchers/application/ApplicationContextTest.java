package com.vouchers.application;

import com.vouchers.application.ApplicationContext;
import com.vouchers.command.*;
import com.vouchers.dao.TouristVouchersDao;
import com.vouchers.service.VoucherService;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

@RunWith(JUnit4.class)
public class ApplicationContextTest extends Assert {
    private Map<Class<?>, Object> beans = new HashMap<>();

    @Test
    public void initializationTest(){
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        applicationContext.initialization();
        assertNotNull(applicationContext.getBean(CommandProvider.class));
        assertNotNull(applicationContext.getBean(VoucherService.class));
        assertNotNull(applicationContext.getBean(TouristVouchersDao.class));
    }
}
