package com.vouchers.builder;

import com.vouchers.entity.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@RunWith(JUnit4.class)
public class VoucherBuilderTest extends Assert {
    private Voucher voucher;
    private  static final String VOUCHER_XML =
            "<voucher id=\"ID-1\">\n" +
            "<type>cruise</type>\n" +
            "<country>Greece</country>\n" +
            "<date>2020-07-22</date>\n" +
            "<days>18</days>\n" +
            "<nights>17</nights>\n" +
            "<transport>liner</transport>\n" +
            "<hotel>\n" +
            "<hotel-characteristics name=\"stars\">\n" +
            " <stars>4</stars>\n" +
            "</hotel-characteristics><hotel-characteristics name=\"nutrition\">\n" +
            " <nutrition>\n" +
            "<nutrition-type>Al</nutrition-type>\n" +
            "</nutrition>\n" +
            "</hotel-characteristics><hotel-characteristics name=\"room\">\n" +
            "<room>\n" +
            "<room-type>2</room-type>\n" +
            "<room-characteristics>\n" +
            "<characteristic>\n" +
            "<name>TV</name>\n" +
            "<availability>true</availability>\n" +
            "</characteristic>\n" +
            "<characteristic>\n" +
            "<name>Air Condition</name>\n" +
            "<availability>false</availability>\n" +
            "</characteristic>\n" +
            "<characteristic>\n" +
            "<name>Balcony</name>\n" +
            "<availability>false</availability>\n" +
            "</characteristic>\n" +
            "<characteristic>\n" +
            "<name>Hair dryer</name>\n" +
            "<availability>false</availability>\n" +
            "</characteristic>\n" +
            "</room-characteristics>\n" +
            "</room>\n" +
            "</hotel-characteristics>\n" +
            "</hotel><voucher-cost>\n" +
            "<cost>1000</cost>\n" +
            "<included-characteristics>\n" +
            "<item>Hair dryer</item>\n" +
            "<item>Nutrition</item>\n" +
            "<item>Transport</item>\n" +
            "<item>Room</item>\n" +
            "</included-characteristics>\n" +
            "</voucher-cost>\n" +
            "</voucher>";
            @Before
    public void createTestVoucher() throws ParseException {
        List<RoomCharacteristic> roomCharacteristicList = new ArrayList<>();
        roomCharacteristicList.add(new RoomCharacteristic("TV", true));
        roomCharacteristicList.add(new RoomCharacteristic("Air Condition", false));
        roomCharacteristicList.add(new RoomCharacteristic("Balcony", false));
        roomCharacteristicList.add(new RoomCharacteristic("Hair dryer", false));

        HotelRoom hotelRoom = new HotelRoom(RoomType.DOUBLE, roomCharacteristicList);
        Nutrition nutrition = new Nutrition(NutritionType.AL);
        HotelStars stars = new HotelStars((short) 4);
        List<HotelCharacteristic> hotelCharacteristics = new ArrayList<>();

        HotelCharacteristic hotelCharacteristic = stars;
        hotelCharacteristic.setName("stars");
        hotelCharacteristics.add(stars);

        hotelCharacteristic = nutrition;
        hotelCharacteristic.setName("nutrition");
        hotelCharacteristics.add(nutrition);

        hotelCharacteristic = hotelRoom;
        hotelCharacteristic.setName("room");
        hotelCharacteristics.add(hotelRoom);

        Hotel hotel = new Hotel(hotelCharacteristics);
        VoucherCost voucherCost = new VoucherCost(1000L, new HashSet<>(Arrays.asList("Room", "Transport", "Nutrition", "Hair dryer")));
        voucher = new Voucher(1L, new SimpleDateFormat("yyyy-mm-dd").parse("2020-07-22"), hotel, "Greece", VoucherType.CRUISE, TransportType.LINER, voucherCost, 18, 17);
    }
    @Test
    public void isVoucherBuilderRight(){
        VoucherBuilder voucherBuilder =  new VoucherBuilder(new VoucherCostBuilder(),
                new HotelBuilder(new HotelStarsBuilder(), new HotelRoomBuilder(), new NutritionBuilder()));
        assertEquals(voucherBuilder.buildXml(voucher),VOUCHER_XML);
    }
}
