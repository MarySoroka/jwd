package com.vouchers.command;

import com.vouchers.builder.BuildException;
import com.vouchers.dao.TouristVouchersDao;
import com.vouchers.dao.TouristVouchersDaoImpl;
import com.vouchers.entity.*;
import com.vouchers.parsers.ParseExecuteException;
import com.vouchers.service.VoucherService;
import com.vouchers.service.VoucherServiceImp;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

public class DownloadVouchersCommandTest extends Assert {
    private Voucher voucher;
    private VoucherService voucherService;
    @Mock
    private HttpServletRequest request ;
    @Mock
    private HttpServletResponse response;
    @Mock
    private RequestDispatcher r = new RequestDispatcher() {
        @Override
        public void forward(ServletRequest request, ServletResponse response) throws ServletException, IOException {

        }

        @Override
        public void include(ServletRequest request, ServletResponse response) throws ServletException, IOException {

        }
    };
    @Mock
    private ServletOutputStream outputStream;
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void createTestVoucher() throws ParseException {
        TouristVouchersDao inMemoryVouchers = new TouristVouchersDaoImpl();
        voucherService = new VoucherServiceImp(inMemoryVouchers);
        List<RoomCharacteristic> roomCharacteristicList = new ArrayList<>();
        roomCharacteristicList.add(new RoomCharacteristic("TV", true));
        roomCharacteristicList.add(new RoomCharacteristic("Air Condition", false));
        roomCharacteristicList.add(new RoomCharacteristic("Balcony", false));
        roomCharacteristicList.add(new RoomCharacteristic("Hair dryer", false));

        HotelRoom hotelRoom = new HotelRoom(RoomType.DOUBLE, roomCharacteristicList);
        Nutrition nutrition = new Nutrition(NutritionType.AL);
        HotelStars stars = new HotelStars((short) 4);
        List<HotelCharacteristic> hotelCharacteristics = new ArrayList<>();

        HotelCharacteristic hotelCharacteristic = stars;
        hotelCharacteristic.setName("stars");
        hotelCharacteristics.add(stars);

        hotelCharacteristic = nutrition;
        hotelCharacteristic.setName("nutrition");
        hotelCharacteristics.add(nutrition);

        hotelCharacteristic = hotelRoom;
        hotelCharacteristic.setName("room");
        hotelCharacteristics.add(hotelRoom);

        Hotel hotel = new Hotel(hotelCharacteristics);
        VoucherCost voucherCost = new VoucherCost(1000L, new HashSet<>(Arrays.asList("Room", "Transport", "Nutrition", "Hair dryer")));
        voucher = new Voucher(1L, new SimpleDateFormat("yyyy-mm-dd").parse("2020-07-22"), hotel, "Greece", VoucherType.CRUISE, TransportType.LINER, voucherCost, 18, 17);
    }

    @Test
    public void isParsedRight() throws IOException, ServletException, ParseExecuteException, BuildException {
        DownloadVouchersCommand parseCommand = new DownloadVouchersCommand(voucherService);
        voucherService.save(voucher);
        when(response.getOutputStream()).thenReturn(outputStream);
        doNothing().when(response).setContentType("text/xml");
        when(request.getRequestDispatcher("/jsp/vouchers/table.jsp")).thenReturn(r);
        doNothing().when(r).forward(request,response);
        parseCommand.executeCommand(request, response);
    }
}

