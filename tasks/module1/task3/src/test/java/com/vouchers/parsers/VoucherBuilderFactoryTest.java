package com.vouchers.parsers;


import com.vouchers.builder.*;
import com.vouchers.entity.Voucher;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.Optional;

@RunWith(JUnit4.class)
public class VoucherBuilderFactoryTest extends Assert {
    private final XMLParserFactory<Voucher> factory = new XMLParserFactory<Voucher>();
    private final EntityBuilder<Voucher> builder = new VoucherBuilder(new VoucherCostBuilder(),
            new HotelBuilder(new HotelStarsBuilder(), new HotelRoomBuilder(), new NutritionBuilder()));

    @Test
    public void getParserSAXTest() {
        SAXParser<Voucher> saxParser = new SAXParser<>(new VoucherBuilder(new VoucherCostBuilder(),
                new HotelBuilder(new HotelStarsBuilder(), new HotelRoomBuilder(), new NutritionBuilder())));
        assertEquals(saxParser.getClass(), factory.getParser("sax", builder).get().getClass());
    }

    @Test
    public void getParserDOMTest() {
        DomParser<Voucher> domParser = new DomParser<>(new VoucherBuilder(new VoucherCostBuilder(),
                new HotelBuilder(new HotelStarsBuilder(), new HotelRoomBuilder(), new NutritionBuilder())));
        assertEquals(domParser.getClass(), factory.getParser("dom", builder).get().getClass());
    }

    @Test
    public void getParserSTAXTest() {
        STAXParser<Voucher> staxParser = new STAXParser<>(new VoucherBuilder(new VoucherCostBuilder(),
                new HotelBuilder(new HotelStarsBuilder(), new HotelRoomBuilder(), new NutritionBuilder())));
        assertEquals(staxParser.getClass(), factory.getParser("stax", builder).get().getClass());
    }

    @Test
    public void getParserUnrealTest() {
        assertEquals(Optional.empty(), factory.getParser("s", builder));
    }

}
