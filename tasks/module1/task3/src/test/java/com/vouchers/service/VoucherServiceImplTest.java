package com.vouchers.service;

import com.vouchers.application.ApplicationContext;
import com.vouchers.builder.*;
import com.vouchers.entity.Voucher;
import com.vouchers.parsers.DomParser;
import com.vouchers.parsers.DomParserTest;
import com.vouchers.parsers.ParseExecuteException;
import com.vouchers.service.VoucherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.List;

@RunWith(JUnit4.class)
public class VoucherServiceImplTest {
    private static final Logger LOGGER = LogManager.getLogger(DomParserTest.class);
    private ApplicationContext applicationContext = ApplicationContext.getInstance();


    @Test
    public void isParsedRight() throws ParseExecuteException, BuildException {
        VoucherBuilder voucherBuilder = new VoucherBuilder(new VoucherCostBuilder(),
                new HotelBuilder(new HotelStarsBuilder(), new HotelRoomBuilder(), new NutritionBuilder()));
        DomParser<Voucher> domParser = new DomParser<>(voucherBuilder);
        List<Voucher> voucher = domParser.parse(this.getClass().getResourceAsStream("/vouchers.xml"));
        VoucherService voucherService = (VoucherService) applicationContext.getBean(VoucherService.class);
        for (Voucher v:
             voucher) {
            voucherService.save(v);
        }
        LOGGER.info(voucherService.getXmlFile(voucherBuilder));
    }
}
