package com.vouchers.parsers;


import com.vouchers.builder.*;
import com.vouchers.entity.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

@RunWith(JUnit4.class)
public class DomParserTest extends Assert {
    private Voucher voucher;

    @Before
    public void createTestVoucher() throws ParseException {
        List<RoomCharacteristic> roomCharacteristicList = new ArrayList<>();
        roomCharacteristicList.add(new RoomCharacteristic("TV", true));
        roomCharacteristicList.add(new RoomCharacteristic("Air Condition", false));
        roomCharacteristicList.add(new RoomCharacteristic("Balcony", false));
        roomCharacteristicList.add(new RoomCharacteristic("Hair dryer", false));

        HotelRoom hotelRoom = new HotelRoom(RoomType.DOUBLE, roomCharacteristicList);
        Nutrition nutrition = new Nutrition(NutritionType.AL);
        HotelStars stars = new HotelStars((short) 4);
        List<HotelCharacteristic> hotelCharacteristics = new ArrayList<>();

        HotelCharacteristic hotelCharacteristic = stars;
        hotelCharacteristic.setName("stars");
        hotelCharacteristics.add(stars);

        hotelCharacteristic = nutrition;
        hotelCharacteristic.setName("nutrition");
        hotelCharacteristics.add(nutrition);

        hotelCharacteristic = hotelRoom;
        hotelCharacteristic.setName("room");
        hotelCharacteristics.add(hotelRoom);

        Hotel hotel = new Hotel(hotelCharacteristics);
        VoucherCost voucherCost = new VoucherCost(1000L, new HashSet<>(Arrays.asList("Room", "Transport", "Nutrition", "Hair dryer")));
        voucher = new Voucher(1L, new SimpleDateFormat("yyyy-mm-dd").parse("2020-07-22"), hotel, "Greece", VoucherType.CRUISE, TransportType.LINER, voucherCost, 18, 17);
    }

    @Test
    public void isParsedRight() throws ParseExecuteException, BuildException {

        DomParser<Voucher> domParser = new DomParser<>(new VoucherBuilder(new VoucherCostBuilder(),
                new HotelBuilder(new HotelStarsBuilder(), new HotelRoomBuilder(), new NutritionBuilder())));
        List<Voucher> v = domParser.parse(this.getClass().getResourceAsStream("/vouchersTest1.xml"));
        Voucher voucherTest = v.get(0);
        assertEquals(voucher.toString(), voucherTest.toString());
    }
}
