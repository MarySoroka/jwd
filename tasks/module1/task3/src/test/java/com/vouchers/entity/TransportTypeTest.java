package com.vouchers.entity;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TransportTypeTest extends Assert {
    @Test
    public void getByNameTransportTypeIsRight() {
        String type = "Liner";
        TransportType transportType = TransportType.getByName(type);
        assertEquals(TransportType.LINER, transportType);

        type = "Car";
        transportType = TransportType.getByName(type);
        assertEquals(TransportType.CAR, transportType);

        type = "Train";
        transportType = TransportType.getByName(type);
        assertEquals(TransportType.TRAIN, transportType);

        type = "Bus";
        transportType = TransportType.getByName(type);
        assertEquals(TransportType.BUS, transportType);

        type = "Airplane";
        transportType = TransportType.getByName(type);
        assertEquals(TransportType.AIRPLANE, transportType);

        type = "Default";
        transportType = TransportType.getByName(type);
        assertEquals(TransportType.CAR, transportType);
    }


}