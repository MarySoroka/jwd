package com.vouchers.command;


import com.vouchers.builder.BuildException;
import com.vouchers.dao.TouristVouchersDao;
import com.vouchers.dao.TouristVouchersDaoImpl;
import com.vouchers.entity.*;
import com.vouchers.parsers.FileParser;
import com.vouchers.parsers.ParseExecuteException;
import com.vouchers.parsers.XMLParserFactory;
import com.vouchers.service.VoucherService;
import com.vouchers.service.VoucherServiceImp;
import com.vouchers.validation.ValidationResult;
import com.vouchers.validation.Validator;
import com.vouchers.validation.ValidatorException;
import com.vouchers.validation.XSDValidator;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.xml.sax.SAXException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

@RunWith(JUnit4.class)
public class UploadVouchersCommandTest extends Assert {
    private Voucher voucher;
    private VoucherService voucherService;
    @Mock
    private HttpServletRequest request;
    @Mock
    private HttpServletResponse response;

    @Mock
    private XSDValidator validator = new XSDValidator(new ValidationResult());
    @Mock
    private FileParser<Voucher> parser;
    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Before
    public void createTestVoucher() throws ParseException {
        TouristVouchersDao inMemoryVouchers = new TouristVouchersDaoImpl();
        voucherService = new VoucherServiceImp(inMemoryVouchers);
        List<RoomCharacteristic> roomCharacteristicList = new ArrayList<>();
        roomCharacteristicList.add(new RoomCharacteristic("TV", true));
        roomCharacteristicList.add(new RoomCharacteristic("Air Condition", false));
        roomCharacteristicList.add(new RoomCharacteristic("Balcony", false));
        roomCharacteristicList.add(new RoomCharacteristic("Hair dryer", false));

        HotelRoom hotelRoom = new HotelRoom(RoomType.DOUBLE, roomCharacteristicList);
        Nutrition nutrition = new Nutrition(NutritionType.AL);
        HotelStars stars = new HotelStars((short) 4);
        List<HotelCharacteristic> hotelCharacteristics = new ArrayList<>();

        HotelCharacteristic hotelCharacteristic = stars;
        hotelCharacteristic.setName("stars");
        hotelCharacteristics.add(stars);

        hotelCharacteristic = nutrition;
        hotelCharacteristic.setName("nutrition");
        hotelCharacteristics.add(nutrition);

        hotelCharacteristic = hotelRoom;
        hotelCharacteristic.setName("room");
        hotelCharacteristics.add(hotelRoom);

        Hotel hotel = new Hotel(hotelCharacteristics);
        VoucherCost voucherCost = new VoucherCost(1000L, new HashSet<>(Arrays.asList("Room", "Transport", "Nutrition", "Hair dryer")));
        voucher = new Voucher(1L, new SimpleDateFormat("yyyy-mm-dd").parse("2020-07-22"), hotel, "Greece", VoucherType.CRUISE, TransportType.LINER, voucherCost, 18, 17);
    }

    @Test
    public void isParsedRight() throws ParserConfigurationException, IOException, SAXException, XMLStreamException, ParseException, ServletException, ParseExecuteException, BuildException, ValidatorException {
        UploadVouchersCommand parseCommand = new UploadVouchersCommand(voucherService, new XMLParserFactory<>());
        List<Voucher> vouchers = new ArrayList<>();

        vouchers.add(voucher);
        Part xmlFile = new Part() {
            @Override
            public InputStream getInputStream() throws IOException {
                return this.getClass().getResourceAsStream("/vouchersTest1.xml");
            }

            @Override
            public String getContentType() {
                return null;
            }

            @Override
            public String getName() {
                return null;
            }

            @Override
            public String getSubmittedFileName() {
                return null;
            }

            @Override
            public long getSize() {
                return 10;
            }

            @Override
            public void write(String fileName) throws IOException {

            }

            @Override
            public void delete() throws IOException {

            }

            @Override
            public String getHeader(String name) {
                return null;
            }

            @Override
            public Collection<String> getHeaders(String name) {
                return null;
            }

            @Override
            public Collection<String> getHeaderNames() {
                return null;
            }
        };

        when(request.getPart("xmlFile")).thenReturn(xmlFile);
        when(request.getParameter("parser")).thenReturn("dom");
        when(parser.parse(request.getInputStream())).thenReturn(vouchers);
        parseCommand.execute(request, response);

        when(request.getPart("xmlFile")).thenReturn(xmlFile);
        when(request.getParameter("parser")).thenReturn("sax");
        when(parser.parse(request.getInputStream())).thenReturn(vouchers);
        parseCommand.execute(request, response);

        when(request.getPart("xmlFile")).thenReturn(xmlFile);
        when(request.getParameter("parser")).thenReturn("stax");
        when(parser.parse(request.getInputStream())).thenReturn(vouchers);
        parseCommand.execute(request, response);


    }
}
