package com.vouchers.entity;

import org.junit.Assert;
import org.junit.Test;

public class NutritionTypeTest extends Assert {
    @Test
    public void getByNameNutritionTypeIsRight() {
        String type = "Al";
        NutritionType nutritionType = NutritionType.getByName(type);
        assertEquals(NutritionType.AL, nutritionType);

        type = "HB";
        nutritionType = NutritionType.getByName(type);
        assertEquals(NutritionType.HB, nutritionType);

        type = "BB";
        nutritionType = NutritionType.getByName(type);
        assertEquals(NutritionType.BB, nutritionType);

        type = "jkk";
        nutritionType = NutritionType.getByName(type);
        assertEquals(NutritionType.NONE, nutritionType);
    }

}
