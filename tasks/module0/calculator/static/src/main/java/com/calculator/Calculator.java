package com.calculator;
import java.util.Scanner;
import java.util.Stack;
public class Calculator {
    public static void main(String[] args) throws Exception {
        Scanner scanner = new Scanner(System.in);
        boolean isRunning = true;
        while (isRunning){
            System.out.println("Please,enter yur expression or exit: \n");
            String expression = scanner.nextLine();
            if(expression.equals("exit")){
                isRunning = false;
            }else{
                System.out.println(parseString(expression));
            }
        }
    }
    private static int priority(String operator){
        switch (operator) {
            case "*":
            case "/":
            case "%":
                return 2;
            case "+":
            case "-":
                return 1;
            default:
                return 0;
        }
    }

    private static void evaluateExpression(Stack<Double> operands, String operator) throws Exception {
        double operand2 = operands.pop();
        double operand1 = operands.pop();
        double result = 0;
        switch (operator){
            case "+":
                result = operand1 + operand2;
                break;
            case "-":
                result = operand1 - operand2;
                break;
            case "*":
                result = operand1 * operand2;
                break;
            case "/":
                result = operand1 / operand2;
                break;
        }
        operands.push(result);
    }
    private static String parseString(String expession) throws Exception {
        String[] expressionOperators = expession.split("\\s*[0-9]+\\s*");
        String[] expressionOperands = expession.split("\\s*[+-/%*]\\s*");
        if(expressionOperands.length == 1 || expressionOperands.length - expressionOperators.length+1 != 1){
            return "Please, enter correct expression";
        }
        Stack<Double> operands = new Stack<Double>();
        Stack<String> operators = new Stack<String>();
        int j = 0;
        for (int i = 1; i < expressionOperators.length; i++){
            operands.push(Double.valueOf(expressionOperands[j]));
            while (!operators.isEmpty()&&((priority(operators.peek()) >= priority(expressionOperators[i])))){
                String op = operators.pop();
                evaluateExpression(operands, op);
            }
            operators.push(expressionOperators[i]);
            j++;

        }
        operands.push(Double.valueOf(expressionOperands[j]));
        while (!operators.isEmpty()){
            String op = operators.pop();
            evaluateExpression(operands, op);
        }
        return String.valueOf(operands.pop());
    }

}
