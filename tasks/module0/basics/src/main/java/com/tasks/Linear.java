package com.tasks;

import static java.lang.Math.*;

public class Linear {
    public static void task1(float x, float y){
        System.out.println("task1");
        System.out.println((x + y));
        System.out.println((x - y));
        System.out.println(x * y);
        System.out.println(x / y);
    }
    public static void task2(int x)
    {
        System.out.println("task2");
        System.out.println(x+3);
    }
    public static void task3(int x, int y){
        System.out.println("task3");
        int expression = 2*x+(y-2)*5;
        System.out.println(expression);
    }
    public static void task4(int a, int b, int c){
        System.out.println("task4");
        int expression =((a-3)*b/2)+c;
        System.out.println(expression);

    }
    public static void task5(float x, float y){
        System.out.println("task5");
        float avarage = ((x+y)/2);
        System.out.println(avarage);

    }
    public static void task6(float n, float m){
        System.out.println("task6");
        float litersInBigCan = (80/n+12)*m;
        System.out.println(litersInBigCan);

    }
    public static void task7(float length){
        System.out.println("task7");
        float area = (length/2)*length;
        System.out.println(area);

    }
    public static void task8(float a, float b , float c){
        System.out.println("task8");
        double result = (b+sqrt(b*b+4*a*c))/2*a;
        double expression = result-pow(a,3)*c+pow(b,-2);
        System.out.println(expression);
    }
    public static void task9(double d){
        System.out.println("task9");
        System.out.println(-1/d);
    }
    public static void task10(double x, double y){
        System.out.println("task10");
        double expression = (sin(x)+cos(y))/(cos(x)-sin(y))*tan(x*y);
        System.out.println(expression);
    }
    public static void task11(double x, double y){
        System.out.println("task11");
        double area = x*y/2;
        double perimeter = sqrt(x*x+y*y)+x+y;
        System.out.println(area);
        System.out.println(perimeter);
    }
    public static void task12(double x1, double y1, double x2, double y2){
        System.out.println("task12");
        double distance  = sqrt(pow(x2-x1,2)+ pow(y2-y1,2));
        System.out.println(distance);
    }
    private static double distance(double x1,double y1,double x2,double y2){
        return sqrt(pow(x2-x1,2)+ pow(y2-y1,2));
    }
    public static void task13(double x1, double y1, double x2, double y2,double x3, double y3){
        System.out.println("task13");
        double a = distance(x1,y1,x2,y2);
        double b = distance(x1,y1,x3,y3);
        double c = distance(x2,y2,x3,y3);
        double perimeter = a+b+c;
        double area = sqrt(perimeter*(perimeter-a)*(perimeter-b)*(perimeter-c));
        System.out.println(perimeter);
        System.out.println(area);
    }
    public static void task14(double radius){
        System.out.println("task14");
        double lengthOfCircle = 2*Math.PI*radius;
        double areaOfCircle =  Math.PI*radius*radius;
        System.out.println(lengthOfCircle);
        System.out.println(areaOfCircle);
    }
    public static void task15(){
        System.out.println("task15");
        for (int i = 0; i< 4; i++){
            System.out.println(pow(Math.PI,i));}
    }
    public static void task16(int number){
        System.out.println("task16");
        String x = String.valueOf(number);
        int result = 1;
        for(int i = 0; i<4;i++){
            result *= Integer.parseInt(String.valueOf(x.charAt(i)));
        }
        System.out.println(result);
    }
    public static void task17(double x, double y){
        System.out.println("task17");
        double a = pow(x, 3);
        double b = pow(y,3);
        double arithmeticAverage = ((a+b)/2);
        double geometryAverage = sqrt(Math.abs(x)*Math.abs(y));
        System.out.println(arithmeticAverage);
        System.out.println(geometryAverage);
    }
    public static void task18(double edge){
        System.out.println("task18");
        double areaOfVerge = edge*edge;
        double areaOfCube = 6 *edge*edge;
        double volumeOfCube =  pow(edge,3);
        System.out.println(areaOfVerge);
        System.out.println(areaOfCube);
        System.out.println(volumeOfCube);
    }
    public static void task19(double side){
        System.out.println("task19");
        double area = side*side* sqrt(3)/4;
        double height = side* sqrt(3)/2;
        double inscribedRadius = sqrt(3)*side/6;
        double describedRadius = sqrt(3)*side/3;
        System.out.println(area);
        System.out.println(height);
        System.out.println(inscribedRadius);
        System.out.println(describedRadius);
    }
    public static void task20(double length){
        System.out.println("task20");
        double radius=  length/(Math.PI*2);
        double areaOfCircle = Math.PI*radius*radius;
        System.out.println(areaOfCircle);
    }
    public static void task21(int num){
        System.out.println("task21");
        String number = String.valueOf(num);
        String number1 ="";
        for(int i = 6; i >= 0; i--){
            number1 += number.charAt(i);
        }
        System.out.println(number1);
    }
    public static void task22(int time){
        System.out.println("task22");
        int hours = time/3600;
        int minutes = time/60 - hours*60;
        int seconds = time - minutes*60 - hours *3600;
        System.out.printf("%02dч %02dмин %02dсек \n", hours, minutes,seconds);
    }
    public static void task23(double r, double R){
        System.out.println("task23");
        double areaOfRing = Math.PI*(R*R-r*r);
        System.out.println(areaOfRing);
    }
    public static void task24(double a, double b, double angle){
        System.out.println("task24");
        double areaOfTrapeze = a*b/Math.sin(angle);
        System.out.println(areaOfTrapeze);
    }
    public static void task25(double a, double b, double c){
        System.out.println("task25");
        double discriminant = sqrt(b*b-4*a*c);
        System.out.println((-b+discriminant)/2*a);
        System.out.println((-b-discriminant)/2*a);
    }
    public static void task26(double a, double b, double angle){
        System.out.println("task26");
        System.out.println(a*b*Math.abs(Math.sin(angle)/2));
    }

    public static void task27(double a){
        System.out.println("task27");
        double c= a*a;
        for(int i = 0; i<3;i++){
            a *=a;
        }
        System.out.println(a);
        System.out.println(a*c);
    }
    public static void task28(double rad){
        System.out.println("task28");
        System.out.println(Math.toDegrees(rad));
        System.out.println(rad*60*180/Math.PI);
        System.out.println(rad*3600*180/Math.PI);
    }
    private static double findAngle(double b, double c, double a){
        return Math.acos((b*b+c*c-a*a)/2/b/c);
    }
    public static void task29(double a, double b, double c){
        System.out.println("task29");
        System.out.printf("%f %f %f in radians \n",findAngle(b,c,a),findAngle(a,b,c),findAngle(a,c,b));
        System.out.printf("%f %f %f in degrees \n",Math.toDegrees(findAngle(b,c,a)),Math.toDegrees(findAngle(a,b,c)),Math.toDegrees(findAngle(a,c,b)));
    }
    public static void task30(double R1, double R2, double R3){
        System.out.println("task30");
        System.out.println(1/R1+1/R2+1/R3);
    }
    public static void task31(float boatSpeed, float streamSpeed, float upstreamTime, float downstreamTime){
        System.out.println("task31");
        System.out.printf("%f Upstream \n",((boatSpeed- streamSpeed)*upstreamTime));
        System.out.printf("%f Downstream \n",((boatSpeed+ streamSpeed)*downstreamTime));
    }
    public static void task32(int hours, int minutes, int seconds, int p, int q, int r){
        System.out.println("task32");
        System.out.printf("%02d h %02d m %02d s\n", (hours+p)%24,(minutes+q)%60,(seconds+r)%60);
    }
    public static void task33(String ch){
        System.out.println("task33");
        System.out.printf("%d %c %c\n",ch.codePointAt(0),(char)(ch.codePointAt(0)+1),(char)(ch.codePointAt(0)-1));
    }
    public static void task34(long bytes){
        System.out.println("task34");
        System.out.printf("%f mega %f kilo %f giga\n",bytes*1.0E-6,bytes*1.0E-3,bytes*1.0E-9);
    }
    public static void task35(float m, float n){
        System.out.println("task35");
        String wholePart = String.valueOf((int)Math.floor(m/n));
        String franctional = String.valueOf(m/n - Math.floor(m/n));
        System.out.println(wholePart.charAt(wholePart.length()-1));
        System.out.println(franctional.charAt(2));
    }
    public static void task36(int num){
        System.out.println("task36");
        String number = String.valueOf(num);
        int odd =1;
        int even =1;
        for(int i = 0; i <4; i++){
            if((Character.getNumericValue(number.charAt(i)))%2==0){
                odd *=Character.getNumericValue(number.charAt(i));
            }else{
                even *=Character.getNumericValue(number.charAt(i));
            }
        }
        System.out.printf("%f \n ",(float)odd/even);
    }
    //37
    private static boolean check(int n){
        return (n % 2 == 0) && (n / 100 < 1);
    }
    private static boolean check(String n){
        return (Character.getNumericValue(n.charAt(0))+Character.getNumericValue(n.charAt(1))==Character.getNumericValue(n.charAt(2))+Character.getNumericValue(n.charAt(3)));
    }
    private static boolean check(String n,int a){
        return (Character.getNumericValue(n.charAt(0))+Character.getNumericValue(n.charAt(1))+Character.getNumericValue(n.charAt(2)))%2==0;
    }
    private static boolean check(int x,int x1, int x2){
        return x<=x2-x1;
    }
    private static boolean check(String n,float b){
        double cube= Character.getNumericValue(n.charAt(0))+Character.getNumericValue(n.charAt(1))+Character.getNumericValue(n.charAt(2));
        cube = pow(cube,3);
        double sqr = pow(Integer.parseInt(n),2);
        return cube == sqr;
    }
    private static boolean check(int a,int b, int c,boolean bol){
        return a==b||b==c||c==a;
    }
    private static boolean check(String n,String d){
        int a = Character.getNumericValue(n.charAt(0));
        int b = Character.getNumericValue(n.charAt(1));
        int c = Character.getNumericValue(n.charAt(2));
        return (a+b==c)||(a+c==b)||(b+c==a);
    }
    private static boolean check(int n,int a){
        for(int i = 0; i<4 ; i++){
            if(pow(a,i)==n){
                return true;
            }
        }
        return false;
    }
    private static boolean check(int a,int b,int c, int x, int y){
        return y==a*x*x+b*x+c;
    }
    public static void task37(int x){
        System.out.println("task37");
        System.out.println(check(98));
        System.out.println(check("1231"));
        System.out.println(check("123",3));
        System.out.println(check(3,1,2));
        System.out.println(check("123",1.6f));
        System.out.println(check("123","1"));
        System.out.println(check(1,1,2,true));
        System.out.println(check(1000,10));
        System.out.println(check(1,4,4,1,9));
    }
    public static void task38(int x, int y){
        System.out.println("task38");
        System.out.println(4- Math.abs(x));
        boolean res = (((x>=-2)&(x<=2))&((y>=0)&(y<=4)))||(((x<=4)&(x>=-4))&((y>=-3)&(y<=0)));
        System.out.println(res);
    }
    public static void task39(float x){
        System.out.println("task39");
        float res = 0;
        for(int i =0; i<5;i++){
            res+= pow(-x,i)*(6-i);
        }
        System.out.printf("%f \n ",res);
    }
    public static void task40(float x)
    {
        System.out.println("task40");
        float res1 = 1;
        float res2 =0;
        for(int i =1; i<4;i++){
            res1+= pow(x,i)*(i+1);
            res2+= pow(-x,i)*(i+1);

        }
        System.out.printf("%f %f",res1, res2);
    }
    public static void main(String[] args){
        task1(3,4);
        task2(3);
        task3(4,5);
        task4(2,3,4);
        task5(3,4);
        task6(3,4);
        task7(3);
        task8(2,3,4);
        task9(5);
        task10(2,3);
        task11(2,3);
        task12(1,2,3,4);
        task13(2,2,3,3,4,4);
        task14(30);
        task15();
        task16(1234567);
        task17(1,3);
        task18(4);
        task19(10);
        task20(10);
        task21(1234567);
        task22(123445);
        task23(2,33);
        task24(4,4,34);
        task25(44,34,5);
        task26(1,4,20);
        task27(4);
        task28(5);
        task29(1,2,3);
        task30(2.2f,3.3f,4.4f);
        task31(5,2,3,6);
        task32(10,12,34,21,34,56);
        task33("w");
        task34(2000);
        task35(2,3);
        task36(12345);
        task37(4);
        task38(2,5);
        task39(2);
        task40(2);
    }
}
