package com.tasks;

public class Branching {
    public static void task1(int a, int b){
        System.out.println("task1");
        if(a<b){
            System.out.println(7);
        }else{
            System.out.println(8);
        }
    }
    public static void task2(int a, int b)
    {
        System.out.println("task2");
        if (a < b) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }
    public static void task3(int a){
        System.out.println("task3");
        if(a<3){
            System.out.println("yes");
        }else{
            System.out.println("no");
        }
    }
    public static void task4(int a, int b){
        System.out.println("task4");
        if(a==b){
            System.out.println("yes");
        }else{
            System.out.println("no");
        }
    }
    public static void task5(int a, int b){
        System.out.println("task5");
        System.out.println(Math.min(a, b));
    }
    public static void task6(int a, int b){
        System.out.println("task6");
        System.out.println(Math.max(a, b));
    }
    public static void task7(int a, int b, int c, int x){
        System.out.println("task7");
        System.out.println(Math.abs(x*x*a+b*x+c));
    }
    public static void task8(int a, int b){
        System.out.println("task8");
        if(a<b){
            System.out.println(a*a);
        }else{
            System.out.println(b*b);
        }
    }
    public static void task9(int a, int b, int c){
        System.out.println("task9");
        System.out.println(a==b&a==c);
    }
    public static void task10(int r1, int r2){
        System.out.println("task10");
        if(r1>r2){
            System.out.println("area of r2");
        }else{
            System.out.println("area of r1");
        }
    }
    public static double area(int a, int b,int c){
        int perimetr = (a+b+c)/2;
        return Math.sqrt(perimetr*(perimetr-a)*(perimetr-b)*(perimetr-c));
    }
    public static void task11(int a1, int b1, int c1, int a2, int b2, int c2){
        System.out.println("task11");
        if(area(a1,b1,c1)>area(a2,b2,c2)){
            System.out.println("area of the first triangle");
        }else{
            System.out.println("area of the second triangle");
        }

    }
    public static void task12(float[] num){
        System.out.println("task12");
        for(int i = 0; i<3; i++){
            if(num[i]>0){
                System.out.println(Math.pow(num[i],2));
            }else{
                System.out.println(Math.pow(num[i],4));

            }
        }
    }
    public static void task13(int x1, int y1, int x2, int y2){
        System.out.println("task13");
        if(Math.sqrt(x1*x1+y1*y1)<Math.sqrt(x2*x2+y2*y2)){
            System.out.println("The first one is closer");
        }else{
            System.out.println("The second one is closer");
        }
    }
    public static void task14(int angle1, int angle2){
        System.out.println("task14");
        System.out.println(angle1+angle2<180);
    }
    public static void task15(float a, float b){
        System.out.println("task15");
        float temp =0;
        if(a<b){
            temp = a;
            a = (a+b)/2;
            b = temp*b*2;
        }else{
            temp = b;
            b = (a+b)/2;
            a = temp*a*2;
        }
        System.out.printf("a=%f b=%f \n",a,b);
    }
    public static void task16(int x, int y){
        System.out.println("task16");
        if(x>=0&y>=0){
            System.out.println("it is the first quarter");
        }else{
            if(x>=0&y<0){
                System.out.println("it is the second quarter");
            }else{
                if(y >= 0){
                    System.out.println("it is the fourth quarter");
                }else{
                    System.out.println("it is the fourth third");}

            }
        }
    }
    public static void task17(int a, int b){
        System.out.println("task17");
        if(a!=b){
            if(a<b){
                a = b;
            }else{
                b =a;
            }
        }else{
            a = 0;
            b =0;
        }
        System.out.printf("a=%d b=%d \n",a,b);
    }
    public static void task18(int a, int b, int c){
        System.out.println("task18");
        int count =0;
        if(a<0){
            count++;
        }
        if(b<0){
            count++;
        }
        if(c<0){
            count++;
        }
        System.out.printf("%d \n",count);
    }
    public static void task19(int a, int b, int c){
        System.out.println("task19");
        int count =0;
        if(a>0){
            count++;
        }
        if(b>0){
            count++;
        }
        if(c>0){
            count++;
        }
        System.out.printf("%d \n",count);
    }
    public static void task20(int a, int b, int c, int k){
        System.out.println("task20");
        if(a%k==0){
            System.out.println(a);
        }
        if(b%k==0){
            System.out.println(b);
        }
        if(c%k==0){
            System.out.println(c);
        }
    }
    public static void task21(String a){
        System.out.println("task21");
        System.out.println("Кто ты: мальчик или девочка? Введи Д или М");
        System.out.println(a);
        if(a.equals("Д")){
            System.out.println("Мне нравятся девочки");
        }else{
            System.out.println("Мне нравятся мальчики");
        }
    }
    public static void task22(int a, int b){
        System.out.println("task22");
        int temp =0;
        if(a<b) {
            temp = a;
            a = b;
            b = temp;
        }
        System.out.printf("a=%d b=%d \n",a,b);
    }
    public static void task23(int a, int b){
        System.out.println("task23");
        if(a<=0|a>12) {
            System.out.println("the month is incorrect");
        }
        if(b<=0|b>31){
            System.out.println("the day is incorrect");
        }
    }
    public static void task24(int n){
        System.out.println("task24");
        if(n%2==0) {
            System.out.println("don't like");
        }else{
            System.out.println("like");
        }
    }
    public static void task25(int n){
        System.out.println("task25");
        if(n>60) {
            System.out.println("Пожароопасная ситуация");}

    }
    public static void task26(int a, int b, int c){
        System.out.println("task26");
        if(a>b){
            if(a>c){
                if(b>c){
                    System.out.println(a+c);
                }else{
                    System.out.println(a+b);
                }
            }else{
                System.out.println(c+b);
            }
        }else{
            if(b>c){
                if(a>c){
                    System.out.println(c+b);
                }else{
                    System.out.println(a+b);
                }
            }else{
                System.out.println(a+c);
            }
        }

    }

    public static void task27(int a, int b, int c, int d){
        System.out.println("task27");
        System.out.println(Math.max(Math.min(a,b),Math.min(c,d)));
    }
    private static int max(int a, int b, int c){
        if (a>b){
            return Math.max(a, c);
        }else{
            return Math.max(b, c);
        }
    }
    public static void task28(int a, int b, int c, int d){
        System.out.println("task28");
            if(a==d){
                System.out.println(a);
            }else{
                if(b==d){
                    System.out.println(b);
                }else{
                    if(c==d){
                        System.out.println(c);
                    }else{
                        System.out.println(max(d-a,d-b,d-c));
                    }
                }
            }
    }
    public static void task29(int x1, int y1, int x2,int y2, int x3, int y3){
        System.out.println("task29");
        System.out.println((x1/y1==x2/y2)&&(x2/y2==x3/y3));
    }
    public static void task30(float a, float b, float c){
        System.out.println("task30");
        if (a > b) {
            if (b > c) {
                System.out.printf("%f %f %f \n", a*2, b*2, c*2);
            }
        } else {
            System.out.printf("%f %f %f \n", Math.abs(a), Math.abs(b), Math.abs(c));
        }
    }
    public static void task31(int a, int b, int x, int y, int z){
        System.out.println("task31");
        System.out.println((a==x&b==y)||(a==y&b==z)||(a==z&b==y)||(a==x&b==z||(a==z&b==x)||(a==y&b==x)));
    }
    public static void task32(int a, int b, int c){
        System.out.println("task32");
        if (a > 0) {
            if(b>0){
                System.out.println(a+b);
                if(c>0){
                    System.out.println(a+c);
                    System.out.println(b+c);
                }
            }else{
                System.out.println(a+c);
            }

        }else{
            if(c>0&b>0){
                System.out.println(b+c);
            }
        }
    }
    public static void task33(int password){
        System.out.println("task33");
        System.out.println("Password");
        String a = String.valueOf(password);
        System.out.println(a);
        if(a.equals("9583")||a.equals("1747")){
            System.out.println("ABC");
        }else{
            if(a.equals("3331")||a.equals("7922")){
                System.out.println("BC");
            }else{
                if(a.equals("9455")||a.equals("8997")){
                    System.out.println("C");
                }else{
                    System.out.println("Error");
                }
            }
        }
    }
    public static void task34(int cost, int money){
        System.out.println("task34");
        System.out.println("Cost");
        System.out.println(cost);
        System.out.println("Money");
        System.out.println(money);
        if(money==cost){
            System.out.println("Thanks");
        }else{
            if(money>cost){
                System.out.printf("Your odd money %d \n", money-cost);
            }else{
                System.out.println("Insufficient funds");
            }
        }
    }
    public static void task35(int day){
        System.out.println("task35");
        System.out.println("number");
        System.out.println(day);
        int[] daysInMonth = {31,28,31,30,31,30,31,31,30,31,30,31};
        int i =-1;
        do{
            i++;
            day -= daysInMonth[i];
        }while (day>0);
        System.out.printf("month %d day %d \n",i,daysInMonth[i]+day);
    }
    public static void task36(int x){
        System.out.println("task36");
        if(x<=3){
            System.out.println(x*x-3*x+9);
        }else{
            System.out.println(1/(Math.pow(x,3)-6));
        }
    }
    public static void task37(int x){
        System.out.println("task37");
        if(x>=3){
            System.out.println(-x*x+3*x+9);
        }else{
            System.out.println(1/(Math.pow(x,3)+6));
        }
    }
    public static void task38(int x){
        System.out.println("task38");
        if(x>=0&x<=3){
            System.out.println(x*x);
        }else{
            System.out.println(4);
        }
    }
    public static void task39(int x){
        System.out.println("task39");
        if(x>=8){
            System.out.println(-x*x+x-9);
        }else{
            System.out.println(1/(Math.pow(x,4)-6));
        }
    }
    public static void task40(int x)
    {
        System.out.println("task40");
        if (x <= 13) {
            System.out.println(Math.pow(-x, 3) + 9);
        } else {
            System.out.println(-3 / x + 1);
        }
    }
    public static void main(String[] args){
       task1(3,4);
       task2(3,4);
       task3(4);
       task4(2,2);
       task5(3,4);
       task6(3,4);
       task7(3,4,5,6);
       task8(2,3);
       task9(1,2,3);
       task10(2,3);
       task11(2,3,4,2,2,3);
       task12(new float[]{2.2f, -3.3f, 5.5f});
       task13(2,2,3,3);
       task14(30, 36);
       task15(-3.3f,4.4f);
       task16(1,-4);
       task17(1,3);
       task18(-2,-3,5);
       task19(1, 3, -4);
       task20(1, 4 , 3, 2);
       task21("Д");
       task22(2, 3);
       task23(2,33);
       task24(4);
       task25(44);
       task26(1,4,2);
       task27(1,2,3,4);
       task28(1,2,3,4);
       task29(1,2,3,4,5,6);
       task30(2.2f,3.3f,4.4f);
       task31(1,2,3,2,1);
       task32(1,-4,3);
       task33(3331);
       task34(2000,2101);
       task35(216);
       task36(2);
       task37(4);
       task38(2);
       task39(2);
       task40(2);
    }
}
