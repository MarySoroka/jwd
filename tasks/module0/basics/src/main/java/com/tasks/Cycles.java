package com.tasks;

import java.math.BigInteger;
public class Cycles {
    private static void task1(){
        System.out.println("task1");
        for(int i = 1; i<6; i++){
            System.out.println(i);
        }
    }
    public static void task2(){
        System.out.println("task2");
        for(int i = 5; i>0; i--){
            System.out.println(i);
        }
    }
    private static void task3(){
        System.out.println("task3");
        for(int i = 1; i<10; i++){
            System.out.println(i*3);
        }
    }
    private static void task4(){
        System.out.println("task4");
        int i = 2;
        while(i<=100){
            if(i%2==0){
                System.out.println(i);
            }
            i++;
        }
    }
    private static void task5(){
        System.out.println("task5");
        int i = 1;
        int sum = 0;
        while(i<=99){
            if(i%2!=0){
                sum +=i;
            }
            i++;
        }
        System.out.println(sum);
    }
    private static void task6(int n){
        System.out.println("task6");
        int i = 1;
        int sum = 0;
        while(i<=n){
            sum += i;
            i++;
        }
        System.out.println(sum);
    }
    private static void task7(float a, float b, float h){
        System.out.println("task7");
        float y = 0;
        for(float i = a; i<=b; i+=h){
            if(i>2){
                y+=i;
            }else{
                y-=i;
            }
        }
        System.out.println(y);

    }
    private static void task8(float a, float b, float c, float h){
        System.out.println("task8");
        float y = 0;
        for(float i = a; i<=b; i+=h){
            if(i==15){
                y+=(i+c)*2;
            }else{
                y+=(i-c)+6;
            }
        }
        System.out.println(y);
    }
    private static void task9(){
        System.out.println("task9");
        int i = 1;
        int sum = 0;
        while(i<=100){
            sum+= i*i;
            i++;
        }
        System.out.println(sum);
    }
    private static void task10(){
        System.out.println("task10");
        BigInteger mul = BigInteger.ONE;
        long temp;
        for (int i =2; i<=200;i++){
            temp = i*i;
            mul = mul.multiply(BigInteger.valueOf(temp));
        }
        System.out.println(mul);

    }
    private static void task11(){
        System.out.println("task11");
        long sum = 1;
        long temp;
        for (int i =2; i<=200;i++){
            temp = i*i*i;
            sum -= temp;
        }
        System.out.println(sum);
    }
    private static void task12(){
        System.out.println("task12");
        int mul = 1;
        int prev = 1;
        int cur = 0;
        for(int i = 0; i<10; i++){
            cur = 6+prev;
            prev = cur;
            mul *=cur;
        }
        System.out.println(mul);
    }
    private static void task13(){
        System.out.println("task13");
        for(float i = -5; i<=5; i+=0.5){
            float res = 5-i*i/2;
            System.out.printf("y = %.2f x = %.2f \n",res,i);
        }
    }
    private static void task14(int n){
        System.out.println("task14");
        float sum = 0;
        for(float i = 1 ; i<=n; i++){
            sum += 1/i;
        }
        System.out.println(sum);
    }
    private static void task15(){
        System.out.println("task15");
        long sum = 0;
        for(float i = 1 ; i<=10; i++){
            sum += Math.pow(2,i);
        }
        System.out.println(sum);
    }
    private static void task16(){
        System.out.println("task16");
        long sum = 0;
        long mul = 1;
        for(float i = 1 ; i<=10; i++){
            sum = 0;
            for(int j=1;j<=i;j++){
                sum += j;
            }
            mul *= sum;
        }
        System.out.println(mul);
        System.out.println(sum);
    }
    private static void task17(float a,int n){
        System.out.println("task17");
        float sum = 0;
        float mul = 1;
        for(float i = 1 ; i<n; i++){
            sum = a+i;
            mul *= sum;
        }
        System.out.println(mul);
    }
    private static void task18(float e){
        System.out.println("task18");
        int n =1;
        float sum = 0;
        while(Math.abs(Math.pow(-1,n-1)/n) >= e){
            sum += Math.pow(-1,n-1)/n;
            n++;
        }
        System.out.println(sum);
    }
    private static void task19(float e){
        System.out.println("task19");
        int n =1;
        float sum = 0;
        while(Math.abs(1/Math.pow(2,n)+1/Math.pow(3,n)) >= e){
            sum += 1/Math.pow(2,n)+1/Math.pow(3,n);
            n++;
        }
        System.out.println(sum);
    }
    private static void task20(float e){
        System.out.println("task20");
        int n =1;
        float sum = 0;
        while(Math.abs((float)1/((3*n-2)*(3*n+1))) >= e){
            sum = sum + (float)1/((3*n-2)*(3*n+1));
            n++;
        }
        System.out.println(sum);
    }
    private  static void task21(float a, float b, float h){
        System.out.println("task21");
        for(float i = a; i<=b; i+=h){
            double y = i-Math.sin(i);
            System.out.printf("x = %.2f y = %.2f \n",i,y);
        }
    }
    private static void task22(float a, float b, float h){
        System.out.println("task22");
        for(float i = a; i<=b; i+=h){
            double y = Math.pow(Math.sin(i),2);
            System.out.printf("x = %.2f y = %.2f \n",i,y);
        }
    }
    private static void task23(float a, float b, float h){
        System.out.println("task23");
        for(float i = a; i<=b; i+=h){
            double y = 1/Math.tan(i/3)+Math.sin(i)/2;
            System.out.printf("x = %.2f y = %.2f \n",i,y);
        }
    }
    private static void task24(int n){
        System.out.println("task24");
        String num = String.valueOf(n);
        String numRes = "";
        int sum = 0;
        int j = num.length()-1;
        for(int i = 0; i< num.length();i++){
            if(Character.getNumericValue(num.charAt(i))%2==0){
                sum += Character.getNumericValue(num.charAt(i));
            }
            numRes += num.charAt(j);
            j--;
        }
        System.out.println(numRes);
        System.out.println(sum);
    }
    private static void task25(int n){
        System.out.println("task25");
        long sum = 0;
        long mul = 1;
        for(long i = 1 ; i<n; i++){
            sum = i;
            mul *= sum;
        }
        System.out.println(mul);


    }

    public static void main(String[] args) {
        task1();
        task2();
        task3();
        task4();
        task5();
        task6(10);
        task7(-2f,2f, 0.5f);
        task8(-2f,2f,3f,0.5f);
        task9();
        task10();
        task11();
        task12();
        task13();
        task14(10);
        task15();
        task16();
        task17(1.5f,10);
        task18(0.5f);
        task19(0.5f);
        task20(0.5f);
        task21(-2f,2f,0.5f);
        task22(-2f,2f,0.5f);
        task23(-2f,3f,0.5f);
        task24(1234);
        task25(10);
    }
}
