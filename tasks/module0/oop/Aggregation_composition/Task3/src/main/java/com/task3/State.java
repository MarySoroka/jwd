package com.task3;

public class State implements StateService{
    private Region[] regions;

    public State(Region[] regions, City capital, double space) {
        this.regions = regions;
        this.capital = capital;
        this.space = space;
    }

    public Region[] getRegions() {
        return regions;
    }

    public void setRegions(Region[] regions) {
        this.regions = regions;
    }

    public void setCapital(City capital) {
        this.capital = capital;
    }

    public double getSpace() {
        return space;
    }

    public void setSpace(double space) {
        this.space = space;
    }

    private City capital;
    double space;

    @Override
    public City getCapital() {
        return this.capital;
    }

    @Override
    public String[] regionCenter() {
        String[] cities = new String[regions.length];
        int i = 0;
        for (Region region:regions) {
            cities[i] = region.getRegionCenter().getName();
            i++;
        }
        return cities;
    }

    @Override
    public Region[] outputRegions() {
        return this.regions;
    }

    @Override
    public double space() {
        return this.space;
    }
}
