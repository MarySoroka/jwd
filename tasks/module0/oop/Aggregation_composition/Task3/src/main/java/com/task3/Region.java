package com.task3;

public class Region {
    public Region(City[] cities, String name,City regionCenter) {
        this.cities = cities;
        this.name = name;
        this.regionCenter= regionCenter;
    }

    public City[] getCities() {
        return cities;
    }

    public void setCities(City[] cities) {
        this.cities = cities;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public City getRegionCenter() {
        return regionCenter;
    }

    public void setRegionCenter(City regionCenter) {
        this.regionCenter = regionCenter;
    }

    private City[] cities;
    private String name;
    private City regionCenter;
}
