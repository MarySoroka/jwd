package com.task3;

import java.util.Arrays;

public class App {
    public static void main(String[] args) {
        District[] districts ;
        Region[] regions = new Region[5];
        City[] cities = new City[5];
        for (int k = 0; k<5; k++){
            for (int j = 0; j<5;j++){
                districts = new District[10];
                for (int i = 0; i<10;i++){
                    District district = new District("district" +i);
                    districts[i] = district;
                }
                City city = new City("city"+j,districts,10000);
                cities[j] = city;
            }
            Region region = new Region(cities,"region"+k,cities[k]);
            regions[k] = region;

        }
        State state = new State(regions,regions[0].getCities()[0],101011.11);
        System.out.println(state.getCapital().getName());
        System.out.println(state.space);
        System.out.println(Arrays.toString(state.regionCenter()));
        System.out.println(state.getRegions().length);
    }
}
