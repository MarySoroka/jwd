package com.task3;

public interface StateService {
    City getCapital();
    String[] regionCenter();
    Region[] outputRegions();
    double space();

}
