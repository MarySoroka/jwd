package com.task3;

public class City {
    public City(String name, District[] districts, int population) {
        this.name = name;
        this.districts = districts;
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public District[] getDistricts() {
        return districts;
    }

    public void setDistricts(District[] districts) {
        this.districts = districts;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    private String name;
    private District[] districts;
    private int population;

}
