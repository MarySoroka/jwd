package com.task5.dao;


import com.task5.entity.FoodType;
import com.task5.entity.TransportType;
import com.task5.entity.Voucher;
import com.task5.entity.VoucherType;
import com.task5.service.VoucherService;

import java.util.ArrayList;
import java.util.Comparator;

public class Vouchers implements VoucherService {
    private ArrayList<Voucher> vouchers;

    public Vouchers() {
        this.vouchers = initialize();
    }

    @Override
    public void sortByDay() {
        Comparator<Voucher> comparator = Comparator.comparingInt(Voucher::getAmountOfDays);
        vouchers.sort(comparator);
    }

    @Override
    public void sortByVoucherType() {
        Comparator<Voucher> comparator = Comparator.comparing(Voucher::getFoodType);
        vouchers.sort(comparator);
    }

    @Override
    public void sortByFoodType() {
        Comparator<Voucher> comparator = Comparator.comparing(Voucher::getTransportType);
        vouchers.sort(comparator);
    }

    @Override
    public void sortByTransportType() {
        Comparator<Voucher> comparator = Comparator.comparing(Voucher::getTransportType);
        vouchers.sort(comparator);
    }

    @Override
    public Voucher getByName(String voucherType, String foodType, String transportType, int day) {
        for (Voucher v:vouchers) {
            if (v.getVoucherType().name().equals(voucherType) &&
                    v.getFoodType().name().equals(foodType) &&
                    v.getTransportType().name().equals(transportType) &&
                    (v.getAmountOfDays() == day)){
                return v;

            }
        }
        return null;
    }

    @Override
    public ArrayList<Voucher> initialize() {
        ArrayList<Voucher> vouchers = new ArrayList<>();
        vouchers.add(new Voucher(14, FoodType.ALL, TransportType.BUS, VoucherType.EXCURSION));
        vouchers.add(new Voucher(10, FoodType.ALL, TransportType.AIRPLANE, VoucherType.TREATMENT));
        vouchers.add(new Voucher(14, FoodType.ALL, TransportType.SHIP, VoucherType.CRUISE));
        vouchers.add(new Voucher(30, FoodType.BREAKFAST, TransportType.CAR, VoucherType.SHOPPING));
        vouchers.add(new Voucher(5, FoodType.BREAKFASTLAUNCH, TransportType.TRAIN, VoucherType.RELAXATION));
        return vouchers;
    }
    public ArrayList<Voucher> getVoucher() {
        return vouchers;
    }

    public void setVouchers(ArrayList<Voucher> vouchers) {
        this.vouchers = vouchers;
    }
}
