package com.task5;
import com.task5.dao.Vouchers;

public class App {
    public static void main(String[] args) {
        Vouchers vouchers = new Vouchers();
        vouchers.sortByDay();
        vouchers.sortByFoodType();
        vouchers.sortByTransportType();
        vouchers.sortByVoucherType();
        vouchers.getByName("relaxation", "bl", "train",5);
    }
}
