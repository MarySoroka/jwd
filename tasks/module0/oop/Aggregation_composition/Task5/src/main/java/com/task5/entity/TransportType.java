package com.task5.entity;

public enum TransportType {
    AIRPLANE("airplain"),
    SHIP("ship"),
    TRAIN("train"),
    BUS("bus"),
    CAR("car");
    private String transportType;
    TransportType(String transport) {
        this.transportType = transport;
    }
    TransportType getByName(String name){
        for (TransportType t: TransportType.values()) {
            if (t.name().equals(name)){
                return t;
            }
        }
        return null;
    }
}
