package com.task5.entity;

public enum VoucherType {
    RELAXATION("relaxation"),
    CRUISE("cruise"),
    TREATMENT("treatment"),
    SHOPPING("shopping"),
    EXCURSION("excursion");
    private String voucherType;
    VoucherType(String voucher) {
        this.voucherType = voucher;
    }
    VoucherType getByName(String name){
        for (VoucherType v:VoucherType.values()) {
            if (v.name().equals(name)){
                return v;
            }
        }
        return null;
    }
}
