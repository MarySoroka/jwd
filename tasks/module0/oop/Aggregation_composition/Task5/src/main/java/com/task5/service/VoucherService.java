package com.task5.service;

import com.task5.entity.Voucher;

import java.util.ArrayList;

public interface VoucherService {
    void sortByDay();
    void sortByVoucherType();
    void sortByFoodType();
    void sortByTransportType();
    Voucher getByName(String voucherType, String foodType, String transportType, int day);
    ArrayList<Voucher> initialize();
}
