package com.task5.entity;

public class Voucher {
    public int getAmountOfDays() {
        return amountOfDays;
    }

    public void setAmountOfDays(int amountOfDays) {
        this.amountOfDays = amountOfDays;
    }

    public FoodType getFoodType() {
        return foodType;
    }

    public void setFoodType(FoodType foodType) {
        this.foodType = foodType;
    }

    public TransportType getTransportType() {
        return transportType;
    }

    public void setTransportType(TransportType transportType) {
        this.transportType = transportType;
    }

    public VoucherType getVoucherType() {
        return voucherType;
    }

    public void setVoucherType(VoucherType voucherType) {
        this.voucherType = voucherType;
    }

    int amountOfDays;
    FoodType foodType;
    TransportType transportType;
    VoucherType voucherType;

    public Voucher(int amountOfDays, FoodType foodType, TransportType transportType, VoucherType voucherType) {
        this.amountOfDays = amountOfDays;
        this.foodType = foodType;
        this.transportType = transportType;
        this.voucherType = voucherType;
    }
}
