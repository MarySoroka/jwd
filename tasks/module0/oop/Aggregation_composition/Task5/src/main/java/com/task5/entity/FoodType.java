package com.task5.entity;

public enum FoodType {
    BREAKFAST("b"),
    BREAKFASTLAUNCH("bl"),
    ALL("a");

    private String foodType;
    FoodType(String type) {
        this.foodType = type;
    }
    FoodType getByName(String name){
        FoodType[] foodT = FoodType.values();
        for (FoodType f:foodT) {
            if (f.name().equals(name)){
                return f;
            }
        }
        return null;
    }
}
