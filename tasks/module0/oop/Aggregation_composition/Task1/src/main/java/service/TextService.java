package service;

public interface TextService {
    void addText(String text);
    String outputText();
    String outputHeader();
}
