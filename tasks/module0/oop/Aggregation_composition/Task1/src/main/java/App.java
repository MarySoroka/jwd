import module.Sentence;
import module.Text;
import module.Word;

import java.util.ArrayList;

public class App {
    public static void main(String[] args) {
        ArrayList<Word> words = new ArrayList<Word>();
        words.add(new Word("hi"));
        words.add(new Word("everyone"));
        ArrayList<Sentence> sentences = new ArrayList<Sentence>();
        sentences.add(new Sentence(words));
        Text text = new Text(sentences,"name");
        text.addText("lalala");
        String outtext = text.outputText();
        String name = text.outputHeader();
    }
}
