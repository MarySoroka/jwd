package module;

import service.TextService;

import java.util.ArrayList;

public class Text implements TextService {
    public ArrayList<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(ArrayList<Sentence> sentences) {
        this.sentences = sentences;
    }

    public Text(ArrayList<Sentence> sentences, String textName) {
        this.sentences = sentences;
        this.textName = textName;
    }

    private ArrayList<Sentence> sentences;

    public String getTextName() {
        return textName;
    }

    public void setTextName(String textName) {
        this.textName = textName;
    }

    private String textName;
    @Override
    public void addText(String text) {
          String[] words = text.split( " ");
          ArrayList<Word> sentence = new ArrayList<Word>();
          for (int i = 0; i < words.length; i++){
              sentence.add(new Word(words[i]));
          }
          this.sentences.add(new Sentence(sentence));
    }
    @Override
    public String outputText() {
          return this.sentences.toArray().toString();
    }
    @Override
    public String outputHeader() {
          return this.textName;
    }
}
