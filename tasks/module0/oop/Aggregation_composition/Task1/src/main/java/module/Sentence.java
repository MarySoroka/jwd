package module;

import java.util.ArrayList;

public class Sentence {
    public Sentence(ArrayList<Word> words) {
        this.words = words;
    }

    public ArrayList<Word> getWords() {
        return words;
    }

    public void setWords(ArrayList<Word> words) {
        this.words = words;
    }

    private ArrayList<Word> words;
}
