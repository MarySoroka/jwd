package com.task2;

public class App {
    public static void main(String[] args) {
        CarFactory carFactory = new CarFactory(new EngineFactory());
        Car car = carFactory.getCar("audi",new Wheel[]{});
        boolean isRunning = true;
        int fuel = 0 ;
        int energy = 0;
        int lastEnergy = 0;
        while (isRunning){
            fuel = car.fuel;
            car.fuel -=20;
            energy = car.engine.getEnergy(fuel);
            lastEnergy = car.engine.run(energy);
            if (lastEnergy>=4){
               for (Wheel wheel:car.wheelList) {
                  wheel.spin();
                  lastEnergy--;
               }
               car.runCar();
               System.out.println(car.outputCarModel());
            }else{
                car.refuel();
                for (Wheel wheel:
                     car.wheelList) {
                    wheel.changeWheel();
                }
                isRunning = car.engine.stop();
                car.stop();
            }
        }
    }
}
