package com.task2;


public class Car implements CarService {
    Wheel[] wheelList;
    Engine engine;
    int fuel;
    public Car(Engine engine, String carModel, Wheel... wheels) {
        this.carModel = carModel;
        this.engine =engine;
        this.wheelList =wheels;
        this.fuel = 100;
    }
    public String getCarModel() {
        return carModel;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    private String carModel;

    @Override
    public void runCar() {
    }
    @Override
    public String outputCarModel() {
        return getCarModel();
    }
    @Override
    public void stop() {

    }
    @Override
    public void refuel() {
        this.fuel = 100;
    }

}
