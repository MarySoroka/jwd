package com.task2;

public class EngineFactory implements Engine {

    @Override
    public int getEnergy(int fuel) {
        return fuel/20;
    }
    @Override
    public int run(int energy) {
        return energy-1;
    }
    @Override
    public boolean stop() {
        return false;
    }


}
