package com.task2;
public class CarFactory {
    private EngineFactory engineFactory;
    public CarFactory(EngineFactory engineFactory) {
        this.engineFactory = engineFactory;
    }
    public Car getCar(String model, Wheel[] wheels){
        Car car = new Car(this.engineFactory,model,wheels);
        return car;
    }
}
