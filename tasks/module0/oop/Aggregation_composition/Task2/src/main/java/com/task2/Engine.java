package com.task2;

public interface Engine {
    int getEnergy(int fuel);
    int run(int energy);
    boolean stop();
}
