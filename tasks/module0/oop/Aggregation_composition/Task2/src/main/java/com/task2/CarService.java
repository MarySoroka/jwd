package com.task2;

public interface CarService {
    void runCar();
    String outputCarModel();
    void stop();
    void refuel();
}
