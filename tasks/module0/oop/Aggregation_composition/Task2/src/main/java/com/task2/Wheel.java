package com.task2;

public interface Wheel {
    void changeWheel();
    void spin();
}
