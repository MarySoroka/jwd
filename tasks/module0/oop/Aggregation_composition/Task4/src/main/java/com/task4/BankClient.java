package com.task4;

import java.util.ArrayList;
import java.util.Comparator;

public class BankClient implements BankClientService{
    public BankClient() {
        this.bankAccounts = new ArrayList<BankAccount>();
    }

    public ArrayList<BankAccount> getBankAccounts() {
        return bankAccounts;
    }

    public void setBankAccounts(ArrayList<BankAccount> bankAccounts) {
        this.bankAccounts = bankAccounts;
    }

    private ArrayList<BankAccount> bankAccounts;

    @Override
    public long getAllAccountSum() {
        long sum = 0;
        for (BankAccount bankAccount:bankAccounts) {
            sum+= bankAccount.getMoneyAmount();
        }
        return sum;
    }

    @Override
    public BankAccount findByName(String data) {
        for (BankAccount bankAccount:bankAccounts) {
            if( bankAccount.getAccountId().equals(data)){
                return bankAccount;
            }
        }
        return null;
    }

    @Override
    public void sortByMoneyAmount() {
       Comparator<BankAccount> comparator = Comparator.comparingInt(BankAccount::getMoneyAmount);
       bankAccounts.sort(comparator);
    }

    @Override
    public void sortById() {
        Comparator<BankAccount> comparator = Comparator.comparing(BankAccount::getAccountId);
        bankAccounts.sort(comparator);
    }

    @Override
    public void sortByIsBlocked() {
        Comparator<BankAccount> comparator = Comparator.comparing(BankAccount::isBlocked);
        bankAccounts.sort(comparator);
    }

    @Override
    public long getAllAccountPositiveSum() {
        long sum = 0;
        for (BankAccount bankAccount:bankAccounts) {
            sum += Math.max(bankAccount.getMoneyAmount(), 0);
        }
        return sum;
    }

    @Override
    public long getAllAccountNegativeSum() {
        long sum = 0;
        for (BankAccount bankAccount:bankAccounts) {
            sum += Math.min(bankAccount.getMoneyAmount(), 0);
        }
        return sum;
    }

    @Override
    public void addBankAccount(int money, boolean isBlocked, String id) {
        this.bankAccounts.add(new BankAccount(money,isBlocked,id));
    }

    @Override
    public void showAll() {
        for (BankAccount b:this.bankAccounts) {
            System.out.println(b.getAccountId()+" "+b.getMoneyAmount()+" "+b.isBlocked());
        }
    }
}
