package com.task4;

public interface BankClientService {
    long getAllAccountSum();
    BankAccount findByName(String data);
    void sortByMoneyAmount();
    void sortById();
    void sortByIsBlocked();
    long getAllAccountPositiveSum();
    long getAllAccountNegativeSum();
    void addBankAccount(int money, boolean isBlocked, String id) ;
    void showAll();
}
