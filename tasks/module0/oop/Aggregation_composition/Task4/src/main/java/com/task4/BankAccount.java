package com.task4;

public class BankAccount {
    private int moneyAmount;
    private boolean isBlocked;

    public String getAccountId() {
        return accountId;
    }

    private final String accountId;
    public BankAccount(int moneyAmount, boolean isBlocked,String accountId) {
        this.moneyAmount = moneyAmount;
        this.isBlocked = isBlocked;
        this.accountId = accountId;
    }


    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public int getMoneyAmount() {
        return moneyAmount;
    }
}
