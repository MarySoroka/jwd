package com.task4;

public class App {
    public static void main(String[] args) {
        BankClient bankClient = new BankClient();
        bankClient.addBankAccount(100000,false,"1");
        bankClient.addBankAccount(-100000,true,"10");
        bankClient.addBankAccount(112,false,"3");
        bankClient.addBankAccount(-3242,true,"4");
        bankClient.addBankAccount(1000,false,"12");
        bankClient.addBankAccount(10,false,"6");
        bankClient.addBankAccount(-10330,false,"7");
        bankClient.showAll();
        System.out.println(bankClient.findByName("3").getAccountId());
        System.out.println(bankClient.getAllAccountSum());
        System.out.println(bankClient.getAllAccountNegativeSum());
        System.out.println(bankClient.getAllAccountPositiveSum());
        bankClient.sortById();
        bankClient.showAll();
        bankClient.sortByIsBlocked();
        bankClient.showAll();
        bankClient.sortByMoneyAmount();
        bankClient.showAll();
    }
}
