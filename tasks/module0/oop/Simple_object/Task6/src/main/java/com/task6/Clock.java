package com.task6;

public class Clock implements ClockService{
    private int seconds;
    private int minutes;
    private int hours;

    public Clock(int seconds, int minutes, int hours) {
        this.seconds = seconds;
        this.minutes = minutes;
        this.hours = hours;
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        if (hours <= 24) {
            this.hours = hours;
        } else {
            this.hours = 0;
        }
    }

    public int getMinutes() {
        return minutes;
    }

    public void setMinutes(int minutes) {
        if (minutes <= 60) {
            this.minutes = minutes;
        } else {
            this.minutes = 0;
        }
    }

    public int getSeconds() {
        return seconds;
    }

    public void setSeconds(int seconds) {
        if (seconds <= 60) {
            this.seconds = seconds;
        } else {
            this.seconds = 0;
        }
    }

    @Override
    public void setTime(int seconds, int minutes, int hours) {
         setHours(hours);
         setMinutes(minutes);
         setSeconds(seconds);
    }

    @Override
    public void changeTimeSeconds(int seconds) {
         int h = seconds%3600;
         setHours(h+getHours());
         int m =0;
         if((seconds-(h*3600))%60>0){
             m = (seconds-(h*3600))%60;
         };
         setMinutes(m+getMinutes());
         int s = 0;
         if((seconds-(h*24))-(m*60)>0){
             s= (seconds-(h*24))-(m*60);
         };
         setSeconds(s+getSeconds());
    }

    @Override
    public void changeTimeMinutes(int minutes) {
        int h = minutes%60;
        setHours(h+getHours());
        int m = 0;
        if((minutes-(h*60))>0){
            m = (minutes-(h*60));
        };
        setMinutes(m+getMinutes());
    }

    @Override
    public void changeTimeHours(int hours) {
        setHours(hours+getHours());
    }
}
