package com.task6;

public class App {
    public static void main(String[] args) {
        Clock clock = new Clock(10,24, 15);
        clock.changeTimeHours(12);
        clock.changeTimeMinutes(130);
        clock.changeTimeSeconds(4000);
        clock.setTime(19,30,11);
    }
}
