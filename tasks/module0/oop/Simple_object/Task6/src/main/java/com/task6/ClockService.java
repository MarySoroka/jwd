package com.task6;

public interface ClockService {
   void setTime(int seconds, int minutes, int hours);
   void changeTimeSeconds(int seconds);
    void changeTimeMinutes(int minutes);
    void changeTimeHours(int hours);

}
