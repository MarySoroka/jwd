package com.task5;
import java.util.Scanner;
public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Counter counter;
        if(scanner.next().equals("n")){
             counter = new Counter();
        }else{
             counter = new Counter(scanner.nextInt(),scanner.nextInt());
        }
        counter.subtract();
        counter.add();
        counter.currentState();
    }
}
