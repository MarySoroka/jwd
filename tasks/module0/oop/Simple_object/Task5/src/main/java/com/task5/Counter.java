package com.task5;

public class Counter  implements CounterService{
    private final int max = 100;
    private final int min = -100;

    public Counter(int maxValue, int minValue) {
        this.maxValue = maxValue;
        this.minValue = minValue;
    }

    public Counter() {
        this.maxValue = this.max;
        this.minValue = this.min;
    }

    private int maxValue;
    private int minValue;


    public int getMaxValue() {
        return maxValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }

    public int getMinValue() {
        return minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    @Override
    public void subtract() {
        if (this.minValue < min){
            this.minValue--;
        }
    }

    @Override
    public void add() {
        if (this.maxValue >max){
            this.maxValue++;
        }
    }

    @Override
    public String currentState() {
        return this.maxValue+" "+this.minValue;
    }
}
