package com.task5;

public interface CounterService {
    void subtract();
    void add();
    String currentState();

}
