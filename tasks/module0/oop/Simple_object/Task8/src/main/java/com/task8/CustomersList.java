package com.task8;

import java.util.ArrayList;
import java.util.Comparator;

public class CustomersList {
    public CustomersList() {
        initialize();
    }

    private ArrayList<Customer> customers;
    public void sortBySurname(){
        Comparator<Customer> comparator = Comparator.comparing(Customer::getSurname);
        customers.sort(comparator);
        for (Customer c:customers) {
                System.out.println(c.toString());
        }
    }
    public void showAllBankAccountInRange(int max, int min){
        for (Customer c:customers) {
            if (c.getCreditNumber()>=min&&c.getCreditNumber()<=max){
                System.out.println(c.toString());
            }
        }
    }
    public void initialize(){
        this.customers = new ArrayList<>();
        this.customers.add(new Customer(1,"a","b",
                "c","d",14,123));
        this.customers.add(new Customer(2,"a","a",
                "c","d",1234,123));
        this.customers.add(new Customer(3,"a","z",
                "c","d",134,123));
        this.customers.add(new Customer(4,"a","d",
                "c","d",184,123));
    }
}
