package com.task9;

public enum BindingType {
    HARD("hard"),
    SOFT("soft");

    private String bindingType;
    BindingType(String type) {
        this.bindingType = type;
    }
    BindingType getByName(String name){
        for (BindingType b:BindingType.values()) {
            if (b.name().equals(name)){
                return b;
            }
        }
        return null;
    }
}
