package com.task9;

public interface BookService {
    void initialize();
    void getByAuthor(String author);
    void getByPubHose(String pubHouse);
    void getOutAfterYear(int year);
}
