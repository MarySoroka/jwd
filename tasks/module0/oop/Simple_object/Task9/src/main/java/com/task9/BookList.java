package com.task9;

import java.util.ArrayList;
import java.util.Arrays;

public class BookList implements BookService{
    private ArrayList<Book> books;

    public BookList() {
        initialize();
    }

    @Override
    public void initialize() {
       this.books = new ArrayList<>();
       this.books.add(new Book(1,"book1",new String[]{"author1","author1_2"}, "pubHouse1",2001,300,100,BindingType.HARD));
        this.books.add(new Book(2,"book2",new String[]{"author2"}, "pubHouse2",1999,100,10,BindingType.HARD));
        this.books.add(new Book(3,"book3",new String[]{"author3","author3_2","author3_3"}, "pubHouse3",2010,30,166,BindingType.SOFT));
        this.books.add(new Book(4,"book4",new String[]{"author4"}, "pubHouse4",2011,50,100,BindingType.HARD));
        this.books.add(new Book(5,"book5",new String[]{"author5"}, "pubHouse5",2001,300,233,BindingType.SOFT));
    }

    @Override
    public void getByAuthor(String author) {
        int i = 0;
        for (Book b:books) {
            for (String a:b.getAuthors()){
            if (a.equals(author)){
                System.out.println(b.toString());
            }}
        }
    }

    @Override
    public void getByPubHose(String pubHouse) {
        for (Book b:books) {
            if (b.getPubHouse().equals(pubHouse)){
                System.out.println(b.toString());
            }
        }
    }

    @Override
    public void getOutAfterYear(int year) {
        for (Book b:books) {
            if (b.getYear()>year){
                System.out.println(b.toString());
            }
        }
    }
}
