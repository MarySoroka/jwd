package com.task9;

import java.util.ArrayList;
import java.util.Arrays;

public class Book {

    public int getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", authors=" + Arrays.toString(authors) +
                ", pubHouse='" + pubHouse + '\'' +
                ", year=" + year +
                ", pageAmount=" + pageAmount +
                ", cost=" + cost +
                ", bindingType=" + bindingType +
                '}';
    }

    public void setId(int id) {
        this.id = id;
    }

    public String[] getAuthors() {
        return authors;
    }

    public void setAuthors(String[] authors) {
        this.authors = authors;
    }

    public String getPubHouse() {
        return pubHouse;
    }

    public void setPubHouse(String pubHouse) {
        this.pubHouse = pubHouse;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    private int id;
    private String name;
    private String[] authors;
    private String pubHouse;
    private int year;
    private int pageAmount;

    public Book(int id, String name,String[] author, String pubHouse, int year, int pageAmount, int cost, BindingType bindingType) {
        this.id = id;
        this.name = name;
        this.authors = author;
        this.pubHouse = pubHouse;
        this.year = year;
        this.pageAmount = pageAmount;
        this.cost = cost;
        this.bindingType = bindingType;
    }

    private int cost;
    private BindingType bindingType;

}
