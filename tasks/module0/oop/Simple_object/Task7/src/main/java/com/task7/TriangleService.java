package com.task7;

public interface TriangleService {
    double space();
    double perimeter();
    double[] medianCrossing();
}
