package com.task7;

public class Triangle implements TriangleService{
    private int x1;
    private int y1;
    private int x2;
    private int y2;
    private double a;
    private double b;
    private double c;
    public Triangle(int x1, int y1, int x2, int y2, int x3, int y3) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.x3 = x3;
        this.y3 = y3;
        this.a = getLength(x1, y1, x2, y2);
        this.b = getLength(x2, y2, x3, y3);
        this.c = getLength(x1, y1, x3, y3);
    }

    private int x3;
    private int y3;


    public static double getLength(int x1, int y1, int x2, int y2){
        return Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2));
    }
    @Override
    public double space() {
        double p = perimeter();
        return Math.sqrt(p*(p-this.a)*(p-this.b)*(p-this.c));
    }

    @Override
    public double perimeter() {
        return this.a+this.b+this.c;
    }

    @Override
    public double[] medianCrossing() {
        double x = (double) (this.x1+this.x2+this.x3)/3;
        double y =(double) (this.y1+this.y2+this.y3)/3;
        return new double[]{x,y} ;
    }
}
