package com.task3;


import java.util.Scanner;


public class App {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Student[] students = new Student[10];
        int k = 0;
        while(k<10){
           System.out.println("enter name");
           String name = scanner.next();
           System.out.println("enter group's number");
           long group = scanner.nextLong();
           System.out.println("enter your marks");
           int[] marks = new int[5];
           boolean isGood = true;
           for (int i = 0; i<5; i++){
               marks[i] = scanner.nextInt();
               if (marks[i]<9){
                   isGood = false;
               }
           }
           if (isGood){
               Student student = new Student(name,group,marks);
               System.out.println("is good");
               students[k] = student;
               k++;
           }
        }

    }
}
