package com.task3;

public class Student {
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getNumOfGroup() {
        return numOfGroup;
    }

    public void setNumOfGroup(long numOfGroup) {
        this.numOfGroup = numOfGroup;
    }

    public int[] getAcademicPerformance() {
        return academicPerformance;
    }

    public void setAcademicPerformance(int[] academicPerformance) {
        this.academicPerformance = academicPerformance;
    }

    private long numOfGroup;

    public Student(String name, long numOfGroup, int[] academicPerformance) {
        this.name = name;
        this.numOfGroup = numOfGroup;
        this.academicPerformance = academicPerformance;
    }

    private int[] academicPerformance;

}
