package com.task10;

import javax.xml.crypto.Data;
import java.util.Arrays;
import java.util.Timer;

public class Airline {
    private AirplaneType airplaneType;
    private WeekdayType[] weekdayTypes;
    private String destination;

    public AirplaneType getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(AirplaneType airplaneType) {
        this.airplaneType = airplaneType;
    }

    public WeekdayType[] getWeekdayTypes() {
        return weekdayTypes;
    }

    public void setWeekdayTypes(WeekdayType[] weekdayTypes) {
        this.weekdayTypes = weekdayTypes;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getFlight() {
        return flight;
    }

    public void setFlight(int flight) {
        this.flight = flight;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public Airline(String destination, AirplaneType airplaneType, WeekdayType[] weekdayTypes, int flight, String data) {
        this.airplaneType = airplaneType;
        this.weekdayTypes = weekdayTypes;
        this.flight = flight;
        this.time = data;
        this.destination = destination;
    }

    private int flight;

    @Override
    public String toString() {
        return "Airline{" +
                "destination=" +destination+
                " airplaneType=" + airplaneType +
                ", weekdayTypes=" + Arrays.toString(weekdayTypes) +
                ", flight=" + flight +
                ", time=" + time +".00"+
                '}';
    }

    private String time;

}
