package com.task10;

public enum AirplaneType {
    JUMBO("jumbo"),
    MIDSIZE("midsize"),
    LIGHT("light");
    private String airplaneType;
    AirplaneType(String airplane) {
        this.airplaneType = airplane;
    }
    AirplaneType getByName(String airplaneType){
        for (AirplaneType a:
             AirplaneType.values()) {
            if (a.name().equals(airplaneType)){
                return a;
            }
        }
        return null;
    }
}
