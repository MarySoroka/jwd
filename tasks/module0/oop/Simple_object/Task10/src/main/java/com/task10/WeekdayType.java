package com.task10;

public enum WeekdayType {
    MON("mon"),
    TUE("tue"),
    WED("wed"),
    THU("thu"),
    FRI("fri"),
    ST("st"),
    SUN("sun");
    private String weekDay;
    WeekdayType(String weekDay) {
        this.weekDay = weekDay;
    }
    WeekdayType getByName(String day){
        for (WeekdayType w:
             WeekdayType.values()) {
            if (w.name().equals(day)){
                return w;
            }
        }
        return null;
    }
}
