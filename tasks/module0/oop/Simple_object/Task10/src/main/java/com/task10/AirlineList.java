package com.task10;

public class AirlineList implements AirlineService {
    private Airline[] airlines;

    public AirlineList() {
        initialize();
    }

    @Override
    public void initialize() {
        this.airlines = new Airline[5];
        for (int i = 0; i<5; i++) {
            this.airlines[i] = new Airline("Minsk"+i,AirplaneType.values()[i%3
                    ],
                    new WeekdayType[]{WeekdayType.SUN,WeekdayType.values()[i]},i,String.valueOf(i+2));
        }
    }

    @Override
    public void getByDestination(String destination) {
        for (Airline a:
             this.airlines) {
            if (a.getDestination().equals(destination)) {
                System.out.println(a.toString());
            }
        }
    }

    @Override
    public void getByWeekDay(WeekdayType weekdayType) {
        for (Airline a:
                this.airlines) {
            for (WeekdayType w:
                 a.getWeekdayTypes()) {
                if (w.name().equals(weekdayType.name())) {
                    System.out.println(a.toString());
                }
            }

        }
    }

    @Override
    public void getByAll(WeekdayType weekdayType, String time) {
        for (Airline a:
                this.airlines) {
            for (WeekdayType w:
                    a.getWeekdayTypes()) {
                if (w.name().equals(weekdayType.name()) && Double.parseDouble(a.getTime()) > Double.parseDouble(time)) {
                    System.out.println(a.toString());
                }
            }

        }
    }
}
