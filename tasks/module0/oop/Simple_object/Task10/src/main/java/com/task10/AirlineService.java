package com.task10;

public interface AirlineService {
    void initialize();
    void getByDestination(String destination);
    void getByWeekDay(WeekdayType weekdayType);
    void getByAll(WeekdayType weekdayType,String time);
}
