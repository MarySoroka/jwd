package com.task4;

public interface TrainService {
    void sortById();
    Train getByName(long id);
    void sortByDestination();
    void initialize();
}
