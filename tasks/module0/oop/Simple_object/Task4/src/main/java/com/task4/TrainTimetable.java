package com.task4;

import java.util.ArrayList;
import java.util.Comparator;

public class TrainTimetable implements TrainService {
    private ArrayList<Train> trains;

    public TrainTimetable() {
        this.trains = new ArrayList<>();
        initialize();
    }

    @Override
    public void sortById() {
        Comparator<Train> comparator = Comparator.comparing(Train::getId);
        trains.sort(comparator);
    }

    @Override
    public Train getByName(long id) {
        for (Train t: this.trains) {
            if (t.getId() == id){
                return t;
            }
        }
        return null;
    }

    @Override
    public void sortByDestination() {
        Comparator<Train> comparator = Comparator.comparing(Train::getDestination);
        trains.sort(comparator);
    }

    @Override
    public void initialize() {
        this.trains.add(new Train("Minsk", 123, 10));
        this.trains.add(new Train("Brest", 124, 18));
        this.trains.add(new Train("Gomel", 1, 12));
        this.trains.add(new Train("Mogilev", 133, 11));
        this.trains.add(new Train("Amsterdam", 3, 14));
    }

    public ArrayList<Train> getTrains() {
        return trains;
    }

    public void setTrains(ArrayList<Train> trains) {
        this.trains = trains;
    }
}
