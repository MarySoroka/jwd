package com.task4;

public class Train {
    public Train(String destination, long id, int time) {
        this.destination = destination;
        this.id = id;
        this.time = time;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public int getTime() {
        return time;
    }

    public void setTime(int time) {
        this.time = time;
    }

    private String destination;
    private long id;
    private int time;
}
