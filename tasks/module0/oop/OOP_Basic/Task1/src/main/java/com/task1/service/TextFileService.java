package service;

import service.SimpleFileService;

public interface TextFileService extends SimpleFileService {
    void editFile( String text);

}
