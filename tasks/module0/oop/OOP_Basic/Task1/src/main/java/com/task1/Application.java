import module.Directory;
import module.TextFile;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException {
        Directory directory = new Directory();
        TextFile textFile = directory.createFile("file","text");
        textFile.editFile("New text file");
        textFile.renameFile("file1");
        System.out.println( textFile.outputFile());
        textFile.deleteFile();

    }
}
