package service;

public interface SimpleFileService {

    void renameFile(String newName);
    String outputFile();
    String deleteFile();
}
