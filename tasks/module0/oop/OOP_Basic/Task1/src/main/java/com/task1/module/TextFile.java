package module;

import module.SimpleFile;
import service.TextFileService;

import java.io.IOException;

public class TextFile extends SimpleFile implements TextFileService {
    private static final String fileExtension = "txt";

    public TextFile(String fileName,String text) throws IOException {
        super(fileExtension,fileName, text);

    }
    @Override
    public void editFile( String text) {
       setText(getText()+text);
    }
}
