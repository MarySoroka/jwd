package service;

import module.SimpleFile;

import java.io.IOException;

public interface DirectoryService {
    SimpleFile createFile(String filePath, String text) throws IOException;

}
