package module;

import service.SimpleFileService;

import java.io.IOException;

public class SimpleFile implements SimpleFileService {
    private String fileName;
    final private String fileExtension;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    private String text;

    public SimpleFile(String fileExtension, String fileName, String text) throws IOException {
        this.text = text;
        this.fileName = fileName;
        this.fileExtension = fileExtension;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileExtension() {
        return fileExtension;
    }

    @Override
    public String deleteFile() {
        return "Deleted";
    }

    @Override
    public void renameFile( String newName) {
       setFileName(newName);
    }

    @Override
    public String outputFile() {
       return getText();
    }
}
