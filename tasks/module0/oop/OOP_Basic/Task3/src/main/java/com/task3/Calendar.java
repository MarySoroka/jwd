package com.task3;

import java.util.List;

import java.util.ArrayList;

public class Calendar {
    public Calendar(int year,int day) {
        init(year, day);
    }

    private  List<Day> days;
    public void getHolidays(){
        for (Day d:this.days) {
            if (d.isHoliday()){
                System.out.println(d.toString() +" "+Holiday.getByDayOnly(d.getDayOfMonth()).getName());
            }
        }
    }
    private void init(int year, int startDay) {
        this.days= new ArrayList<>();
        DayOfWeekType lastDay = DayOfWeekType.of(startDay);
        int dayOfWeek = lastDay.ordinal();
        int dayOfYear = 1;
        for (Month m : Month.values()) {
            for(int dayOfMonth = 1;dayOfMonth <= m.days(year);dayOfMonth++ ){
                this.days.add(new Day(DayOfWeekType.values()[dayOfWeek%7],dayOfMonth,dayOfYear,Holiday.getByDay(dayOfMonth,m)!=null));
                dayOfYear++;
                dayOfWeek++;
            }
        }

    }
    private class Day{
        @Override
        public String toString() {
            return "Day{" +
                    "dayOfWeekType=" + dayOfWeekType +
                    ", isHoliday=" + isHoliday +
                    ", dayOfMonth=" + dayOfMonth +
                    ", dayOfYear=" + dayOfYear +
                    '}';
        }

        public Day(DayOfWeekType dayOfWeekType, int dayOfMonth, int dayOfYear,boolean isHoliday) {
            this.dayOfWeekType = dayOfWeekType;
            this.dayOfMonth = dayOfMonth;
            this.dayOfYear = dayOfYear;
            this.isHoliday = isHoliday;
        }

        private DayOfWeekType dayOfWeekType;

        public boolean isHoliday() {
            return isHoliday;
        }

        public void setHoliday(boolean holiday) {
            isHoliday = holiday;
        }

        private boolean isHoliday;
        public DayOfWeekType getDayOfWeekType() {
            return dayOfWeekType;
        }

        public void setDayOfWeekType(DayOfWeekType dayOfWeekType) {
            this.dayOfWeekType = dayOfWeekType;
        }

        public int getDayOfMonth() {
            return dayOfMonth;
        }

        public void setDayOfMonth(int dayOfMonth) {
            this.dayOfMonth = dayOfMonth;
        }

        public int getDayOfYear() {
            return dayOfYear;
        }

        public void setDayOfYear(int dayOfYear) {
            this.dayOfYear = dayOfYear;
        }

        private int dayOfMonth;
        private int dayOfYear;
    }
}
