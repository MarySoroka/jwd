package com.task3;

public enum Month implements DaySupplier {
    JAN("January", 1) {
        public int days(int year) {
            return 31;
        }
    },
    FEB("February", 2) {
        public int days(int year) {
            return isVesocos(year) ? 29 : 28;
        }
    },
    MAR("March", 3) {
        public int days(int year) {
            return 31;
        }
    },
    APL("April", 4) {
        public int days(int year) {
            return 30;
        }
    },
    MAY("May", 5) {
        public int days(int year) {
            return 31;
        }
    },
    JUNE("June", 6) {
        public int days(int year) {
            return 30;
        }
    },
    JULY("July", 7) {
        public int days(int year) {
            return 31;
        }
    },
    AUG("August", 8) {
        public int days(int year) {
            return 31;
        }
    },
    SEP("September", 9) {
        public int days(int year) {
            return 30;
        }
    },
    OCT("October", 10) {
        public int days(int year) {
            return 31;
        }
    },
    NOV("November", 11) {
        public int days(int year) {
            return 30;
        }
    },
    DEC("December", 12) {
        public int days(int year) {
            return 31;
        }


    }
    ;
    @Override
    public boolean isVesocos(int year) {
        return (year % 4 == 0) && (year % 100 == 0) && (year % 400 == 0);
    }
    Month(String month, int ind) {
    }
}