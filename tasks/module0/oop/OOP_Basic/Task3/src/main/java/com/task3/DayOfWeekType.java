package com.task3;

public enum  DayOfWeekType {
    MON("monday"),
    TUE("tuesday"),
    WED("wednesday"),
    THU("thursday"),
    FRI("friday"),
    ST("saturday"),
    SUN("sunday");
    private String dayOfWeek;
    DayOfWeekType(String dayOfWeek) {

    }
    static DayOfWeekType of(int day){
        return DayOfWeekType.values()[day%7];
    }
}
