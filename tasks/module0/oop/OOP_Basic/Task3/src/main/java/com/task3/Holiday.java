package com.task3;

public enum Holiday {
    NEWYEAR("New year",Month.JAN,1),
    CRISTMAS("Cristmas",Month.JAN,7),
    WOMENSDAY("Women's day",Month.MAR,8),
    REVOLUTIONDAY("Revolution day",Month.NOV,7);

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    private String name;
    private Month month;
    private  int day;
    Holiday(String name, Month month, int day) {
        this.name = name;
        this.month = month;
        this.day = day;
    }
    public static Holiday getByDay(int day,Month month){
        for (Holiday h:Holiday.values()) {
            if (h.day == day&&h.month.equals(month)){
                return h;
            }
        }
        return null;
    }
    public static Holiday getByDayOnly(int day){
        for (Holiday h:Holiday.values()) {
            if (h.day == day){
                return h;
            }
        }
        return null;
    }
}
