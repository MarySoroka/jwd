package com.task3;

public enum DayType {
    SIMPLE,
    WEEKEND,
    HOLIDAY
}
