package com.task5.service;

import com.task5.module.FlowersComposition;
import com.task5.module.Wrapper;
import com.task5.module.WrapperColor;

import java.util.ArrayList;

public class WrapperBuilderService implements WrapperService {
    private ArrayList<Wrapper> wrappers;

    public WrapperBuilderService() {
        this.wrappers = initialize();
    }

    @Override
    public Wrapper getByName(String name) {
        for (Wrapper wrap:wrappers) {
            if(wrap.getColor().equals(WrapperColor.getName(name))){
                return wrap;
            }
        }
        return null;
    }
    @Override
    public ArrayList<Wrapper> initialize() {
        WrapperColor[] wrapperColors = WrapperColor.values();
        ArrayList<Wrapper> wrappers = new ArrayList<>();
        int k = 20;
        for (WrapperColor wrap: wrapperColors) {
            wrappers.add(new Wrapper(wrap,k));
            k+=15;
        }
        return wrappers;
    }

    @Override
    public void showAll() {
        for (Wrapper wrap:wrappers) {
            System.out.println(wrap.getColor()+" "+wrap.getWrapperPrice());
        }
    }

    @Override
    public void chooseWrapper(String wrapper, FlowersComposition flowersComposition) {
        Wrapper wrap = getByName(wrapper);
        flowersComposition.setWrapper(getByName(wrapper));
    }
}
