package com.task5.module;

public enum WrapperColor {
    BLUE("blue"),
    RED("red"),
    PINK("pink"),
    GREEN("green"),
    YELLOW("yellow");
    private String color;
    WrapperColor(String color) {
        this.color = color;
    }
    public static WrapperColor getName(String color){
        final WrapperColor[] values = WrapperColor.values();
        for (WrapperColor name: values) {
            if(name.color.toUpperCase().equals(color)){
                return name;
            }
        }
        return null;
    }
}
