package com.task5;

import com.task5.controller.*;
import com.task5.module.*;
import com.task5.service.*;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        FlowerService flowerService = new FlowerBuilderService();
        WrapperService wrapperService = new WrapperBuilderService();
        FlowersList flowersList = new FlowersList();
        flowersList.initialize();
        FlowersComposition flowersComposition = new FlowersComposition(new FlowersList());
        FlowersCompositionService flowersCompositionService = new CompositionBuilderService();
        AppCommand addFlowers = new AddFlowersCommand(flowersCompositionService,flowersList,flowerService,flowersComposition);
        AppCommand changeWrapper = new ChangeWrapperCommand(flowersComposition,wrapperService);
        AppCommand showComposition = new ShowCompositionCommand(flowersCompositionService,flowersComposition);
        Map<AppCommandName,AppCommand> commandMap = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commandMap.put(AppCommandName.ADDFLOWER,addFlowers);
        commandMap.put(AppCommandName.CHANGECOLOR,changeWrapper);
        commandMap.put(AppCommandName.SHOWCOMPOSITION,showComposition);
        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commandMap);
        AppController appController = new AppController(commandFactory);
        boolean isRinning = true;
        while(isRinning){
            System.out.println("Enter your command");
            System.out.println("-af");
            System.out.println("-cc");
            System.out.println("-sc");
            System.out.println("-e");
            String commandUser = scan.nextLine();
            if (commandUser.equals("-e")){
                break;
            }
            appController.executeCommand(commandUser);
        }
    }
}
