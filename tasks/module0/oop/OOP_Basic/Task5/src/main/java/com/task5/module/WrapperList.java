package com.task5.module;

import com.task5.service.WrapperBuilderService;
import com.task5.service.WrapperService;

import java.util.ArrayList;
import java.util.Objects;

public class WrapperList extends WrapperBuilderService {
    public WrapperList() {
        this.wrappers = initialize();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        WrapperList that = (WrapperList) o;
        return Objects.equals(wrappers, that.wrappers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(wrappers);
    }

    private ArrayList<Wrapper> wrappers;

}
