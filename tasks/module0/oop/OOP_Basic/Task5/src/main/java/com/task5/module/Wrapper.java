package com.task5.module;

import com.task5.service.WrapperService;

import java.util.ArrayList;


public class Wrapper{
    public WrapperColor getColor() {
        return color;
    }

    public void setColor(WrapperColor color) {
        this.color = color;
    }

    WrapperColor color;

    public Wrapper(WrapperColor color, int wrapperPrice) {
        this.color = color;
        this.wrapperPrice = wrapperPrice;
    }

    public int getWrapperPrice() {
        return wrapperPrice;
    }

    public void setWrapperPrice(int wrapperPrice) {
        this.wrapperPrice = wrapperPrice;
    }

    int wrapperPrice;

}
