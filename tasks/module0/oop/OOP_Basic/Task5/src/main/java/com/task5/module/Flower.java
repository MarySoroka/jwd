package com.task5.module;

import com.task5.service.FlowerService;

import java.util.ArrayList;

public class Flower {
    public Flower(FlowersName flowersName, int flowerPrice) {
        this.flowersName = flowersName;
        this.flowerPrice = flowerPrice;
    }

    public FlowersName getFlowersName() {
        return flowersName;
    }

    public void setFlowersName(FlowersName flowersName) {
        this.flowersName = flowersName;
    }

    public int getFlowerPrice() {
        return flowerPrice;
    }

    public void setFlowerPrice(int flowerPrice) {
        this.flowerPrice = flowerPrice;
    }

    FlowersName flowersName;
    int flowerPrice;

}
