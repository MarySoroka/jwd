package com.task5.service;

import com.task5.module.FlowersComposition;
import com.task5.module.Wrapper;

import java.util.ArrayList;

public interface WrapperService {
    Wrapper getByName(String name);
    ArrayList<Wrapper> initialize();
    void showAll();
    void chooseWrapper(String wrapper, FlowersComposition flowersComposition);
}
