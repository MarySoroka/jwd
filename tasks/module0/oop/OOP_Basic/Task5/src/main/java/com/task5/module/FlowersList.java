package com.task5.module;

import com.task5.service.FlowerBuilderService;
import com.task5.service.FlowerService;

import java.util.ArrayList;
import java.util.Objects;

public class FlowersList extends FlowerBuilderService {
    public FlowersList() {
        this.flowers = new ArrayList<Flower>();
    }

    private ArrayList<Flower> flowers;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FlowersList that = (FlowersList) o;
        return Objects.equals(flowers, that.flowers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(flowers);
    }

    public ArrayList<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(ArrayList<Flower> flowers) {
        this.flowers = flowers;
    }

}
