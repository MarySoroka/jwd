package com.task5.service;

import com.task5.module.*;

import java.util.ArrayList;

public interface FlowersCompositionService {
    void showComposition( FlowersComposition flowersComposition);
    void addFlowers(String flowerName, FlowersComposition flowersComposition);
    ArrayList<Flower> getFlowersList();
    }
