package com.task5.module;

import com.task5.controller.AppCommandName;

public enum FlowersName {
    ROSE("rose"),
    TUPLE("tuple"),
    PEONY("peony"),
    CHAMOMILE("chamomile"),
    CHRYSANTHEMUM("chrysanthemum");
    private String flower;
    FlowersName(String flower) {
        this.flower = flower;
    }
    public static FlowersName getName(String flower){
        final FlowersName[] values = FlowersName.values();
        for (FlowersName name: values) {
            if(name.flower.toUpperCase().equals(flower)){
                return name;
            }
        }
        return null;
    }
}
