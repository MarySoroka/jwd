package com.task5.controller;

public interface AppCommandFactory {
    AppCommand getCommand(String command);
}
