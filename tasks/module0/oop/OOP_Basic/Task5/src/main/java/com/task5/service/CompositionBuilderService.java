package com.task5.service;

import com.task5.module.*;

import java.util.ArrayList;

public class CompositionBuilderService implements FlowersCompositionService{
    public Wrapper getWrapper() {
        return wrapper;
    }

    public void setWrapper(Wrapper wrapper) {
        this.wrapper = wrapper;
    }

    Wrapper wrapper;

    public CompositionBuilderService() {
        this.flowers = new FlowersList();
    }

    public FlowersList getFlowers() {
        return flowers;
    }

    public void setFlowers(FlowersList flowers) {
        this.flowers = flowers;
    }

    FlowersList flowers;
    @Override
    public void showComposition( FlowersComposition flowersComposition) {
        if (flowersComposition.getWrapper() != null && flowersComposition.getFlowers() != null) {
            ArrayList<Flower> flower = flowersComposition.getFlowersList();
            for (Flower flow : flower) {
                System.out.println(flow.getFlowersName() + " " + flow.getFlowerPrice());
            }
            System.out.println(flowersComposition.getWrapper().getColor() + " " + flowersComposition.getWrapper().getWrapperPrice());
        }else {
            System.out.println("Enter all parameter before payment");
        }
    }


    @Override
    public void addFlowers(String flowerName, FlowersComposition flowersComposition) {
        ArrayList<Flower> flower = flowersComposition.getFlowersList();
        FlowerBuilderService flowerBuilderService = new FlowerBuilderService();
        Flower flow =flowerBuilderService.getByName(flowerName);
        flower.add(flow);
        flowers.setFlowers(flower);
        flowersComposition.setFlowers(flowers);
    }

    @Override
    public ArrayList<Flower> getFlowersList() {
        return this.flowers.getFlowers();
    }
}
