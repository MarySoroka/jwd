package com.task5.controller;

public enum AppCommandName {
    ADDFLOWER("-af"),
    CHANGECOLOR("-cc"),
    SHOWCOMPOSITION("-sc"),
    EXIT("-e");
    private final String shortCommand;
    AppCommandName(String s) {
        this.shortCommand = s;
    }
    public static AppCommandName getName(String command){
        final AppCommandName[] values = AppCommandName.values();
        for (AppCommandName name: values) {
            if(name.shortCommand.equals(command)){
                return name;
            }
        }
        return null;
    }
}
