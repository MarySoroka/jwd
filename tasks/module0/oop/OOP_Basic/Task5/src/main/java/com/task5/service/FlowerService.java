package com.task5.service;

import com.task5.module.Flower;

import java.util.ArrayList;

public interface FlowerService {
    Flower getByName(String name);
    void showAll();
    ArrayList<Flower> initialize();

}
