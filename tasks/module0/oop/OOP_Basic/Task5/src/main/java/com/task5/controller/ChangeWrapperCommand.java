package com.task5.controller;
import com.task5.module.FlowersComposition;
import com.task5.module.WrapperColor;
import com.task5.service.WrapperService;

import java.util.Scanner;

public class ChangeWrapperCommand implements AppCommand {

    public ChangeWrapperCommand(FlowersComposition flowersComposition, WrapperService wrapperService) {
        this.flowersComposition = flowersComposition;
        this.wrapperService = wrapperService;
    }

    private FlowersComposition flowersComposition;
    private WrapperService wrapperService;


    @Override
    public void execute(String userData) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the color:");
        this.wrapperService.showAll();
        String data = scan.nextLine();
        if (Validation.isValid(WrapperColor.getName(data))){
            this.wrapperService.chooseWrapper(data,this.flowersComposition);
        }else{
            System.out.println("Name is incorrect");
        }
    }
}
