package com.task5.controller;

public class AppController {

    private final AppCommandFactory appCommandFactory;

    public AppController(AppCommandFactory appCommandFactory) {
        this.appCommandFactory = appCommandFactory;
    }
    public void executeCommand(String data){
        final AppCommand command = appCommandFactory.getCommand(data);
        command.execute(data);
    }
}
