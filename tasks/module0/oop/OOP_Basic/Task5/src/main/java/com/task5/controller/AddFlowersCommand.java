package com.task5.controller;

import com.task5.module.FlowersComposition;
import com.task5.module.FlowersList;
import com.task5.module.FlowersName;
import com.task5.service.FlowerService;
import com.task5.service.FlowersCompositionService;

import java.util.Scanner;

public class AddFlowersCommand implements  AppCommand {

    private FlowersCompositionService flowersCompositionService;
    private FlowersList flowersList;
    private FlowersComposition flowersComposition;

    public AddFlowersCommand(FlowersCompositionService flowersCompositionService, FlowersList flowersList, FlowerService flowerService, FlowersComposition flowersComposition) {
        this.flowersCompositionService = flowersCompositionService;
        this.flowersList = flowersList;
        this.flowerService = flowerService;
        this.flowersComposition = flowersComposition;

    }

    private FlowerService flowerService;
    @Override
    public void execute(String userData) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Choose flower:");
        this.flowerService.showAll();
        String data = scan.nextLine();
        if(Validation.isValid(FlowersName.getName(data))){
            this.flowersCompositionService.addFlowers(data,this.flowersComposition);
        }else {
            System.out.println("Name is incorrect");
        }
    }
}
