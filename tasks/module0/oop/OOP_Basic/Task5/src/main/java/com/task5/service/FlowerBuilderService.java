package com.task5.service;

import com.task5.module.Flower;
import com.task5.module.FlowersName;

import java.util.ArrayList;

public class FlowerBuilderService implements FlowerService {

    private ArrayList<Flower> flowers;

    public FlowerBuilderService() {
        this.flowers = initialize();
    }

    @Override
    public Flower getByName(String name) {

        for (Flower flow:flowers) {
            if(flow.getFlowersName().equals(FlowersName.getName(name))){
                return flow;
            }
        }
        return null;
    }

    @Override
    public void showAll() {
        for (Flower flow:flowers) {
            System.out.println(flow.getFlowersName()+" "+flow.getFlowerPrice());
        }
    }

    @Override
    public ArrayList<Flower> initialize() {
        FlowersName[] flowersName = FlowersName.values();
        ArrayList<Flower> flowers = new ArrayList<>();
        int k = 100;
        for (FlowersName flow: flowersName) {
            flowers.add(new Flower(flow,k));
            k+=30;
        }
        return flowers;
    }
}
