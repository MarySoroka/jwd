package com.task5.controller;

import com.task5.module.FlowersComposition;
import com.task5.service.FlowersCompositionService;

public class ShowCompositionCommand implements AppCommand {
    private FlowersCompositionService flowersCompositionService;
    private FlowersComposition flowersComposition;
    public ShowCompositionCommand(FlowersCompositionService flowersCompositionService,FlowersComposition flowersComposition) {
        this.flowersCompositionService = flowersCompositionService;
        this.flowersComposition = flowersComposition;
    }

    @Override
    public void execute(String userData) {
        flowersCompositionService.showComposition(this.flowersComposition);
    }
}
