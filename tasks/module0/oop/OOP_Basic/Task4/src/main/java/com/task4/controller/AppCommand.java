package com.task4.controller;

public interface AppCommand {
    void execute(String userData);
}
