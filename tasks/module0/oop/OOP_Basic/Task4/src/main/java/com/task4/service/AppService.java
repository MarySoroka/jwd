package com.task4.service;

import com.task4.module.Treasure;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Scanner;

public class AppService implements CaveService{
    private TreasureService treasureService;

    public AppService() {
        this.treasures = new ArrayList<>();
    }

    public ArrayList<Treasure> getTreasures() {
        return treasures;
    }

    public void setTreasures(ArrayList<Treasure> treasures) {
        this.treasures = treasures;
    }

    private ArrayList<Treasure> treasures;


    @Override
    public void showMostExpensive() {
        int maxInd = 0;
        for (int i = 1; i<100; i++){
            if(this.treasures.get(maxInd).getPrice()<this.treasures.get(i).getPrice()){
                maxInd = i;
            }
        }
        System.out.println( this.treasures.get(maxInd).getName()+" "+this.treasures.get(maxInd).getPrice());
    }

    @Override
    public void showTreasureOfThisCost(long cost) {
        ArrayList<Treasure> treasures1 = new ArrayList<>();
        for(int i = 0; i<100; i++){
            if(this.treasures.get(i).getPrice()<= cost){
                treasures1.add(treasures.get(i));
            }
        }
        for (Treasure tr:treasures1) {
            System.out.println(tr.getName()+" "+tr.getPrice());
        }

    }

    @Override
    public void showAll() {
        for (Treasure tr:treasures) {
            System.out.println(tr.getName()+" "+tr.getPrice());
        }
    }

    @Override
    public void initializeTreasure( String filename)  {
        File file = new File(filename);
        try {
            Scanner scan = new Scanner(file);
                while (scan.hasNextLine()){
                    this.treasures.add(new Treasure(scan.next(),scan.nextLong()));
                scan.nextLine();}

        } catch (FileNotFoundException e) {
             System.out.println(e.getMessage());;
        }


    }
}
