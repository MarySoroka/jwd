package com.task4.service;

import com.task4.module.Treasure;

public interface TreasureService {
   Treasure initializeTreasure(String name, long price);
}
