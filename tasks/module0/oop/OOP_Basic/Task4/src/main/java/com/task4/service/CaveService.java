package com.task4.service;

import com.task4.module.Treasure;

import java.io.FileNotFoundException;
import java.util.ArrayList;

public interface CaveService {
    void showMostExpensive();
    void showTreasureOfThisCost(long cost);
    void showAll();
    void initializeTreasure( String filename) throws FileNotFoundException;
}
