package com.task4.controller;


import com.task4.service.CaveService;

import java.util.Scanner;

public class DisplaySpecificCostTreasureCommand implements AppCommand{
    public DisplaySpecificCostTreasureCommand(CaveService caveService) {
        this.caveService = caveService;
    }

    private CaveService caveService;

    public long getCost() {
        return cost;
    }

    public void setCost(long cost) {
        this.cost = cost;
    }

    private long cost;

    @Override
    public void execute(String userData) {
        System.out.println("Please, enter cost:");
        Scanner scan = new Scanner(System.in);
        this.cost = scan.nextLong();
        caveService.showTreasureOfThisCost(this.cost);
    }
}
