package com.task4.controller;

import com.task4.service.CaveService;

public class DisplayAllTreasureCommand implements AppCommand{
    private CaveService caveService;
    public DisplayAllTreasureCommand(CaveService caveService){
        this.caveService = caveService;
    }

    @Override
    public void execute(String userData) {
         caveService.showAll();
    }
}
