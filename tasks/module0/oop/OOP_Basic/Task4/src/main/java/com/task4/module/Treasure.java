package com.task4.module;

import com.task4.service.TreasureService;

public class Treasure implements TreasureService {
    public Treasure(String name, long price) {
        this.name = name;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPrice() {
        return price;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    String name;
    long price;

    @Override
    public Treasure initializeTreasure(String name, long price) {
        return new Treasure(name, price);
    }
}
