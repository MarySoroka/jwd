package com.task4.controller;

import java.util.Map;

public class SimpleAppComandFactory implements AppCommandFactory {
    private final Map<AppCommandName,AppCommand> commandMap;

    public SimpleAppComandFactory(Map<AppCommandName, AppCommand> commandMap) {
        this.commandMap = commandMap;
    }

    @Override
    public AppCommand getCommand(String command) {
        final AppCommandName appCommandName= AppCommandName.getName(command);
        final AppCommand command1 = commandMap.getOrDefault(appCommandName,userData-> System.out.println("There is no such command.Try again."));
        return command1;
    }
}
