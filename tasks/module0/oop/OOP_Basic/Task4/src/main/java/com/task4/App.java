package com.task4;

import com.task4.controller.*;
import com.task4.service.AppService;
import com.task4.service.CaveService;
import com.task4.service.TreasureService;

import java.io.FileNotFoundException;
import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class App {
    public static void main(String[] args) throws FileNotFoundException {
        Scanner scan = new Scanner(System.in);
        CaveService caveService = new AppService();
        caveService.initializeTreasure("/home/maria/Desktop/courses/jwd/tasks/module0/oop/OOP_Basic/Task4/target/classes/com/task4/treasures.txt");
        AppCommand displayAll = new DisplayAllTreasureCommand(caveService);
        AppCommand displaySpecific = new DisplaySpecificCostTreasureCommand(caveService);
        AppCommand displayExp = new DisplayExpensiveTreasureCommand(caveService);
        Map<AppCommandName,AppCommand> commandMap = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commandMap.put(AppCommandName.SHOWALL,displayAll);
        commandMap.put(AppCommandName.SHOWEXP,displayExp);
        commandMap.put(AppCommandName.SHOWCOST, displaySpecific);
        AppCommandFactory commandFactory = new SimpleAppComandFactory(commandMap);
        AppController appController = new AppController(commandFactory);
        boolean isRinning = true;
        while(isRinning){
            System.out.println("Enter your command");
            System.out.println("-s");
            System.out.println("-se");
            System.out.println("-sc");
            System.out.println("-e");
            String commandUser = scan.nextLine();
            if (commandUser.equals("-e")){
                break;
            }
            appController.executeCommand(commandUser);
        }
    }
}
