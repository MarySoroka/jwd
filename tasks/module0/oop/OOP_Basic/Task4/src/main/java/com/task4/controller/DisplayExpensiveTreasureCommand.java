package com.task4.controller;

import com.task4.service.CaveService;

public class DisplayExpensiveTreasureCommand implements AppCommand{
    public DisplayExpensiveTreasureCommand(CaveService caveService) {
        this.caveService = caveService;
    }

    private CaveService caveService;


    @Override
    public void execute(String userData) {
        caveService.showMostExpensive();

    }
}
