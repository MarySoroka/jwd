package com.task4.controller;

public interface AppCommandFactory {
    AppCommand getCommand(String command);
}
