package com.task6.module;

public enum BoxColor {
    BLUE("blue"),
    RED("red"),
    PINK("pink"),
    GREEN("green"),
    YELLOW("yellow");
    private String color;
    BoxColor(String color) {
        this.color = color;
    }
    public static BoxColor getName(String color){
        final BoxColor[] values = BoxColor.values();
        for (BoxColor name: values) {
            if(name.color.toUpperCase().equals(color)){
                return name;
            }
        }
        return null;
    }
}
