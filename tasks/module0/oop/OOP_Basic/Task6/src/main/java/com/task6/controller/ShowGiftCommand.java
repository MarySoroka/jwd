package com.task6.controller;

import com.task6.module.Gift;
import com.task6.service.GiftService;

public class ShowGiftCommand implements AppCommand {
    private GiftService giftService;
    private Gift gift;
    public ShowGiftCommand(GiftService giftService, Gift gift) {
        this.giftService = giftService;
        this.gift = gift;
    }

    @Override
    public void execute(String userData) {
        giftService.showComposition(this.gift);
    }
}
