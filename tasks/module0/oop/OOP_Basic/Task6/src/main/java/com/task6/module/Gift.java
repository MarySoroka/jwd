package com.task6.module;

import com.task6.service.GiftBuilderService;

public class Gift extends GiftBuilderService {

   SweetsList flowers;

    public Gift(SweetsList flowers) {
        super();
    }

    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    Box box;

    @Override
    public void showComposition(Gift gift) {

    }

    @Override
    public void addFlowers(String flowerName, Gift gift) {

    }
}
