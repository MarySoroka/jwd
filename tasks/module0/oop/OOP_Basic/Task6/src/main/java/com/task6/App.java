package com.task6;

import com.task6.controller.*;
import com.task6.module.Gift;
import com.task6.module.SweetsList;
import com.task6.service.*;

import java.util.EnumMap;
import java.util.Map;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        SweetService sweetService = new SweetsBuilderService();
        BoxService boxService = new BoxBuilderService();
        SweetsList sweetsList = new SweetsList();
        sweetsList.initialize();
        Gift gift = new Gift(new SweetsList());
        GiftService giftService = new GiftBuilderService();
        AppCommand addFlowers = new AddSweetCommand(giftService, sweetsList, sweetService, gift);
        AppCommand changeWrapper = new ChangeBoxCommand(gift, boxService);
        AppCommand showComposition = new ShowGiftCommand(giftService, gift);
        Map<AppCommandName,AppCommand> commandMap = new EnumMap<AppCommandName, AppCommand>(AppCommandName.class);
        commandMap.put(AppCommandName.ADDFLOWER,addFlowers);
        commandMap.put(AppCommandName.CHANGECOLOR,changeWrapper);
        commandMap.put(AppCommandName.SHOWCOMPOSITION,showComposition);
        AppCommandFactory commandFactory = new SimpleAppCommandFactory(commandMap);
        AppController appController = new AppController(commandFactory);
        boolean isRinning = true;
        while(isRinning){
            System.out.println("Enter your command");
            System.out.println("-as");
            System.out.println("-cc");
            System.out.println("-sg");
            System.out.println("-e");
            String commandUser = scan.nextLine();
            if (commandUser.equals("-e")){
                break;
            }
            appController.executeCommand(commandUser);
        }
    }
}
