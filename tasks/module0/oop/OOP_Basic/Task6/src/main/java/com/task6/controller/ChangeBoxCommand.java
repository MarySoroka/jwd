package com.task6.controller;
import com.task6.module.Gift;
import com.task6.module.BoxColor;
import com.task6.service.BoxService;

import java.util.Scanner;

public class ChangeBoxCommand implements AppCommand {

    public ChangeBoxCommand(Gift gift, BoxService boxService) {
        this.gift = gift;
        this.boxService = boxService;
    }

    private Gift gift;
    private BoxService boxService;


    @Override
    public void execute(String userData) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Enter the color:");
        this.boxService.showAll();
        String data = scan.nextLine();
        if (Validation.isValid(BoxColor.getName(data))){
            this.boxService.chooseWrapper(data,this.gift);
        }else{
            System.out.println("Name is incorrect");
        }
    }
}
