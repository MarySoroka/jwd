package com.task6.module;

public class Sweets {
    public Sweets(SweetName sweetName, int flowerPrice) {
        this.sweetName = sweetName;
        this.flowerPrice = flowerPrice;
    }

    public SweetName getSweetName() {
        return sweetName;
    }

    public void setSweetName(SweetName sweetName) {
        this.sweetName = sweetName;
    }

    public int getFlowerPrice() {
        return flowerPrice;
    }

    public void setFlowerPrice(int flowerPrice) {
        this.flowerPrice = flowerPrice;
    }

    SweetName sweetName;
    int flowerPrice;

}
