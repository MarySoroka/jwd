package com.task6.service;

import com.task6.module.Sweets;
import com.task6.module.Gift;

import java.util.ArrayList;

public interface GiftService {
    void showComposition(Gift gift);
    void addFlowers(String flowerName, Gift gift);
    ArrayList<Sweets> getFlowersList();
    }
