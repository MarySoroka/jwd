package com.task6.controller;

import com.task6.module.Gift;
import com.task6.module.SweetsList;
import com.task6.module.SweetName;
import com.task6.service.SweetService;
import com.task6.service.GiftService;

import java.util.Scanner;

public class AddSweetCommand implements  AppCommand {

    private GiftService giftService;
    private SweetsList sweetsList;
    private Gift gift;

    public AddSweetCommand(GiftService giftService, SweetsList sweetsList, SweetService sweetService, Gift gift) {
        this.giftService = giftService;
        this.sweetsList = sweetsList;
        this.sweetService = sweetService;
        this.gift = gift;

    }

    private SweetService sweetService;
    @Override
    public void execute(String userData) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Choose flower:");
        this.sweetService.showAll();
        String data = scan.nextLine();
        if(Validation.isValid(SweetName.getName(data))){
            this.giftService.addFlowers(data,this.gift);
        }else {
            System.out.println("Name is incorrect");
        }
    }
}
