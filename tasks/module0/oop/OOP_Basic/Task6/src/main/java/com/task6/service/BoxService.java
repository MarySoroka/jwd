package com.task6.service;

import com.task6.module.Gift;
import com.task6.module.Box;

import java.util.ArrayList;

public interface BoxService {
    Box getByName(String name);
    ArrayList<Box> initialize();
    void showAll();
    void chooseWrapper(String wrapper, Gift gift);
}
