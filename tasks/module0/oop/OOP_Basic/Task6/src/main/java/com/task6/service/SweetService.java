package com.task6.service;

import com.task6.module.Sweets;

import java.util.ArrayList;

public interface SweetService {
    Sweets getByName(String name);
    void showAll();
    ArrayList<Sweets> initialize();

}
