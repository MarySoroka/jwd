package com.task6.service;

import com.task6.module.Sweets;
import com.task6.module.Gift;
import com.task6.module.SweetsList;
import com.task6.module.Box;

import java.util.ArrayList;

public class GiftBuilderService implements GiftService {
    public Box getBox() {
        return box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

    Box box;

    public GiftBuilderService() {
        this.flowers = new SweetsList();
    }

    public SweetsList getFlowers() {
        return flowers;
    }

    public void setFlowers(SweetsList flowers) {
        this.flowers = flowers;
    }

    SweetsList flowers;
    @Override
    public void showComposition( Gift gift) {
        if (gift.getBox() != null && gift.getFlowers() != null) {
            ArrayList<Sweets> sweets = gift.getFlowersList();
            for (Sweets flow : sweets) {
                System.out.println(flow.getSweetName() + " " + flow.getFlowerPrice());
            }
            System.out.println(gift.getBox().getColor() + " " + gift.getBox().getWrapperPrice());
        }else {
            System.out.println("Enter all parameter before payment");
        }
    }


    @Override
    public void addFlowers(String flowerName, Gift gift) {
        ArrayList<Sweets> sweets = gift.getFlowersList();
        SweetsBuilderService sweetsBuilderService = new SweetsBuilderService();
        Sweets flow = sweetsBuilderService.getByName(flowerName);
        sweets.add(flow);
        flowers.setSweets(sweets);
        gift.setFlowers(flowers);
    }

    @Override
    public ArrayList<Sweets> getFlowersList() {
        return this.flowers.getSweets();
    }
}
