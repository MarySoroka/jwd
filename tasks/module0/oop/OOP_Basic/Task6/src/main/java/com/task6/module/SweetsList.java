package com.task6.module;

import com.task6.service.SweetsBuilderService;

import java.util.ArrayList;
import java.util.Objects;

public class SweetsList extends SweetsBuilderService {
    public SweetsList() {
        this.sweets = new ArrayList<Sweets>();
    }

    private ArrayList<Sweets> sweets;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SweetsList that = (SweetsList) o;
        return Objects.equals(sweets, that.sweets);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sweets);
    }

    public ArrayList<Sweets> getSweets() {
        return sweets;
    }

    public void setSweets(ArrayList<Sweets> sweets) {
        this.sweets = sweets;
    }

}
