package com.task6.service;

import com.task6.module.Sweets;
import com.task6.module.SweetName;

import java.util.ArrayList;

public class SweetsBuilderService implements SweetService {

    private ArrayList<Sweets> sweets;

    public SweetsBuilderService() {
        this.sweets = initialize();
    }

    @Override
    public Sweets getByName(String name) {

        for (Sweets flow: sweets) {
            if(flow.getSweetName().equals(SweetName.getName(name))){
                return flow;
            }
        }
        return null;
    }

    @Override
    public void showAll() {
        for (Sweets flow: sweets) {
            System.out.println(flow.getSweetName()+" "+flow.getFlowerPrice());
        }
    }

    @Override
    public ArrayList<Sweets> initialize() {
        SweetName[] sweetName = SweetName.values();
        ArrayList<Sweets> sweets = new ArrayList<>();
        int k = 100;
        for (SweetName flow: sweetName) {
            sweets.add(new Sweets(flow,k));
            k+=30;
        }
        return sweets;
    }
}
