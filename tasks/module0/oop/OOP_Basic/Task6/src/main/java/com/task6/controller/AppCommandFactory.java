package com.task6.controller;

public interface AppCommandFactory {
    AppCommand getCommand(String command);
}
