package com.task6.module;

public class Box {
    public BoxColor getColor() {
        return color;
    }

    public void setColor(BoxColor color) {
        this.color = color;
    }

    BoxColor color;

    public Box(BoxColor color, int wrapperPrice) {
        this.color = color;
        this.wrapperPrice = wrapperPrice;
    }

    public int getWrapperPrice() {
        return wrapperPrice;
    }

    public void setWrapperPrice(int wrapperPrice) {
        this.wrapperPrice = wrapperPrice;
    }

    int wrapperPrice;

}
