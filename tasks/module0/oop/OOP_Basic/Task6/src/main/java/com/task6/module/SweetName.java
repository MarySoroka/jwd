package com.task6.module;

public enum SweetName {
    CARAMEL("caramel"),
    CHOCOLATE("chocolate"),
    GELATIN("gelatin");
    private String flower;
    SweetName(String flower) {
        this.flower = flower;
    }
    public static SweetName getName(String flower){
        final SweetName[] values = SweetName.values();
        for (SweetName name: values) {
            if(name.flower.toUpperCase().equals(flower)){
                return name;
            }
        }
        return null;
    }
}
