package com.task6.controller;

public interface AppCommand {
    void execute(String userData);
}
