package com.task2;

public class App {
    public static void main(String[] args) {
        Payment payment = new Payment();
        for (int i = 0; i < 5; i++){
            Payment.Product product = new Payment.Product(String.valueOf(i),i);
            payment.setProducts(product);
            System.out.println(product.productName);
        }
        System.out.println(payment.getFinalPrice());
    }
}
