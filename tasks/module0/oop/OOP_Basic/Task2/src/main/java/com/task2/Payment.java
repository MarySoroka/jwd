package com.task2;

import java.awt.*;
import java.util.ArrayList;
import java.util.Map;

public class Payment {
    public long getFinalPrice() {
        for (Product product:getProducts()) {
            setFinalPrice(finalPrice+product.getPrice());
        }
        return finalPrice;
    }

    public void setFinalPrice(long finalPrice) {
        this.finalPrice = finalPrice;
    }

    public ArrayList<Product> getProducts() {
        return products;
    }

    public void setProducts(Product product) {
        this.products.add(product);
    }

    long finalPrice = 0;
    ArrayList<Product> products = new ArrayList<Product>();


    static class Product {
        Product(String productName, int price){
            this.price = price;
            this.productName = productName;
        }
        public String getProductName() {
            return productName;
        }

        public void setProductName(String productName) {
            this.productName = productName;
        }

        public int getPrice() {
            return price;
        }

        public void setPrice(int price) {
            this.price = price;
        }

        String productName;
        int price;

    }
}
