public class Arrays {
    private static int[] createArray(int n) {
        int[] array = new int[n];
        for (int i = 0; i< n; i++){
                array[i] = (int) (Math.random()*10);
                System.out.printf(" %d",array[i]);
        }
        System.out.println();
        return array;
    }

    private static double[] createDoubleArray(int n) {
        double[] array = new double[n];
        for (int i = 0; i< n; i++){
            array[i] = (Math.random()*10);
            System.out.printf(" %f",array[i]);
        }
        System.out.println();
        return array;
    }
    private static int[] createNegativeArray(int n) {
        int[] array = new int[n];
        for (int i = 0; i< n; i++){
            array[i] = (int) (Math.random()*10)*(int) Math.pow(-1,(int) (Math.random()*10));
            System.out.printf(" %d",array[i]);
        }
        System.out.println();
        return array;
    }

    public static boolean isPrime(int n){
        int i,m=0,flag=0;
        m=n/2;
        if(n==0||n==1){
            return false;
        }else{
            for(i=2;i<=m;i++){
                if(n%i==0){
                    return false;
                }
            }
           return true;
        }
    }

    public static void task1(int n, int k){
        System.out.println("task1");
        int[] array = createArray(n);
        for (int i = 0; i< n; i++){
           if(array[i]%k==0){
               System.out.printf(" %d",array[i]);
           }
        }
        System.out.println();
    }

    public static void task2(int n){
        System.out.println("task2");
        int[] array = createArray(n);
        int[] arrayWithZero = new int[n];
        int j = 0;
        for (int i = 0; i< n; i++){
            if(array[i]==0){
                arrayWithZero[j] = i;
                System.out.printf(" %d",arrayWithZero[j]);
                j++;
            }
        }
        System.out.println();
    }
    public static void task3(int n){
        System.out.println("task3");
        int[] array = createNegativeArray(n);
        if ((array[0]>0)&(array[0]!=0)){
            System.out.println("positive");
        }else{
            if (array[1]<0){
              System.out.println("negative");
            }else{
                System.out.println("Positive");
            }
        }
        System.out.println();
    }
    public static void task4(int n){
        System.out.println("task4");
        double[] array = createDoubleArray(n);
        boolean isIncrease = true;
        int i = 1;
        while (isIncrease){
            if(array[i-1]>array[i]){
                isIncrease = false;
            }
            i++;
        }
        System.out.println(isIncrease);
    }
    public static void task5(int n){
        System.out.println("task5");
        int[] array = createArray(n);
        int[] arrayEven = new int[n];
        int j = 0;
        boolean exist = false;
        for (int i = 0; i< n; i++){
            if(array[i]%2==0){
                arrayEven[j] = array[i];
                exist = true;
                System.out.printf(" %d",arrayEven[j]);
                j++;
            }
        }
        if (!exist){
        System.out.println("There aren't even numbers");
        }else{
            System.out.println();
        }
    }
    public static void task6(int n){
        System.out.println("task6");
        int[] array = createArray(n);
        int minInd = 0;
        int maxInd = 0;
        for (int i = 0; i< n; i++){
            if(array[i]>array[maxInd]){
                maxInd = i;
            }
            if (array[i]<array[minInd]){
                minInd = i;
            }
        }
        System.out.println(array[maxInd]-array[minInd]);
    }

    public static void task7(int n,int z){
        System.out.println("task7");
        int[] array = createArray(n);
        int count = 0;
        for (int i = 0; i< n; i++){
           if (array[i]>z){
               array[i] = z;
               count++;
           }
            System.out.printf(" %d",array[i]);
        }
        System.out.println("\n"+count);
    }

    public static void task8(int n){
        System.out.println("task8");
        double[] array = createDoubleArray(n);
        int countPos = 0;
        int countNeg =0;
        for (int i = 0; i< n; i++){
            if(array[i]>0){
                countPos++;
            }
            if(array[i]<0){
                countNeg++;
            }
        }
        System.out.printf("%d %d %d\n",countNeg,countPos,(n-countNeg-countPos));
    }
    public static void task9(int n){
        System.out.println("task9");
        double[] array = createDoubleArray(n);
        int minInd = 0;
        int maxInd = 0;
        for (int i = 0; i< n; i++){
            if(array[i]>array[maxInd]){
                maxInd = i;
            }
            if (array[i]<array[minInd]){
                minInd = i;
            }
        }
        double temp  = array[minInd];
        array[minInd] = array[maxInd];
        array[maxInd] = temp;
        for(int i = 0; i< array.length;i++){
            System.out.printf("%f ",array[i]);
        }
        System.out.println();
    }
    public static void task10(int n){
        System.out.println("task10");
        int[] array = createArray(n);
        for (int i = 0; i< n; i++){
            if(array[i]>i){
                System.out.printf(" %d",array[i]);
            }
        }
        System.out.println();
    }
    public static void task11(int n, int m , int l){
        System.out.println("task11");
        int[] array = createArray(n);
        for (int i = 0; i< n; i++){
            if(array[i]%m==l){
                System.out.printf(" %d",array[i]);
            }
        }
        System.out.println();
    }
    public static void task12(int n){
        System.out.println("task12");
        int[] array = createArray(n);
        int sum = 0;
        for (int i = 0; i< n; i++){
            if(isPrime(i)){
                sum += array[i];
            }
        }
        System.out.println(sum);
    }
    public static void task13(int n, int m, int l, int k){
        System.out.println("task13");
        int[] array = createArray(n);
        int count = 0;
        for (int i = 0; i< n; i++){
            if((array[i]%m==0)&&(array[i]>=l)&&(array[i]<=k)){
                count++;
            }
        }
        System.out.println(count);
    }
    public static void task14(int n){
        System.out.println("task14");
        double[] array = createDoubleArray(n);
        int minInd = 0;
        int maxInd = 0;
        for (int i = 0; i< n; i++){
            if((array[i]>array[maxInd])&&((i+1)%2==0)){
                maxInd = i;
            }
            if ((array[i]<array[minInd])&&((i+1)%2!=0)){
                minInd = i;
            }
        }
        double sum = array[maxInd]+array[minInd];
        System.out.println(sum);
    }
    public static void task15(int n,int c, int d){
        System.out.println("task15");
        double[] array = createDoubleArray(n);
        for (int i = 0; i< n; i++){
            if((array[i]>=c)&&(array[i]<=d)){
                System.out.printf("%f ",array[i]);
            }
        }
        System.out.println();
    }
    public static void main(String[] args) {
        task1(10,2);
        task2(10);
        task3(10);
        task4(10);
        task5(10);
        task6(10);
        task7(10,3);
        task8(10);
        task9(10);
        task10(10);
        task11(10,2, 1);
        task12(10);
        task13(10,2,2,5);
        task14(10);
        task15(10,3,6);
    }
}
