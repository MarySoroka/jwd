public class MultidimensionalArrays {
    public static void task1(){
        System.out.println("task1");
        int[][] array = new int[3][4];
        for (int i = 0; i< 3; i++){
             array[i][0] = 1;
             for (int j =0; j<4; j++){
                 System.out.printf(" %d",array[i][j]);
             }
            System.out.println();
        }
    }

    public static void task2(){
        System.out.println("task2");
        int[][] array = new int[2][3];
        for (int i = 0; i< 2; i++){
            for (int j =0; j<3; j++){
                array[i][j] = (int) (Math.random()*10);
                System.out.printf(" %d",array[i][j]);
            }
            System.out.println();
        }
    }

    public static void task3(int n, int m){
        System.out.println("task3");
        int[][] array=createArray(n, m);
        System.out.println();
        for (int i = 0; i< n; i++){
            for (int j =0; j<m; j++){
                if((j==0)||(j==m-1)){
                    System.out.printf(" %d",array[i][j]);
                }
            }
            System.out.println();
        }
    }

    public static void task4(int n, int m){
        System.out.println("task4");
        int[][] array=createArray(n, m);
        System.out.println();
        for (int i = 0; i< n; i++){
            if((i==0)||(i==m-1)){
            for (int j =0; j<m; j++){

                System.out.printf(" %d",array[i][j]);
                }

            System.out.println();}
        }
    }

    private static int[][] createArray(int n, int m) {
        int[][] array = new int[n][m];
        for (int i = 0; i< n; i++){
            for (int j =0; j<m; j++){
                array[i][j] = (int) (Math.random()*10);
                System.out.printf(" %d",array[i][j]);
            }
            System.out.println();
        }
        return array;
    }
    private static int[][] createNegativeArray(int[][] array,int n,int m) {
        for (int i = 0; i< n; i++){
            for (int j =0; j<m; j++){
                array[i][j] = (int) (Math.random()*10) *(int) Math.pow(-1,(int) (Math.random()*10));
                System.out.printf(" %d",array[i][j]);
            }
            System.out.println();
        }
        return array;
    }

    public static void task5(int n, int m){
        System.out.println("task5");
        int[][] array=createArray(n, m);
        System.out.println();
        for (int i = 0; i< n; i++){
            if((i%2==0)&&(i!=0)){
            for (int j =0; j<m; j++){

                System.out.printf(" %d",array[i][j]);
                }
            System.out.println();}
        }
    }

    public static void task6(int n, int m){
        System.out.println("task6");
        int[][] array=createArray(n, m);
        System.out.println();
        for (int i = 0; i< n; i++){
            for (int j =0; j<m; j++){
                if(((j%2!=0)||(j==0))&&(array[0][j] > array[m-1][j])){
                    System.out.printf(" %d",array[i][j]);
                }
            }
            System.out.println();
        }
    }
    public static void task7(int n, int m){
        System.out.println("task7");
        int[][] array=createNegativeArray(createArray(n, m),n,m);
        System.out.println();
        int sum = 0;
        for (int i = 0; i< n; i++){
            for (int j =0; j<m; j++){
                if(array[i][j]<0){
                    sum+=Math.abs(array[i][j]);
                }
            }
        }
        System.out.println(sum);
    }
    public static void task8(int n, int m){
        System.out.println("task8");
        int[][] array=createArray(n, m);
        System.out.println();
        int count = 0;
        for (int i = 0; i< n; i++){
            for (int j =0; j<m; j++){
                if(array[i][j]==7){
                    count++;
                }
            }
        }
        System.out.println(count);
    }

    public static void task9(int n, int m){
        System.out.println("task9");
        int[][] array=createArray(n, m);
        System.out.println();
        int count = m-1;
        for (int i = 0; i< n; i++){
            for (int j =0; j<m; j++){
                if((i==j)||(i==j-count)){
                    System.out.printf(" %d",array[i][j]);
                    if (i==j-count){
                        count--;
                    }
                }

            }
            count--;
        }
        System.out.println();
    }
    public static void task10(int n, int m,int k, int p){
        System.out.println("task10");
        int[][] array=createArray(n, m);
        System.out.println();
        for (int i = 0; i< n; i++){
            for (int j =0; j<m; j++){
                if((j==p-1)||(i==k-1)){
                    System.out.printf(" %d",array[i][j]);
                }else{
                    System.out.print("  ");

                }
            }
            System.out.println();
        }
    }
    public static void task11(int n, int m){
        System.out.println("task11");
        int[][] array=createArray(n, m);
        System.out.println();
        for (int i = 0; i< n; i++){
            if((i+1)%2!=0){
                for (int j =m-1; j>=0; j--){
                    System.out.printf(" %d",array[i][j]);
                }
            }else{
                for (int j =0; j<m; j++){
                    System.out.printf(" %d",array[i][j]);
                }
            }
            System.out.println();
        }
    }
    public static void task12(int n){
        System.out.println("task12");
        int[][] array = new int[n][n];
        System.out.println();
        for (int i = 0; i< n; i++){
            for (int j =0; j<n; j++){
                if(i==j){
                    array[i][j] = i;
                }
                System.out.printf(" %d",array[i][j]);
            }
            System.out.println();
        }
    }
    public static void task13(int n){
        System.out.println("task13");
        int[][] array=new int[n][n];
        System.out.println();
        for (int i = 0; i< n; i++){
            if(i%2==0){
                for (int j =0; j<n; j++){
                    array[i][j] = j+1;
                    System.out.printf(" %d",array[i][j]);
                }
            }else{
                for (int j =0; j<n; j++){
                    array[i][j] = n - j;
                    System.out.printf(" %d",array[i][j]);
                }
            }
            System.out.println();
        }
    }
    public static void task14(int n){
        System.out.println("task14");
        int[][] array=new int[n][n];
        int count = n-1;
        System.out.println();
        for (int i = 0; i< n; i++){
                for (int j =0; j<n; j++){
                    if(j==count){
                        array[i][j] = i+1;
                        System.out.printf(" %d",array[i][j]);
                        count--;
                    }else {
                        System.out.printf(" %d", array[i][j]);
                    }
                }

            System.out.println();
        }
    }
    public static void task15(int n){
        System.out.println("task15");
        int[][] array=new int[n][n];
        System.out.println();
        for (int i = 0; i< n; i++){
            for (int j =0; j<n; j++){
                if(j==i){
                    array[i][j] = n-i;
                    System.out.printf(" %d",array[i][j]);
                }else {
                    System.out.printf(" %d", array[i][j]);
                }
            }

            System.out.println();
        }
    }
    public static void main(String[] args){
        task1();
        task2();
        task3(2,3);
        task4(3,3);
        task5(4,4);
        task6(4,4);
        task7(5,5);
        task8(5,5);
        task9(3,3);
        task10(4,4,2,4);
        task11(4,4);
        task12(5);
        task13(6);
        task14(6);
        task15(6);
    }
}
