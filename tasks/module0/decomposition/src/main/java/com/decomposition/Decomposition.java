package com.decomposition;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Decomposition {
    static double areaOfTriangle(double a, double b, double c){
        double halfPerimeter = (a+b+c)/2;
        double area = Math.sqrt(halfPerimeter*(halfPerimeter-a)*(halfPerimeter-b)*(halfPerimeter-c));
        return area;
    }
    static double lengthOfSide(int a1, int b1, int a2, int b2){
        return Math.sqrt(Math.pow(a2-a1,2)+Math.pow(b2-b1,2));
    }
    static void task1(int x1, int y1,int x2, int y2,int x3, int y3){
        System.out.println("task1");
        double a = lengthOfSide(x1,y1,x2,y2);
        double b = lengthOfSide(x1,y1,x3,x2);
        double c = lengthOfSide(x2,y2,x3,y3);
        System.out.println(areaOfTriangle(a,b,c));
    }
    static int sst(int a, int b, int gcd){
        return a*b/gcd;
    }
    static int gcd(int a, int b){
        int gcd = 0;
        for(int i = 1; i<=a||i<=b;i++){
            if((a%i == 0)&&(b%i==0)){
                gcd = i;
            }
        }
        return gcd;
    }
    static void task2(int a,int b){
        System.out.println("task2");
        int gcd = gcd(a,b);
        System.out.println(gcd);
        System.out.println(sst(a,b,gcd));
    }
    static int gcd(int a, int b,int c, int d){
        int gcd = 0;
        for(int i = 1; i<=a||i<=b||i<=c||i<=d;i++){
            if((a%i == 0)&&(b%i==0)&&(c%i == 0)&&(d%i==0)){
                gcd = i;
            }
        }
        return gcd;
    }
    static void task3(int a,int b,int c, int d){
        System.out.println("task3");
        System.out.println(gcd(a,b,c,d));
    }
    static long sst(long a, long b,long c){
        for (int i = 1; i<a*b*c;i++){
            if(i%a==0&&i%b==0&&i%c==0){
                return i;
            }
        }
        return a*b*c;
    }
    static void task4(long a,long b,long c){
        System.out.println("task4");
        System.out.println(sst(a,b,c));
    }
    static int max(int a, int b, int c){
        int num1 = Math.max(a,b);
        int num2 = Math.max(c,b);
        if(num1>num2){
            return num1;
        }else{
            return num2;
        }
    }
    static int min(int a, int b, int c){
        int num1 = Math.min(a,b);
        int num2 = Math.min(c,b);
        if(num1<num2){
            return num1;
        }else{
            return num2;
        }
    }
    static void task5(int a,int b,int c){
        System.out.println("task5");
        System.out.println(max(a,b,c)+min(a,b,c));
    }
    static void task6(double a){
        System.out.println("task6");
        System.out.println(areaOfTriangle(a,a,a));
    }
    static double distance(int a1, int b1, int a2, int b2){
        return Math.sqrt(Math.pow(a2-a1,2)+Math.pow(b2-b1,2));
    }
    static void maxDistance(int n,int [][] dots){
        double maxDist = distance(dots[0][0],dots[0][1],dots[1][0],dots[1][1]);
        int indI = 0;
        int indJ = 1;
        for(int i = 0; i <n; i++){
            for(int j = i+1;j<n;j++){
                double distance = distance(dots[i][0],dots[i][1],dots[j][0],dots[j][1]);
                if(distance>maxDist){
                    maxDist = distance;
                    indI = i;
                    indJ = j;
                }
            }
        }
        System.out.printf("Max distance between %d %d and %d %d = %f\n",dots[indI][0],dots[indI][1],dots[indJ][0],dots[indJ][1],maxDist);
    }
    static void task7(int n, int[][] dots){
        System.out.println("task7");
        maxDistance(n, dots);
    }
    static int[] bubbleSort(int n, int[] array){
        int temp1;
        for(int i = 0; i<n; i++){
            for (int j =n-i; j<n;j++){
                if(array[i]>array[j]){
                    temp1 = array[i];
                    array[i] = array[j];
                    array[j] = temp1;
                }
            }
        }
        return array;
    }
    static void task8(int n, int[] array){
        System.out.println("task8");
        array = bubbleSort(n, array);
        System.out.println(array[1]);

    }
    static boolean isPrime(int a, int b){
        return gcd(a, b) == 1;
    }
    static void task9(int a, int b, int c){
        System.out.println("task9");
        System.out.println(isPrime(a,b)&&isPrime(b,c)&&isPrime(a,c));
    }
    static int factorial(int n){
        int factorial = 1;
        for(int i = 1; i <=n; i++){
                factorial*=i;
        }
        return factorial;
    }
    static void task10(){
        System.out.println("task10");
        for (int i = 1; i <10; i++){
            if(i%2!=0){
                System.out.println(i + ":" + factorial(i));}
        }
    }
    static int addition(int k, int m, int[] d){
        int add = 0;
        for (int i = k; i<=m;i++){
            add+= d[i];
        }
        return add;
    }
    static void task11(int[] d){
        System.out.println("task11");
        System.out.println(addition(0,2,d));
        System.out.println(addition(2,4,d));
        System.out.println(addition(3,5,d));
    }
    static double areaOfRightTriangle(int a, int b){
        return (double) a*b/2;
    }
    static void task12(int x, int y, int z, int t){
        System.out.println("task12");
        System.out.println(areaOfRightTriangle(x,y)+areaOfTriangle(z,t,Math.sqrt(x*x+y*y)));
    }

    static int[] getArray(String num){
        int[] array = new int[num.length()];
        for (int i = 0; i < num.length();i++){
            array[i] = Character.getNumericValue(num.charAt(i));
        }
        return array;
    }
    static void task13(long n){
        System.out.println("task13");
        String num = String.valueOf(n);
        System.out.println(Arrays.toString(getArray(num)));
    }
    static long maxLen(String a, String b){
        if (a.length()>b.length()){
            return Long.parseLong(a);
        }else{
            return Long.parseLong(b);
        }
    }
    static void task14(long a, long b){
        System.out.println("task14");
        System.out.println(maxLen(String.valueOf(a),String.valueOf(b)));
    }
    static int add(int num){
        int add = 0;
        while(num!=0){
            add +=num%10;
            num/=10;
        }
        return add;
    }
    static int[] createArr(int k, int n){
        List<Integer> list = new LinkedList<>();
        for(int i =0 ; i < n; i++){
            if((add(i)==k)){
                list.add(i);
            }
        }
        int[] arr = new int[list.size()];
        for (int i = 0 ; i<list.size(); i++){
            arr[i] = list.get(i);
        }
        return arr;
    }
    static void task15(int k, int n){
        System.out.println("task15");
        System.out.println(Arrays.toString(createArr(k, n)));
    }

    static boolean isPrime(int a){
        for(int i = 2; i<a;i++){
            if((a%i == 0)){
                return false;
            }
        }
        return true;
    }
    static int[] createArr(int n){
        List<Integer> list = new LinkedList<>();
        for(int i =n ; i < 2*n; i++){
            if(isPrime(i)&&isPrime(i+2)){
                list.add(i);
                list.add(i+2);
                i+=2;
            }
        }
        int[] arr = new int[list.size()];
        for (int i = 0 ; i<list.size(); i++){
            arr[i] = list.get(i);
        }
        return arr;
    }
    static void task16(int n){
        System.out.println("task16");
        System.out.println(Arrays.toString(createArr(n)));
    }
    static int sumPowN(int n){
        String num = String.valueOf(n);
        int sum = 0;
        for(int i = 0; i<num.length();i++){
            sum+= Character.getNumericValue(num.charAt(i));
        }
        return(int) Math.pow(sum,num.length());
    }
    static boolean isArmstrong(int k){
        if(sumPowN(k)==k){
            return true;
        }else {
            return false;
        }
    }
    static int[] createArmstrongArr(int k){
        List<Integer> list = new LinkedList<>();
        for(int i =1 ; i < k; i++){
            if(isArmstrong(i)){
                list.add(i);
            }
        }
        int[] arr = new int[list.size()];
        for (int i = 0 ; i<list.size(); i++){
            arr[i] = list.get(i);
        }
        return arr;
    }
    static void task17(int k){
        System.out.println("task17");
        System.out.println(Arrays.toString(createArmstrongArr(k)));
    }
    static boolean checkNum(int n){
        String num = String.valueOf(n);
        for(int i = 1; i<num.length();i++){
            if(Character.getNumericValue(num.charAt(i-1))>Character.getNumericValue(num.charAt(i))&&(Character.getNumericValue(num.charAt(i-1))!=Character.getNumericValue(num.charAt(i)))){
                return false;
            }
        }
        return true;
    }
    static int createNum(int n){
        String num = "";
        if(n!=1){
        for (int i = 1; i <n ; i++){
            num += String.valueOf(i);
        }
        return Integer.parseInt(num);}
        else{
           return n;
        }
    }
    static void task18(int n){
        System.out.println("task18");
        List<Integer> list = new LinkedList<>();
        for (int i = createNum(n); i <Math.pow(10,n);i++){
            if(checkNum(i)){
                list.add(i);
            }
        }
        int[] arr = new int[list.size()];
        for (int i = 0 ; i<list.size(); i++){
            arr[i] = list.get(i);
        }
        System.out.println(Arrays.toString(arr));
    }

    static boolean onlyOddNum(int n){
        String num = String.valueOf(n);
        for(int i = 0; i<num.length();i++){
            if(Character.getNumericValue(num.charAt(i))%2==0){
                return false;
            }
        }
        return true;
    }
    static int onlyEvenNum(long n){
        String num = String.valueOf(n);
        int amount = 0;
        for(int i = 0; i<num.length();i++){
            if(Character.getNumericValue(num.charAt(i))%2==0){
                amount +=1;
            }
        }
        return amount;
    }
    static void task19(int n){
        System.out.println("task19");
        List<Integer> list = new LinkedList<>();
        for (int i = createNum(n); i <Math.pow(10,n);i++){
            if(onlyOddNum(i)){
                list.add(i);
            }
        }
        long add = 0;
        for (Integer integer : list) {
            add += integer;
        }
        System.out.println(add);
        System.out.println(onlyEvenNum(add));
    }

    static long sum(long n,int k){
        String num = String.valueOf(n);
        long sum = 0;
        for (int i = 0; i < num.length(); i++){
            sum += Character.getNumericValue(num.charAt(i));
        }
        if(n-sum!=0){
            k++;
            return sum(n-sum,k);
        }else{
            return k;
        }

    }
    static void task20(int n){
        System.out.println("task20");
        System.out.println(sum(n,1));
    }

    public static void main(String[] args) {
       task1(1,1,2,2,1,2);
       task2(24,32);
       task3(1,2,3,4);
       task4(5,12,3);
       task5(1,2,3);
       task6(4);
       task7(5,new int[][]{{1,1},{3,3},{5,4},{6,6},{7,7}});
       task8(6,new int[]{2,5,3,7,3,5});
       task9(3,5,7);
       task10();
       task11(new int[]{1,4,3,5,6,6});
       task12(1,2,3,4);
       task13(12345);
       task14(1243,234);
       task15(10,100);
       task16(40);
       task17(100);
       task18(4);
       task19(1);
       task20(12);
    }
}
